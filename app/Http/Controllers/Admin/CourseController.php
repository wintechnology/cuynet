<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CourseController extends Controller
{
    public function course(){
        Session::put('page','courses');
        $courses = Course::orderBy('updated_at','DESC')->get();
        return view('admin.course.courses')->with(compact('courses'));
    }

    public function addEditCourse(Request $req, $id = null)
    {
        Session::put('page','courses');
        if ($id == "") {
            $title = "Ajouter une matière";
            $course = new Course();
        } else {
            $title = "Modifier une matière";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; dd($data); die;

            $course->code = $data['code'];
            $course->name = $data['name'];
            $course->room_id = $data['room_id'];

//            echo "<pre>"; dd($course); die;
            $course->save();

            Session::flash('success_message', 'Matière "'.$course->name.'" ajoutée avec succès!');
            return redirect('admin/courses');
        }

        //        Get all section
        $courses = Course::get();
        $rooms = Room::get();
        return view('admin.course.add_edit_course')->with(compact('title', 'courses', 'rooms'));
    }
}
