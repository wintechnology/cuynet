<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SectionType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SectionTypeController extends Controller
{
    public function type(){
        Session::put('page','types_section');
        $sectionsTypes = SectionType::get();
        return view('admin.section.section_types')->with(compact('sectionsTypes'));
    }

    public function addEditSectionsType(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un type de section/filière";
            $sectionsType = new SectionType();
        } else {
            $title = "Modifier un type de section/filière";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $sectionsType->name = $data['name'];

            $sectionsType->save();

            Session::flash('success_message', 'Type de section/filière "'.$sectionsType->name.'" enregistré !');
            return redirect('admin/types_sections');
        }
        return view('admin.section.section_types')->with(compact('title'));
    }
}
