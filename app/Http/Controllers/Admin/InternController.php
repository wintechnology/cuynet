<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Intern;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class InternController extends Controller
{
    public function intern(){
        Session::put('page','interns');
        $interns = Intern::orderBy('updated_at','DESC')->get();
        return view('admin.intern.interns')->with(compact('interns'));
    }

    public function addEditIntern(Request $req, $id = null)
    {
        Session::put('page','courses');
        if ($id == "") {
            $title = "Ajouter un stagiaire";
            $intern = new Intern();
        } else {
            $title = "Modifier un stagiaire";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
                        echo "<pre>"; dd($data); die;

            //            Upload file
            /*if ($req->hasFile('formation_content')) {
                $file_tmp = $req->file('formation_content');
                if ($file_tmp->isValid()) {
                    //                    get file extension
                    $extension = $file_tmp->getClientOriginalExtension();
                    //                    Generate new file name
                    $fileName = rand(111, 99999) . '.' . $extension;
//                    $filePath = 'files/' . $fileName;
                    //                    Upload the file
//                    File::make($file_tmp)->save($filePath);
//                    $filePath = Storage::putFile('files/formations', $req->file('formation_content'));
//                    $filePath = $req->file('formation_content')->storeAs('files/formations', $fileName, 'public');
                    $filePath = $req->file('formation_content')->store('public/files');

                    $formation->content = $filePath;
                }
            }*/

//            if (empty($data['formation_goal'])) {
//                $data['formation_goal'] = "";
//            }
//            if (empty($data['meta_title'])) {
//                $data['meta_title'] = "";
//            }
//            if (empty($data['meta_description'])) {
//                $data['meta_description'] = "";
//            }
//            if (empty($data['meta_keywords'])) {
//                $data['meta_keywords'] = "";
//            }
//            if (empty($data['category_url'])) {
//                $data['category_url'] = strtolower($data['category_name']);
//            }

            $formation = Formation::find($data['formation_id']);


            $course->formation_id = $data['formation_id'];
            $course->place = $data['place'];
            $course->start = $data['date_debut'];
            $course->end = $data['date_fin'];
            $course->places = $data['places'];

//            echo "<pre>"; dd($course); die;
            $course->save();

            Session::flash('success_message', 'Session de formation pour "'.ucwords($formation->theme).'" ajoutée avec succès!');
            return redirect('admin/courses');
        }

        //        Get all section
        $date = date("d/m/Y");
        return view('admin.intern.add_edit_intern')->with(compact('title', 'date'));
    }
}
