<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Classe;
use App\Models\LibraryCat;
use App\Models\Location;
use App\Models\Ouvrage;
use App\Models\Rent;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LibraryController extends Controller
{
    public function libraryCategories(){
        Session::put('page','library_categories');
        $libraryCategories = LibraryCat::get();
        return view('admin.settings.library_categories')->with(compact('libraryCategories'));
    }
    public function locations(){
        Session::put('page','locations');
        $locations = Location::get();
        return view('admin.settings.locations')->with(compact('locations'));
    }
    public function ouvrages(){
        Session::put('page','ouvrages');
        $ouvrages = Ouvrage::get();
        return view('admin.library.ouvrages')->with(compact('ouvrages'));
    }
    public function rents(){
        Session::put('page','rents');
        $rents = Rent::get();
        return view('admin.library.rents')->with(compact('rents'));
    }
    public function bookings(){
        Session::put('page','bookings');
        $bookings = Booking::get();
        return view('admin.library.bookings')->with(compact('bookings'));
    }

    public function addEditLibraryCategory(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter une catégorie";
            $libraryCategories = new LibraryCat();
        } else {
            $title = "Modifier une catégorie";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $libraryCategories->name = $data['category_name'];
            $libraryCategories->save();

            Session::flash('success_message', 'Catégorie "'.$libraryCategories->name.'" enregistré !');
            return redirect('admin/library_categories');
        }
        return view('admin.settings.library_categories')->with(compact('title'));
    }
    public function addEditLocation(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un emplacement";
            $locations = new Location();
        } else {
            $title = "Modifier un emplacement";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $locations->name = $data['name'];
            $locations->save();

            Session::flash('success_message', 'Emplacement "'.$locations->name.'" enregistré !');
            return redirect('admin/locations');
        }
        return view('admin.settings.locations')->with(compact('title'));
    }

    public function addEditOuvrage(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un ouvrage";
            $ouvrages = new Ouvrage();
        } else {
            $title = "Modifier un ouvrage";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $ouvrages->ref = $data['ref'];
            $ouvrages->category_id = $data['category_id'];
            $ouvrages->location_id = $data['location_id'];
            $ouvrages->title = $data['title'];
            $ouvrages->quantity = $data['quantity'];
            $ouvrages->save();

            Session::flash('success_message', 'Ouvrage "'.$ouvrages->title.'" enregistré !');
            return redirect('admin/ouvrages');
        }
        $categories = LibraryCat::get();
        $locations = Location::get();
        $ouvragesCounter = Ouvrage::count() + 1;
        $todayDay = date("d");
        $todayMonth = date("m");
        $todayYear = date("Y");
        $ref = "OUV{$todayYear}{$todayMonth}{$todayDay}00{$ouvragesCounter}";
        return view('admin.library.add_edit_ouvrage')->with(compact('title',
        'categories',
        'locations',
        'ref'));
    }
    public function addEditRent(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un prêt";
            $rents = new Rent();
        } else {
            $title = "Modifier un prêt";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $rents->ref = $data['ref'];
            $rents->student_id = $data['student_id'];
            $rents->ouvrage_id = $data['ouvrage_id'];
            $rents->rentDate = $data['rentDate'];
            $rents->delay = $data['delay'];
            $rents->dateBack = $data['dateBack'];
            $rents->quantityTake = $data['quantityTake'];
            $rents->quantityBack = $data['quantityBack'];
            $rents->save();

            Session::flash('success_message', 'Prêt "'.$rents->ref.'" enregistré !');
            return redirect('admin/rents');
        }
        $ouvrages = Ouvrage::get();
        $students = Student::get();
        $rentsCounter = Rent::count() + 1;
        $todayDay = date("d");
        $todayMonth = date("m");
        $todayYear = date("Y");
        $ref = "RT{$todayYear}{$todayMonth}{$todayDay}00{$rentsCounter}";
        return view('admin.library.add_edit_rent')->with(compact('title',
        'ouvrages',
        'students',
        'ref'));
    }
    public function addEditBooking(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter une reservation";
            $bookings = new Booking();
        } else {
            $title = "Modifier une reservation";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $bookings->num = $data['num'];
            $bookings->student_id = $data['student_id'];
            $bookings->ouvrage_id = $data['ouvrage_id'];
            $bookings->classe_id = $data['classe_id'];
            $bookings->rentDate = $data['rentDate'];
            $bookings->save();

            Session::flash('success_message', 'Reservation "'.$bookings->num.'" enregistré !');
            return redirect('admin/bookings');
        }
        $ouvrages = Ouvrage::get();
        $students = Student::get();
        $classes = Classe::get();
        $bookingCounter = Booking::count() + 1;
        $todayDay = date("d");
        $todayMonth = date("m");
        $todayYear = date("Y");
        $ref = "BKG{$todayYear}{$todayMonth}{$todayDay}00{$bookingCounter}";
        return view('admin.library.add_edit_booking')->with(compact('title',
        'ouvrages',
        'students',
        'classes',
        'ref'));
    }
}
