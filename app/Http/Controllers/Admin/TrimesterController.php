<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Trimester;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TrimesterController extends Controller
{
    public function trimester()
    {
        Session::put('page', 'trimesters');
        $trimesters = Trimester::orderBy('updated_at', 'ASC')->get();
        return view('admin.trimester.trimesters')->with(compact('trimesters'));
    }

    public function addEditTrimester(Request $req, $id = null)
    {
        Session::put('page', 'trimesters');
        if ($id == "") {
            $title = "Ajouter un trimestre";
            $trimester = new Trimester();
        } else {
            $title = "Modifier un trimestre";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; dd($data); die;

            $trimester->year = $data['year'];
            $trimester->value = $data['name'];
            $trimester->start = $data['start'];
            $trimester->end = $data['end'];

            //            echo "<pre>"; dd($course); die;
            $trimester->save();

            Session::flash('success_message', 'Trimestre "' . $trimester->name . '" ajoutée avec succès!');
            return redirect('admin/trimesters');
        }

        //        Get all section
        $trimesters = Trimester::get();
        $currentYear = date("Y");
        $nextYear = date("Y") + 1;
        $scolar = "${currentYear} - ${nextYear}";
        return view('admin.trimester.add_edit_trimester')->with(compact('title', 'trimesters', 'scolar'));
    }
}
