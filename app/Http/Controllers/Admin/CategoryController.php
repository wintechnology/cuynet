<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CourseCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    public function category(){
        Session::put('page','categories');
        $categories = CourseCategory::get();
        return view('admin.settings.categories')->with(compact('categories'));
    }
    public function yearCategory(){
        Session::put('page','year-categories');
        $categories = Category::get();
        return view('admin.settings.year_categories')->with(compact('categories'));
    }

    public function updateCategoryStatus(Request $req)
    {
        if ($req->ajax()) {
            $data = $req->all();
            //            echo '<pre>';print_r($data); die;
            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            CourseCategory::where('id', $data['category_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'category_id' => $data['category_id']]);
        }
    }

    public function addEditCategory(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter une catégorie";
            $category = new CourseCategory;
        } else {
            $title = "Modifier une catégorie";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();

            $category->name = ucwords($data['category_name']);
            $category->save();

            Session::flash('success_message', 'categorie "'.ucwords($data['category_name']).'" ajoutée avec succès !');
            return redirect('admin/categories');
        }

        //        Get all section
        $getCourseCategories = CourseCategory::get();
        return view('admin.settings.categories')->with(compact('title', 'getCourseCategories'));
    }
    public function addEditYearCategory(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter une catégorie";
            $category = new Category();
        } else {
            $title = "Modifier une catégorie";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();

            $category->name = ucwords($data['category_name']);
            $category->save();

            Session::flash('success_message', 'categorie "'.ucwords($data['category_name']).'" ajoutée avec succès !');
            return redirect('admin/year-categories');
        }

        //        Get all section
        $getCategories = Category::get();
        return view('admin.settings.year_categories')->with(compact('title', 'getCategories'));
    }
}
