<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SiteController extends Controller
{
    public function sites(){
        Session::put('page','sites');
        $sites = Site::get();
        return view('admin.settings.sites')->with(compact('sites'));
    }

    public function addEditSite(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un site";
            $site = new Site();
        } else {
            $title = "Modifier un site";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $site->name = $data['name'];
            $site->code = $data['code'];
            $site->save();

            Session::flash('success_message', 'Site "'.$site->name.'" enregistré !');
            return redirect('admin/sites');
        }

        //        Get all section
        $sites = Site::get();
        return view('admin.settings.sites')->with(compact('title', 'sites'));
    }
}
