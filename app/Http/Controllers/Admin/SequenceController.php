<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Sequence;
use App\Models\Trimester;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SequenceController extends Controller
{
    public function sequence()
    {
        Session::put('page', 'sequences');
        $sequences = Sequence::orderBy('value', 'ASC')->get();
        return view('admin.sequence.sequences')->with(compact('sequences'));
    }

    public function addEditSequence(Request $req, $id = null)
    {
        Session::put('page', 'sequences');
        if ($id == "") {
            $title = "Ajouter une sequence";
            $sequence = new Sequence();
        } else {
            $title = "Modifier une sequence";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; dd($data); die;

            $sequence->trimester_id = $data['trimester_id'];
            $sequence->value = $data['name'];
            $sequence->start = $data['start'];
            $sequence->end = $data['end'];

            //            echo "<pre>"; dd($sequence); die;
            $sequence->save();

            Session::flash('success_message', 'Séquence "' . $sequence->value . '" ajoutée avec succès!');
            return redirect('admin/sequences');
        }

        //        Get all section
        $sequences = Sequence::orderBy('value', 'ASC')->get();
        $trimesters = Trimester::get();
        return view('admin.sequence.add_edit_sequence')->with(compact('title', 'sequences', 'trimesters'));
    }
}
