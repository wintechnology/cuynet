<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use App\Models\Category;
use App\Models\School;
use App\Models\Sex;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TeacherController extends Controller
{
    public function teachers(){
        Session::put('page','teachers');
        $teachers = Teacher::orderBy('updated_at','DESC')->get();
        return view('admin.settings.teachers')->with(compact('teachers'));
    }

    public function addEditTeacher(Request $req, $id = null)
    {
        Session::put('page','teachers');
        if ($id == "") {
            $title = "Ajouter un enseignant";
            $teacher = new Teacher();
        } else {
            $title = "Modifier un enseignant";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; dd($data); die;

            $teacher->matricule = $data['matricule'];
            $teacher->name = $data['name'];
            $teacher->phone = $data['phone'];
            $teacher->email = $data['mail'];
//            echo "<pre>"; dd($academic_year); die;
            $teacher->save();

            Session::flash('success_message', 'Enseignant "'.$teacher->name.'" ajouté avec succès!');
            return redirect('admin/teachers');
        }

        //        Get all section
        $academicYears = AcademicYear::get();
        $sexes = Sex::get();
        $categories = Category::get();
        $school = School::find(1);
        $currentYear = date("Y");
        $nextYear = date("Y") + 1;
        $scolar = "${currentYear} - ${nextYear}";
        return view('admin.settings.add_edit_teacher')->with(compact('title', 'academicYears', 'categories', 'school', 'scolar', 'sexes'));
    }
}
