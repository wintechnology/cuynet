<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Lesson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LessonController extends Controller
{
    public function lesson(){
        Session::put('page','lessons');
        $lessons = Lesson::orderBy('updated_at','DESC')->get();
        return view('admin.lesson.lessons')->with(compact('lessons'));
    }

    public function addEditLesson(Request $req, $id = null)
    {
        Session::put('page','lessons');
        if ($id == "") {
            $title = "Ajouter une leçon";
            $lesson = new Lesson();
        } else {
            $title = "Modifier une leçon";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; dd($data); die;

            $lesson->course_id = $data['course_id'];
            $lesson->objective = $data['objective'];
            $lesson->label = $data['label'];

//            echo "<pre>"; dd($sequence); die;
            $lesson->save();

            Session::flash('success_message', 'Leçon "'.$lesson->label.'" ajoutée avec succès!');
            return redirect('admin/lessons');
        }

        //        Get all section
        $lessons = Lesson::get();
        $courses = Course::get();
        return view('admin.lesson.add_edit_lesson')->with(compact('title',
            'lessons',
            'courses'));
    }
}
