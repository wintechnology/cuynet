<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Site;
use App\Models\City;
use App\Models\Country;
use App\Models\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MemberController extends Controller
{
    public function member(){
        Session::put('page','members');
        $members = Member::orderBy('updated_at', 'DESC')->get();
        return view('admin.member.members')->with(compact('members'));
    }

    public function addEditMember(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter une structure";
            $member = new Member();
        } else {
            $title = "Modifier une structure";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//                        echo "<pre>"; dd($data); die;

            //            Upload file
            /*if ($req->hasFile('formation_content')) {
                $file_tmp = $req->file('formation_content');
                if ($file_tmp->isValid()) {
                    //                    get file extension
                    $extension = $file_tmp->getClientOriginalExtension();
                    //                    Generate new file name
                    $fileName = rand(111, 99999) . '.' . $extension;
//                    $filePath = 'files/' . $fileName;
                    //                    Upload the file
//                    File::make($file_tmp)->save($filePath);
//                    $filePath = Storage::putFile('files/formations', $req->file('formation_content'));
//                    $filePath = $req->file('formation_content')->storeAs('files/formations', $fileName, 'public');
                    $filePath = $req->file('formation_content')->store('public/files');

                    $formation->content = $filePath;
                }
            }*/

//            if (empty($data['formation_goal'])) {
//                $data['formation_goal'] = "";
//            }
//            if (empty($data['meta_title'])) {
//                $data['meta_title'] = "";
//            }
//            if (empty($data['meta_description'])) {
//                $data['meta_description'] = "";
//            }
//            if (empty($data['meta_keywords'])) {
//                $data['meta_keywords'] = "";
//            }
//            if (empty($data['category_url'])) {
//                $data['category_url'] = strtolower($data['category_name']);
//            }


            $member->reference = $data['reference'];
            $member->company_name = $data['name'];
            $member->activity_sector = $data['activity_sector'];
            $member->country = $data['country'];
            $member->city = $data['city'];
            $member->phone = $data['phone'];
            $member->email = $data['email'];
            $member->type = $data['type'];
            $member->save();

            Session::flash('success_message', 'Structure "'.ucwords($data['name']).'" ajoutée avec succès!');
            return redirect('admin/members');
        }

        //        Get all Features
        $getSa = Site::get();
        $getCountries = Country::get();
        $getCities = City::get();
        return view('admin.member.add_edit_member')->with(compact('title', 'getSa', 'getCountries', 'getCities'));
    }
}
