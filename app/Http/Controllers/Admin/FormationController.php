<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CourseCategory;
use App\Models\Formation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class FormationController extends Controller
{
    public function addEditFormation(Request $req, $id = null)
    {
        Session::put('page', 'formations');

        if ($id == "") {
            $title = "Ajouter une formation";
            $formation = new Formation();
        } else {
            $title = "Modifier une formation";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            // echo "<pre>";
            // dd($data);
            // die;

            $formation->name = $data['name'];
            $formation->save();

            Session::flash('success_message', 'Type de formation "' . ucwords($data['name']) . '" ajouté avec succès!');
            $formations = Formation::orderBy('updated_at', 'DESC')->get();
            return redirect('admin/add-edit-formation_type')->with(compact('formations'));
        }

        //        Get all section
        $formations = Formation::orderBy('updated_at', 'DESC')->get();
        return view('admin.formation.formations')->with(compact('formations'));
    }
}
