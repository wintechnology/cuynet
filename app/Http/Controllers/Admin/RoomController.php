<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Building;
use App\Models\Room;
use App\Models\RoomType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RoomController extends Controller
{
    public function room(){
        Session::put('page','rooms');
        $rooms = Room::get();
        $roomTypes = RoomType::get();
        $buildings = Building::get();
        return view('admin.settings.rooms')->with(compact('rooms','roomTypes','buildings'));
    }

    public function addEditRoom(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter une salle";
            $room = new Room();
        } else {
            $title = "Modifier une salle";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $room->type_id = $data['roomType_id'];
            $room->building_id = $data['building_id'];
            $room->code = $data['code'];
            $room->name = $data['name'];

            $room->save();

            Session::flash('success_message', 'Salle "'.$room->name.'" enregistrée !');
            return redirect('admin/rooms');
        }

        //        Get all section
        $rooms = Room::get();
        $roomTypes = RoomType::get();
        $buildings = Building::get();
        return view('admin.settings.rooms')->with(compact('title', 'rooms','roomTypes','buildings'));
    }
}
