<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use App\Models\Art;
use App\Models\Branch;
use App\Models\CivilState;
use App\Models\Classe;
use App\Models\Collect;
use App\Models\Country;
use App\Models\Father;
use App\Models\File as mFile;
use App\Models\Language;
use App\Models\Level;
use App\Models\Mother;
use App\Models\Regime;
use App\Models\RequiredDocument;
use App\Models\Rubric;
use App\Models\School;
use App\Models\Section;
use App\Models\Sex;
use App\Models\State;
use App\Models\Student;
use App\Models\Tutor;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class SubscriptionController extends Controller
{
    public function subscriptions()
    {
        Session::put('page', 'subscriptions');
        $students = Student::get();
        return view("admin.student.subscription")->with(compact('students'));
    }
    public function sexes()
    {
        Session::put('page', 'sexes');
        $sexes = Sex::get();
        return view("admin.student.sexes")->with(compact('sexes'));
    }
    public function maritals()
    {
        Session::put('page', 'maritals');
        $maritals = CivilState::get();
        return view("admin.student.maritals")->with(compact('maritals'));
    }
    public function fathers()
    {
        Session::put('page', 'fathers');
        $fathers = Father::get();
        return view("admin.student.fathers")->with(compact('fathers'));
    }
    public function mothers()
    {
        Session::put('page', 'mothers');
        $mothers = Mother::get();
        return view("admin.student.mothers")->with(compact('mothers'));
    }
    public function tutors()
    {
        Session::put('page', 'tutors');
        $tutors = Tutor::get();
        return view("admin.student.tutors")->with(compact('tutors'));
    }

    public function addEditSex(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un sex";
            $sex = new Sex();
        } else {
            $title = "Modifier un sex";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $sex->name = $data['name'];
            $sex->code = $data['code'];

            $sex->save();

            Session::flash('success_message', 'Sexe "' . $sex->name . '" enregistré !');
            return redirect('admin/sexes');
        }
        return view("admin.student.sexes")->with(compact('title'));
    }
    public function addEditMarital(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un état civil";
            $marital = new CivilState();
        } else {
            $title = "Modifier un état civil";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $marital->name = $data['name'];
            $marital->code = $data['code'];

            $marital->save();

            Session::flash('success_message', 'Etat civil "' . $marital->name . '" enregistré !');
            return redirect('admin/maritals');
        }
        return view("admin.student.maritals")->with(compact('title'));
    }
    public function addEditFather(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un père";
            $father = new Father();
        } else {
            $title = "Modifier un père";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $father->name = $data['name'];
            $father->work = $data['work'];
            $father->phone = $data['phone'];
            $father->email = $data['email'];

            $father->save();

            Session::flash('success_message', 'Contact "' . $father->name . '" enregistré !');
            return redirect('admin/fathers');
        }
        return view("admin.student.fathers")->with(compact('title'));
    }
    public function addEditMother(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter une mère";
            $mother = new Mother();
        } else {
            $title = "Modifier une mère";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $mother->name = $data['name'];
            $mother->work = $data['work'];
            $mother->phone = $data['phone'];
            $mother->email = $data['email'];

            $mother->save();

            Session::flash('success_message', 'Mère "' . $mother->name . '" enregistré !');
            return redirect('admin/mothers');
        }
        return view("admin.student.mothers")->with(compact('title'));
    }
    public function addEditTutor(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un tuteur";
            $tutor = new Tutor();
        } else {
            $title = "Modifier un tuteur";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $tutor->name = $data['name'];
            $tutor->work = $data['work'];
            $tutor->phone = $data['phone'];
            $tutor->email = $data['email'];

            $tutor->save();

            Session::flash('success_message', 'Tuteur "' . $tutor->name . '" enregistré !');
            return redirect('admin/tutors');
        }
        return view("admin.student.tutors")->with(compact('title'));
    }

    /**
     * Show the step One Form for creating a new product.
     *
     * @param Request $request
     * @return View
     */
    public function createStepOne(Request $request)
    {
        //        $product = $request->session()->get('product');
        $school = School::get()[0];
        $academicYears = AcademicYear::get();
        $sexes = Sex::get();
        $sections = Section::get();
        $branches = Branch::get();
        $classes = Classe::get();
        $states = State::get();
        $regimes = Regime::get();
        $rds = RequiredDocument::get();
        $arts = Art::get();
        $lv2s = Language::get();

        $levels = Level::get();
        $year = date("Y");
        $uuid = uniqid("", true);
        $uuidpart1 = strtoupper(substr($uuid, 11, 2));
        $uuidpart2 = strtoupper(substr($uuid, 20, 3));
        $matricule = "${year}i${uuidpart1}${uuidpart2}";

        //$student = new Student();
        $student = Student::where("matricule", $matricule)->first();
        if (!empty($student)) {
            $uuidpart1 = strtoupper(substr($uuid, 11, 2));
            $uuidpart2 = strtoupper(substr($uuid, 20, 3));
            $matricule = "${year}i${uuidpart1}${uuidpart2}";

            $student = Student::where("matricule", $matricule)->first();
        }

        $student_name = "BRAGBO";
        $student_surname = "Jean-Pierre";
        $father_name = "NomTest PrenomTest";
        $father_profession = "ProfessionTest";
        $father_phone = "000000000";
        $birthay_place = "Abidjan";
        $countries = Country::get();
        $fathers = Father::get();
        $mothers = Mother::get();
        $tutors = Tutor::get();
        return view("admin.student.step-one")
            ->with(compact(
                'school',
                'academicYears',
                'sexes',
                'sections',
                'levels',
                'student_name',
                'student_surname',
                'father_name',
                'father_profession',
                'father_phone',
                'birthay_place',
                'matricule',
                'countries',
                'regimes',
                'states',
                'branches',
                'classes',
                'rds',
                'arts',
                'lv2s',
                'fathers',
                'mothers',
                'tutors',
                'student'
            ));
    }


    /**
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     */
    public function postCreateStepOne(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Inscription";
            $student = new Student();
        } else {
            $title = "Modifier un élève";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //    echo "<pre>"; print_r($data); die;

            $student->school_name = $data['school_name'];
            $student->year_id = $data['year_id'];
            $student->matricule = $data['matricule'];
            $student->other_matricule = $data['other_matricule'];
            $student->name = strtoupper($data['name']);
            $student->surname = ucwords($data['surname']);
            $student->birthday_date = $data['birthday_date'];
            $student->birthday_place = $data['birthday_place'];
            $student->nationality = $data['nationality'];
            $student->sex = $data['sex'];
            $student->city = $data['city'];
            $student->neighborhood = $data['neighborhood'];
            $student->phone = $data['phone'];
            $student->email = $data['email'];
            $student->profession = $data['profession'];
            $student->address = $data['address'];
            $student->marital = $data['marital'];
            $student->phone_partner = $data['phone_partner'];
            $student->email_partner = $data['email_partner'];
            $student->name_partner = $data['name_partner'];
            $student->profession_partner = $data['profession_partner'];
            $student->chronic_disease = $data['chronic_disease'];
            $student->allergy = $data['allergy'];
            $student->medical_observation = $data['medical_observation'];
            $student->child = $data['child'];

            if ("non" == $data['father_manual']) {
                $student->father_id = $data['father_id'];
            } else {
                $father = new Father();
                $father->name = $data['father_name'];
                $father->work = $data['father_profession'];
                $father->phone = $data['father_phone'];
                $father->email = $data['father_email'];

                $father->save();

                $father = Father::latest()->first();
                $student->father_id = $father->id;
            }
            if ("non" == $data['mother_manual'])
                $student->mother_id = $data['mother_id'];
            else {
                $mother = new Mother();
                $mother->name = $data['mother_name'];
                $mother->work = $data['mother_profession'];
                $mother->phone = $data['mother_phone'];
                $mother->email = $data['mother_email'];

                $mother->save();

                $mother = Mother::latest()->first();
                $student->mother_id = $mother->id;
            }
            if ("non" == $data['tutor_manual'])
                $student->tutor_id = $data['tutor_id'];
            else {
                $tutor = new Tutor();
                $tutor->name = $data['tutor_name'];
                $tutor->work = $data['tutor_profession'];
                $tutor->phone = $data['tutor_phone'];
                $tutor->email = $data['tutor_email'];

                $tutor->save();

                $tutor = Tutor::latest()->first();
                $student->tutor_id = $tutor->id;
            }

            $student->faculty_id = $data['faculty_id'];
            $student->previous_level_id = $data['previous_level_id'];
            $student->diploma = $data['diploma'];
            $student->diploma_year = $data['diploma_year'];
            $student->diploma_mean = $data['diploma_mean'];
            $student->diploma_mention = $data['diploma_mention'];
            $student->branch_id = $data['branch_id'];
            $student->class_id = $data['class_id'];
            $student->status_id = $data['status_id'];
            $student->regime_id = $data['regime_id'];
            $student->second_language = $data['second_language_id'];
            $student->art = $data['art_id'];
            $student->subscription_date = $data['subscription_date'];
            $student->state = $data['state'];
            $student->repeating = $data['repeating'];
            $student->sport = $data['sport'];
            $student->subscription_agent = ucwords(Auth::guard('admin')->user()->username);

            if (null != $req->file('picture_link')) {
                $file = $req->file('picture_link');
                $picture_name = $file->getClientOriginalName();
                $picture_link = "img/students/{$picture_name}";
                $student->picture_link = $picture_link;
            } else {
                $student->picture_link = "";
            }

            if (null != $req->file('other_file_attached')) {
                $other_file = new mFile();
                $attached_file = $req->file('other_file_attached');
                $other_file->name = $attached_file->getClientOriginalName();
                $other_file->link = "img/files/{$other_file->name}";
                $other_file->size = $attached_file->getSize();
                $other_file->extension = $attached_file->getMimeType();
                $other_file->file_type = "Non Renseigné";
                $other_file->owner_id = $data['matricule'];
                if (null != $data['other_file'])
                    $other_file->file_type = $data['other_file'];

                $other_file->save();
                $attached_file->move(public_path() . '/img/files/', $other_file->name);
            }
            //            echo "<pre>"; print_r($student); die;
            $student->save();
            if (null != $req->file('picture_link'))
                $file->move(public_path() . '/img/students/', $picture_name);

            Session::flash('success_message', 'Elève "' . $student->name . '" enregistré !');
            return redirect('admin/subscriptions');
        }
        $students = Student::get();
        return redirect("admin/subscriptions")->with(compact('students'));
    }

    /**
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function createStepTwo(Request $request)
    {
        //        $product = $request->session()->get('product');

        $school = School::get()[0];
        $academicYears = AcademicYear::get();
        $sexes = Sex::get();
        $sections = Section::get();
        $branches = Branch::get();
        $classes = Classe::get();
        $states = State::get();
        $regimes = Regime::get();
        $rds = RequiredDocument::get();
        $arts = Art::get();
        $lv2s = Language::get();

        $levels = Level::get();
        $year = date("Y");
        $uuid = uniqid("", true);
        $uuidpart1 = strtoupper(substr($uuid, 11, 2));
        $uuidpart2 = strtoupper(substr($uuid, 20, 3));
        $matricule = "${year}i${uuidpart1}${uuidpart2}";
        if (!empty($student)) {
            $uuidpart1 = strtoupper(substr($uuid, 11, 2));
            $uuidpart2 = strtoupper(substr($uuid, 20, 3));
            $matricule = "${year}i${uuidpart1}${uuidpart2}";

            $student = Student::where("matricule", $matricule)->first();
        }

        $student_name = "BRAGBO";
        $student_surname = "Jean-Pierre";
        $father_name = "NomTest PrenomTest";
        $father_profession = "ProfessionTest";
        $father_phone = "000000000";
        $birthay_place = "Abidjan";
        $countries = Country::get();
        return view("admin.student.step-two")->with(compact(
            'school',
            'academicYears',
            'sexes',
            'sections',
            'levels',
            'student_name',
            'student_surname',
            'father_name',
            'father_profession',
            'father_phone',
            'birthay_place',
            'matricule',
            'countries',
            'regimes',
            'states',
            'branches',
            'classes',
            'rds',
            'arts',
            'lv2s'
        ));
    }

    public function postCreateStepTwo(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Inscription Simplifiée";
            $student = new Student();
        } else {
            $title = "Modifier un élève";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $student->school_name = $data['school_name'];
            $student->year_id = $data['year_id'];
            $student->matricule = $data['matricule'];
            $student->other_matricule = $data['other_matricule'];
            $student->name = strtoupper($data['name']);
            $student->surname = ucwords($data['surname']);
            $student->birthday_date = $data['birthday_date'];
            $student->birthday_place = $data['birthday_place'];
            $student->nationality = $data['nationality'];
            $student->sex = $data['sex'];
            $student->city = $data['city'];
            $student->neighborhood = $data['neighborhood'];
            $student->phone = $data['phone'];
            $student->email = $data['email'];
            $student->profession = $data['profession'];
            $student->address = $data['address'];

            if ("non" == $data['father_manual']) {
                $student->father_id = $data['father_id'];
            } else {
                $father = new Father();
                $father->name = $data['father_name'];
                $father->work = $data['father_profession'];
                $father->phone = $data['father_phone'];
                $father->email = $data['father_email'];

                //                $father->save();

                $father = Father::latest()->first();
                $student->father_id = $father->id;
            }
            if ("non" == $data['mother_manual'])
                $student->mother_id = $data['mother_id'];
            else {
                $mother = new Mother();
                $mother->name = $data['mother_name'];
                $mother->work = $data['mother_profession'];
                $mother->phone = $data['mother_phone'];
                $mother->email = $data['mother_email'];

                //                $mother->save();

                $mother = Mother::latest()->first();
                $student->mother_id = $mother->id;
            }
            if ("non" == $data['tutor_manual'])
                //                $student->tutor_id = $data['tutor_id'];
                $student->tutor_id = null;
            else {
                $tutor = new Tutor();
                $tutor->name = $data['tutor_name'];
                $tutor->work = $data['tutor_profession'];
                $tutor->phone = $data['tutor_phone'];
                $tutor->email = $data['tutor_email'];

                //                $tutor->save();

                $tutor = Tutor::latest()->first();
                $student->tutor_id = $tutor->id;
            }

            $student->branch_id = $data['branch_id'];
            $student->class_id = $data['class_id'];
            $student->status_id = $data['status_id'];
            $student->regime_id = $data['regime_id'];
            $student->second_language = $data['second_language_id'];
            $student->art = $data['art_id'];
            $student->subscription_date = $data['subscription_date'];
            $student->state = $data['state'];
            $student->repeating = $data['repeating'];
            $student->sport = $data['sport'];
            $student->subscription_agent = ucwords(Auth::guard('admin')->user()->username);

            if (null != $req->file('picture_link')) {
                $file = $req->file('picture_link');
                $picture_name = $file->getClientOriginalName();
                $picture_link = "img/students/{$picture_name}";
                $student->picture_link = $picture_link;
            } else {
                $student->picture_link = "";
            }

            if (null != $req->file('other_file_attached')) {
                $other_file = new mFile();
                $attached_file = $req->file('other_file_attached');
                $other_file->name = $attached_file->getClientOriginalName();
                $other_file->link = "img/files/{$other_file->name}";
                $other_file->size = $attached_file->getSize();
                $other_file->extension = $attached_file->getMimeType();
                $other_file->file_type = "Non Renseigné";
                $other_file->owner_id = $data['matricule'];
                if (null != $data['other_file'])
                    $other_file->file_type = $data['other_file'];

                $other_file->save();
                $attached_file->move(public_path() . '/img/files/', $other_file->name);
            }
            //            echo "<pre>"; print_r($student); die;
            $student->save();
            if (null != $req->file('picture_link'))
                $file->move(public_path() . '/img/students/', $picture_name);

            Session::flash('success_message', 'Elève "' . $student->name . '" enregistré !');
            return redirect('admin/subscriptions');
        }
        $students = Student::get();
        return redirect("admin/subscriptions")->with(compact('students'));
    }

    public function createStepThree(Request $request)
    {
        //        $product = $request->session()->get('product');

        $school = School::get()[0];
        $academicYears = AcademicYear::get();
        $sexes = Sex::get();
        $sections = Section::get();
        $levels = Level::get();
        $found = false;
        $attached_files = [];
        return view("admin.student.step-three")->with(compact('school', 'academicYears', 'sexes', 'sections', 'levels', 'found', 'attached_files'));
    }

    public function postCreateStepThree(Request $req)
    {
        if ($req->isMethod('post')) {

            $data = $req->all();
            echo "<pre>";
            print_r($data);
            die;
            if (null != $data['student_id']) {
                Student::whereId($data['student_id'])->update([
                    'year_id' => $data['year_id'],
                    'other_matricule' => $data['other_matricule'],
                    'name' => $data['name'],
                    'surname' => $data['surname'],
                    'birthday_date' => $data['birthday_date'],
                    'birthday_place' => $data['birthday_place'],
                    'nationality' => $data['nationality'],
                    'sex' => $data['sex'],
                    'city' => $data['city'],
                    'neighborhood' => $data['neighborhood'],
                    'phone' => $data['phone'],
                    'email' => $data['email'],
                    'profession' => $data['profession'],
                    'address' => $data['address'],
                    'marital' => $data['marital'],
                    'phone_partner' => $data['phone_partner'],
                    'email_partner' => $data['email_partner'],
                    'name_partner' => $data['name_partner'],
                    'profession_partner' => $data['profession_partner'],
                    'chronic_disease' => $data['chronic_disease'],
                    'allergy' => $data['allergy'],
                    'medical_observation' => $data['medical_observation'],
                    'child' => $data['child'],
                    'faculty_id' => $data['faculty_id'],
                    'previous_level_id' => $data['previous_level_id'],
                    'diploma' => $data['diploma'],
                    'diploma_year' => $data['diploma_year'],
                    'diploma_mean' => $data['diploma_mean'],
                    'diploma_mention' => $data['diploma_mention'],
                    'branch_id' => $data['branch_id'],
                    'class_id' => $data['class_id'],
                    'status_id' => $data['status_id'],
                    'regime_id' => $data['regime_id'],
                    'second_language' => $data['second_language_id'],
                    'art' => $data['art_id'],
                    'subscription_date' => $data['subscription_date'],
                    'state' => $data['state'],
                    'repeating' => $data['repeating'],
                    'sport' => $data['sport'],
                    'subscription_agent' => ucwords(Auth::guard('admin')->user()->username),
                ]);

                if ("non" == $data['father_manual']) {
                    Student::whereId($data['student_id'])->update(['father_id' => $data['father_id']]);
                } else {
                    $father = new Father();
                    $father->name = $data['father_name'];
                    $father->work = $data['father_profession'];
                    $father->phone = $data['father_phone'];
                    $father->email = $data['father_email'];

                    $father->save();

                    $father = Father::latest()->first();
                    Student::whereId($data['student_id'])->update(['father_id' => $father->id]);
                }

                if ("non" == $data['mother_manual'])
                    Student::whereId($data['student_id'])->update(['mother_id' => $data['mother_id']]);
                else {
                    $mother = new Mother();
                    $mother->name = $data['mother_name'];
                    $mother->work = $data['mother_profession'];
                    $mother->phone = $data['mother_phone'];
                    $mother->email = $data['mother_email'];

                    $mother->save();

                    $mother = Mother::latest()->first();
                    Student::whereId($data['student_id'])->update(['mother_id' => $mother->id]);
                }

                if ("non" == $data['tutor_manual'])
                    Student::whereId($data['student_id'])->update(['tutor_id' => $data['tutor_id']]);
                else {
                    $tutor = new Tutor();
                    $tutor->name = $data['tutor_name'];
                    $tutor->work = $data['tutor_profession'];
                    $tutor->phone = $data['tutor_phone'];
                    $tutor->email = $data['tutor_email'];

                    $tutor->save();

                    $tutor = Tutor::latest()->first();
                    Student::whereId($data['student_id'])->update(['tutor_id' => $tutor->id]);
                }

                if (null != $req->file('picture_link')) {
                    $file = $req->file('picture_link');
                    $picture_name = $file->getClientOriginalName();
                    $picture_link = "img/students/{$picture_name}";
                    $old_picture = mFile::where('owner_id', $data['matricule']);
                    if (File::exists($old_picture->link)) File::delete($old_picture->link);
                    Student::whereId($data['student_id'])->update(['picture_link' => $picture_link]);
                    $file->move(public_path() . '/img/students/', $picture_name);
                }

                if (null != $req->file('other_file_attached')) {
                    $other_file = new mFile();
                    $attached_file = $req->file('other_file_attached');
                    $other_file->name = $attached_file->getClientOriginalName();
                    $other_file->link = "img/files/{$other_file->name}";
                    $other_file->size = $attached_file->getSize();
                    $other_file->extension = $attached_file->getMimeType();
                    $other_file->file_type = "Non Renseigné";
                    $other_file->owner_id = $data['matricule'];
                    if (null != $data['other_file'])
                        $other_file->file_type = $data['other_file'];

                    $other_file->save();
                    $attached_file->move(public_path() . '/img/files/', $other_file->name);
                }
            }

            $student = Student::where('id', $data['student_id']);
            //            echo "<pre>"; print_r($student); die;

            Session::flash('success_message', 'Elève "' . $student->name . '" Modifié !');
            $academicYears = AcademicYear::get();
            $attached_files = mFile::where('owner_id', $student->matricule);
            $fathers = Father::get();
            $mothers = Mother::get();
            $tutors = Tutor::get();
            $sections = Section::get();
            $levels = Level::get();
            $branches = Branch::get();
            $classes = Classe::get();
            $states = State::get();
            $regimes = Regime::get();
            $lv2s = Language::get();
            $arts = Art::get();
            $rds = RequiredDocument::get();
            $countries = Country::get();
            Session::flash('modified', 'information modifié avec succès');
            return view("admin.student.step-three")->with(compact(
                'student',
                'academicYears',
                'rds',
                'attached_files',
                'fathers',
                'mothers',
                'tutors',
                'sections',
                'levels',
                'branches',
                'classes',
                'states',
                'regimes',
                'lv2s',
                'arts',
                'countries'
            ));
        }
        return redirect("admin/subscriptions/create-step-four");
    }

    public function createStepFour(Request $request)
    {
        //        $product = $request->session()->get('product');

        return view("admin.student.step-four");
    }

    public function postCreateStepFour(Request $request)
    {
        /*
        $product = $request->session()->get('product');
        $product->save();

        $request->session()->forget('product');*/

        return redirect("admin/subscriptions");
    }

    public function postFind(Request $req)
    {
        $found = false;
        $student = new Student();
        $attached_files = [];
        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;
            if (!empty($data['matricule'])) {
                $student = Student::where('matricule', $data['matricule'])->first();
                $found = true;
                if (null != $student) {
                    $found = true;
                    $attached_files = mFile::where('owner_id', $student->matricule)->get();
                } else {
                    Session::flash('not_found', 'Aucun resultat trouvé');
                }
                //                echo 'find by matricule <br/>';
                //            echo "<pre>"; print_r($student); die;
            } else {
                $student = Student::where(DB::raw("CONCAT(`name`, ' ', `surname`)"), 'LIKE', '%' . $data['name'] . '%')->first();
                //                echo "<pre>"; print_r($student); die;
                if (null != $student) {
                    $found = true;
                    $attached_files = mFile::where('owner_id', $student->matricule)->get();
                } else {
                    Session::flash('not_found', 'Aucun resultat trouvé');
                }
                //                echo 'find by name <br/>';
            }
            $academicYears = AcademicYear::get();

            $fathers = Father::get();
            $mothers = Mother::get();
            $tutors = Tutor::get();
            $sections = Section::get();
            $levels = Level::get();
            $branches = Branch::get();
            $classes = Classe::get();
            $states = State::get();
            $regimes = Regime::get();
            $lv2s = Language::get();
            $arts = Art::get();
            $rds = RequiredDocument::get();
            $countries = Country::get();
            return view("admin.student.step-three")->with(compact(
                'student',
                'found',
                'academicYears',
                'rds',
                'attached_files',
                'fathers',
                'mothers',
                'tutors',
                'sections',
                'levels',
                'branches',
                'classes',
                'states',
                'regimes',
                'lv2s',
                'arts',
                'countries'
            ));
        }
        return redirect("admin/subscriptions/create-step-three")->with(compact('found'));
    }
    public function postFindCollect(Request $req)
    {
        Session::put('page', 'collects');
        $found = false;
        $student = new Student();
        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;
            if (!empty($data['matricule'])) {
                $student = Student::where('matricule', $data['matricule'])->first();
                $found = true;
                //                echo 'find by matricule <br/>';
                //            echo "<pre>"; print_r($student); die;
            } else {
                $student = Student::where(DB::raw("CONCAT(`name`, ' ', `surname`)"), 'LIKE', '%' . $data['name'] . '%')->first();
                $found = true;
                //                echo 'find by name <br/>';
                //                echo "<pre>"; print_r($student); die;
            }
            $collects = Collect::where('student_id', $student->id)->get();
            $total_total = 0;
            $total_advance = 0;
            $total_rest = 0;
            foreach ($collects as $c) {
                $total_total += (int)$c->total;
                $total_advance += (int)$c->advance;
                $total_rest += (int)$c->rest;
            }

            $academicYears = AcademicYear::get();
            return view("admin.student.collect")->with(compact('student', 'found', 'academicYears', 'collects', 'total_total', 'total_advance', 'total_rest'));
        }
        return redirect("admin/subscriptions/collect")->with(compact('found'));
    }
    public function collect(Request $req, $id = null)
    {
        Session::put('page', 'collects');
        $found = false;
        $student = new Student();
        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;
            if (!empty($data['matricule'])) {
                $student = Student::where('matricule', $data['matricule'])->first();
                $found = true;
                //                echo 'find by matricule <br/>';
                //            echo "<pre>"; print_r($student); die;
            } else {
                $student = Student::where(DB::raw("CONCAT(`name`, ' ', `surname`)"), 'LIKE', '%' . $data['name'] . '%')->first();
                $found = true;
                //                echo 'find by name <br/>';
                //                echo "<pre>"; print_r($student); die;
            }
            $academicYears = AcademicYear::get();
            return view("admin.student.step-three")->with(compact('student', 'found', 'academicYears'));
        }
        if ($req->isMethod('get') && null != $id) {
            $student = Student::find($id)->first();
            $found = true;
            $collects = Collect::where('student_id', $id)->get();
            $total_total = 0;
            $total_advance = 0;
            $total_rest = 0;
            foreach ($collects as $c) {
                $total_total += (int)$c->total;
                $total_advance += (int)$c->advance;
                $total_rest += (int)$c->rest;
            }
            return view("admin.student.collect")->with(compact('found', 'student', 'collects', 'total_total', 'total_advance', 'total_rest'));
        }
        return view("admin.student.collect")->with(compact('found'));
    }

    public function addCollect(Request $req, $id = null)
    {
        if ($req->isMethod('post')) {
            $data = $req->all();
            /* echo "<pre>";
            print_r($data);
            die; */
            $collect = new Collect();

            $collect->matricule_student = $data['matricule'];
            $collect->applicant_name = $data['applicant_name'];
            $collect->applicant_phone = $data['applicant_phone'];
            $collect->applicant_email = $data['applicant_email'];
            $collect->value_date = $data['value_date'];
            $collect->student_id = $data['student_id'];
            $collect->year_id = $data['year_id'];
            $student_id = $data['student_id'];
            $collect->class_id = $data['class_id'];
            $collect->account_id = $data['account_id'];
            $collect->rubric = $data['rubric'];
            $collect->remark = $data['remark'];

            $rubric = Rubric::where("id", $data['rubric'])->first();
            $collect->total = $rubric->amount;
            $total = (int)$rubric->amount;

            $collect->advance = $data['advance'];
            $avance = (int)$data['advance'];
            $collect->rest = $total - $avance;

            /* echo "<pre>";
            print_r($collect);
            die; */
            $collect->save();

            $student = Student::find($student_id);
            $found = true;
            $collects = Collect::where('student_id', $student_id)->get();
            $total_total = 0;
            $total_advance = 0;
            $total_rest = 0;
            foreach ($collects as $c) {
                $total_total += (int)$c->total;
                $total_advance += (int)$c->advance;
                $total_rest += (int)$c->rest;
            }
            return view("admin.student.collect")->with(compact('student', 'found', 'collects', 'total_total', 'total_advance', 'total_rest'));
        }
        $student = Student::find($id);
        $classes = Classe::get();
        $academicYears = AcademicYear::get();
        $rubriques = Rubric::get();
        return view("admin.student.add_collect")->with(compact('student', 'classes', 'academicYears', 'rubriques'));
    }

    public function collectTicketPDF($id)
    {

        //        get Infos
        setlocale(LC_CTYPE, 'fr_FR');
        $school = School::get()->first();
        $currentYear = date("Y");
        $student = Student::find($id);

        //        End

        $this->fpdf = new Fpdf;
        $this->fpdf->AddPage("L", 'A5');
        $this->fpdf->Image('img/scholl_logo.jpg', 75, 15, 50, 30);
        $this->fpdf->SetFont("arial", '', '8');
        $this->fpdf->Text(20, 20, $school->name);
        $this->fpdf->Text(20, 24, $school->address ?? "Adresse");
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(155, 20, iconv('UTF-8', 'ISO-8859-1', "République de cote d'Ivoire"));
        $this->fpdf->Text(155.5, 24, iconv('UTF-8', 'ISO-8859-1', "Union - Discipline - Travail"));
        $this->fpdf->SetFont("arial", '', '8');
        $this->fpdf->Text(20, 28, $school->phone ?? "Téléphone");
        $this->fpdf->Text(20, 32, $school->mail ?? "Email");
        $this->fpdf->Text(20, 36, $school->web_site ?? "Site web");
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(75, 60, "BILLET D'ENTREE " . $currentYear);
        $this->fpdf->SetFont("arial", '', '12');
        $this->fpdf->Text(20, 80, iconv('UTF-8', 'ISO-8859-1', "Nom et prénoms : "));
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(60, 80, iconv('UTF-8', 'ISO-8859-1', $student->name . " " . $student->surname));
        $this->fpdf->SetFont("arial", '', '12');
        $this->fpdf->Text(20, 90, iconv('UTF-8', 'ISO-8859-1', "Né(e) le : "));
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(40, 90, iconv('UTF-8', 'ISO-8859-1', date("d/m/Y", strtotime($student->birthday_date))));
        $this->fpdf->SetFont("arial", '', '12');
        $this->fpdf->Text(120, 90, iconv('UTF-8', 'ISO-8859-1', "à  "));
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(130, 90, iconv('UTF-8', 'ISO-8859-1', $student->birthday_place));
        $this->fpdf->SetFont("arial", '', '12');
        $this->fpdf->Text(20, 100, iconv('UTF-8', 'ISO-8859-1', "Statut : "));
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(50, 100, iconv('UTF-8', 'ISO-8859-1', ($student->status->name ?? " ")));
        $this->fpdf->SetFont("arial", '', '12');
        $this->fpdf->Text(120, 100, iconv('UTF-8', 'ISO-8859-1', "Régime : "));
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(140, 100, iconv('UTF-8', 'ISO-8859-1', ($student->regime->name ?? " ")));
        $this->fpdf->SetFont("arial", '', '12');
        $this->fpdf->Text(20, 110, iconv('UTF-8', 'ISO-8859-1', "Classe : "));
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(40, 110, iconv('UTF-8', 'ISO-8859-1', ($student->classe->name ?? " ")));
        $this->fpdf->SetFont("arial", '', '12');
        $this->fpdf->Text(20, 120, iconv('UTF-8', 'ISO-8859-1', "LV2 : "));
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(40, 120, iconv('UTF-8', 'ISO-8859-1', ($student->second_language ?? " ")));
        $this->fpdf->SetFont("arial", '', '12');
        $this->fpdf->Text(120, 120, iconv('UTF-8', 'ISO-8859-1', "Art : "));
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(140, 120, iconv('UTF-8', 'ISO-8859-1', ($student->art ?? " ")));
        $this->fpdf->SetFont("arial", '', '12');
        $this->fpdf->Text(20, 130, iconv('UTF-8', 'ISO-8859-1', "Inscrit(e) le : "));
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(50, 130, iconv('UTF-8', 'ISO-8859-1', (date("d/m/Y", strtotime($student->subscription_date)) ?? " ")));
        $this->fpdf->Text(20, 140, iconv('UTF-8', 'ISO-8859-1', "Est autorisé(e) à suivre les cours"));


        //        Separator
        $this->fpdf->Output();
        exit;
    }
    public function studentFilePDF($id)
    {

        //        get Infos
        setlocale(LC_CTYPE, 'fr_FR');
        $student = Student::find($id);
        $currentYear = date("Y");
        $class = Classe::where('id', $student->class_id)->first();

        //        End

        $this->fpdf = new Fpdf;
        $this->fpdf->AddPage("P", 'A4');
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->SetTitle('Fiche Individuelle - ' . iconv('UTF-8', 'ISO-8859-1', $student->name . ' ' . $student->surname));
        $this->fpdf->Text(45, 10, "FICHE INDIVIDUELLE D'INSCRIPTION ET DE REINSCRIPTION " . $currentYear);
        //        ETAT CIVIL
        $this->fpdf->Rect(10, 20, 190, 55); // Grand cadre
        $this->fpdf->SetFillColor(217, 217, 217);
        $this->fpdf->Rect(10.5, 20.5, 189, 5, 'F');
        $this->fpdf->SetFont("arial", 'B', '14');
        $this->fpdf->Text(15, 25, "ETAT CIVIL");
        $this->fpdf->Rect(167.5, 21.5, 31, 31);
        if ($student->picture_link) {
            $this->fpdf->Image($student->picture_link, 168, 22, 30, 30);
        } else {
            $this->fpdf->Text(158.5, 42, iconv('UTF-8', 'ISO-8859-1', "Pas de photo"));
            $this->fpdf->Line(150.5, 21.5, 198.5, 62.5);
            $this->fpdf->Line(150.5, 62.5, 198.5, 21.5);
        }
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 33, iconv('UTF-8', 'ISO-8859-1', "Matricule : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 33, iconv('UTF-8', 'ISO-8859-1', $student->matricule ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 40, iconv('UTF-8', 'ISO-8859-1', "Nom : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(30, 40, iconv('UTF-8', 'ISO-8859-1', $student->name ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 45, iconv('UTF-8', 'ISO-8859-1', "Prénoms : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 45, iconv('UTF-8', 'ISO-8859-1', $student->surname ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 50, iconv('UTF-8', 'ISO-8859-1', "Date de naissance : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(50, 50, iconv('UTF-8', 'ISO-8859-1', date("d/m/Y", strtotime($student->birthday_date)) ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(90, 50, iconv('UTF-8', 'ISO-8859-1', "Lieu de naissance : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(125, 50, iconv('UTF-8', 'ISO-8859-1', $student->birthday_place ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 55, iconv('UTF-8', 'ISO-8859-1', "Sexe : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $sex = ($student->sex == "M") ? "Masculin" : "Féminin";
        $this->fpdf->Text(30, 55, iconv('UTF-8', 'ISO-8859-1', $sex));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(90, 55, iconv('UTF-8', 'ISO-8859-1', "Nationalité : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(120, 55, iconv('UTF-8', 'ISO-8859-1', $student->nationality ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 60, iconv('UTF-8', 'ISO-8859-1', "Téléphone : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 60, iconv('UTF-8', 'ISO-8859-1', $student->phone ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(90, 60, iconv('UTF-8', 'ISO-8859-1', "Email : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(105, 60, iconv('UTF-8', 'ISO-8859-1', $student->email ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 65, iconv('UTF-8', 'ISO-8859-1', "Profession : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 65, iconv('UTF-8', 'ISO-8859-1', $student->profession ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 70, iconv('UTF-8', 'ISO-8859-1', "Ville : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(25, 70, iconv('UTF-8', 'ISO-8859-1', $student->city ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(60, 70, iconv('UTF-8', 'ISO-8859-1', "Quartier : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(75, 70, iconv('UTF-8', 'ISO-8859-1', $student->neighborhood ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(125, 70, iconv('UTF-8', 'ISO-8859-1', "Adresse : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(140, 70, iconv('UTF-8', 'ISO-8859-1', $student->address ?? ""));

        //      Personne à contacter
        $this->fpdf->Rect(10, 80, 190, 80); // Grand cadre
        $this->fpdf->SetFillColor(217, 217, 217);
        $this->fpdf->Rect(10.5, 80.5, 189, 5, 'F');
        $this->fpdf->SetFont("arial", 'B', '14');
        $this->fpdf->Text(15, 85, "PERSONNE A CONTACTER");
        $this->fpdf->SetFont("arial", '', '10');
        //        Père
        $this->fpdf->Text(15, 90, iconv('UTF-8', 'ISO-8859-1', "Père"));
        $this->fpdf->Text(15, 95, iconv('UTF-8', 'ISO-8859-1', "Nom : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 95, iconv('UTF-8', 'ISO-8859-1', $student->father->name ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 100, iconv('UTF-8', 'ISO-8859-1', "Téléphone : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 100, iconv('UTF-8', 'ISO-8859-1', $student->father->phone ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(70, 100, iconv('UTF-8', 'ISO-8859-1', "Profession : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(90, 100, iconv('UTF-8', 'ISO-8859-1', $student->father->work ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(130, 100, iconv('UTF-8', 'ISO-8859-1', "Mail : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(140, 100, iconv('UTF-8', 'ISO-8859-1', $student->father->email ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 105, iconv('UTF-8', 'ISO-8859-1', "Ville : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(30, 105, iconv('UTF-8', 'ISO-8859-1', $student->father->phone ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(70, 105, iconv('UTF-8', 'ISO-8859-1', "Quartier : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(85, 105, iconv('UTF-8', 'ISO-8859-1', $student->father->email ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(130, 105, iconv('UTF-8', 'ISO-8859-1', "Adresse : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(146, 105, iconv('UTF-8', 'ISO-8859-1', $student->father->work ?? ""));
        //      Mère
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 115, iconv('UTF-8', 'ISO-8859-1', "Mère"));
        $this->fpdf->Text(15, 120, iconv('UTF-8', 'ISO-8859-1', "Nom : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 120, iconv('UTF-8', 'ISO-8859-1', $student->mother->name ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 125, iconv('UTF-8', 'ISO-8859-1', "Téléphone : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 125, iconv('UTF-8', 'ISO-8859-1', $student->mother->phone ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(70, 125, iconv('UTF-8', 'ISO-8859-1', "Profession : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(90, 125, iconv('UTF-8', 'ISO-8859-1', $student->mother->email ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(130, 125, iconv('UTF-8', 'ISO-8859-1', "Mail : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(140, 125, iconv('UTF-8', 'ISO-8859-1', $student->mother->work ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 130, iconv('UTF-8', 'ISO-8859-1', "Ville : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(30, 130, iconv('UTF-8', 'ISO-8859-1', $student->mother->phone  ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(70, 130, iconv('UTF-8', 'ISO-8859-1', "Quartier : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(85, 130, iconv('UTF-8', 'ISO-8859-1', $student->mother->email  ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(130, 130, iconv('UTF-8', 'ISO-8859-1', "Adresse : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(146, 130, iconv('UTF-8', 'ISO-8859-1', $student->mother->work ?? ""));
        //        Tuteur
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 140, iconv('UTF-8', 'ISO-8859-1', "Tuteur"));
        $this->fpdf->Text(15, 145, iconv('UTF-8', 'ISO-8859-1', "Nom : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 145, iconv('UTF-8', 'ISO-8859-1', $student->tutor->name ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 150, iconv('UTF-8', 'ISO-8859-1', "Téléphone : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 150, iconv('UTF-8', 'ISO-8859-1', $student->tutor->phone ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(70, 150, iconv('UTF-8', 'ISO-8859-1', "Profession : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(90, 150, iconv('UTF-8', 'ISO-8859-1', $student->tutor->email ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(130, 150, iconv('UTF-8', 'ISO-8859-1', "Mail : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(140, 150, iconv('UTF-8', 'ISO-8859-1', $student->tutor->work ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 155, iconv('UTF-8', 'ISO-8859-1', "Ville : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(30, 155, iconv('UTF-8', 'ISO-8859-1', $student->tutor->phone ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(70, 155, iconv('UTF-8', 'ISO-8859-1', "Quartier : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(85, 155, iconv('UTF-8', 'ISO-8859-1', $student->tutor->email ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(130, 155, iconv('UTF-8', 'ISO-8859-1', "Adresse : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(146, 155, iconv('UTF-8', 'ISO-8859-1', ($student->tutor->work ?? "")));
        //      Inscription
        $this->fpdf->Rect(10, 165, 190, 65); // Grand cadre
        $this->fpdf->SetFillColor(217, 217, 217);
        $this->fpdf->Rect(10.5, 165.5, 189, 5, 'F');
        $this->fpdf->SetFont("arial", 'B', '14');
        $this->fpdf->Text(15, 170, "INSCRIPTION");
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 180, iconv('UTF-8', 'ISO-8859-1', "Niveau précèdent : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(50, 180, iconv('UTF-8', 'ISO-8859-1', $student->level->name ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(100, 180, iconv('UTF-8', 'ISO-8859-1', "Ecole d'origine : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(130, 180, iconv('UTF-8', 'ISO-8859-1', $student->old_school ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 185, iconv('UTF-8', 'ISO-8859-1', "Filière : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 185, iconv('UTF-8', 'ISO-8859-1', $student->section->name ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 190, iconv('UTF-8', 'ISO-8859-1', "Niveau : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 190, iconv('UTF-8', 'ISO-8859-1', $class->level->name ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 195, iconv('UTF-8', 'ISO-8859-1', "Classe : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 195, iconv('UTF-8', 'ISO-8859-1', $class->level->name . ' ' . $student->classe->abr ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(100, 195, iconv('UTF-8', 'ISO-8859-1', "Statut : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(130, 195, iconv('UTF-8', 'ISO-8859-1', $student->status->name ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 200, iconv('UTF-8', 'ISO-8859-1', "Régime : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 200, iconv('UTF-8', 'ISO-8859-1', $student->regime->name ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 205, iconv('UTF-8', 'ISO-8859-1', "Date d'inscription : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(50, 205, iconv('UTF-8', 'ISO-8859-1', date("d/m/Y", strtotime($student->subscription_date)) ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 210, iconv('UTF-8', 'ISO-8859-1', "Nouveau : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(40, 210, iconv('UTF-8', 'ISO-8859-1', ($student->state == "new") ? "Oui" : "Non"));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(120, 210, iconv('UTF-8', 'ISO-8859-1', "Ancien : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(140, 210, iconv('UTF-8', 'ISO-8859-1', ($student->state == "new") ? "Non" : "Oui"));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 215, iconv('UTF-8', 'ISO-8859-1', "LV2 : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 215, iconv('UTF-8', 'ISO-8859-1', $student->second_language ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 220, iconv('UTF-8', 'ISO-8859-1', "Art : "));
        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(35, 220, iconv('UTF-8', 'ISO-8859-1', $student->art ?? ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(15, 225, iconv('UTF-8', 'ISO-8859-1', "Redoublant : "));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(60, 225, iconv('UTF-8', 'ISO-8859-1', "OUI "));
        $this->fpdf->Rect(68, 221.5, 5, 5);
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(69, 225, iconv('UTF-8', 'ISO-8859-1', ($student->repeating == "true") ? "X" : ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(100, 225, iconv('UTF-8', 'ISO-8859-1', "NON "));
        $this->fpdf->Rect(109, 221.5, 5, 5);
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(110, 225, iconv('UTF-8', 'ISO-8859-1', ($student->repeating == "false") ? "X" : ""));
        $this->fpdf->SetFont("arial", '', '10');
        $this->fpdf->Text(150, 225, iconv('UTF-8', 'ISO-8859-1', "TRIPLE "));
        $this->fpdf->Rect(163, 221.5, 5, 5);
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(164, 225, iconv('UTF-8', 'ISO-8859-1', ($student->repeating == "triple") ? "X" : ""));


        $this->fpdf->Output();
        exit;
    }
}
