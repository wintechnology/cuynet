<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use App\Models\Category;
use App\Models\Classe;
use App\Models\Course;
use App\Models\Room;
use App\Models\Schedule;
use App\Models\School;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use function PHPUnit\Framework\isEmpty;
use function PHPUnit\Framework\isNull;

class AcademicYearController extends Controller
{
    public function academic_year()
    {
        Session::put('page', 'academic_years');
        $academic_years = AcademicYear::orderBy('updated_at', 'DESC')->get();
        //        $courseSpends = CourseSpend::get();
        //        echo "<pre>";print_r($totalSpendByCourse);die;
        return view('admin.academic.years')->with(compact('academic_years'));
    }
    public function schedules()
    {
        Session::put('page', 'schedules');
        $schedules = Schedule::orderBy('updated_at', 'DESC')->get();
        //        $courseSpends = CourseSpend::get();
        //        echo "<pre>";print_r($totalSpendByCourse);die;
        return view('admin.academic.schedules')->with(compact('schedules'));
    }

    public function addEditYear(Request $req, $id = null)
    {
        Session::put('page', 'academic_years');
        if ($id == "") {
            $title = "Ajouter une année scolaire";
            $academic_year = new AcademicYear();
        } else {
            $title = "Modifier une année scolaire";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; dd($data); die;
            if (null == $data['school_id']) {
                Session::flash('error_message', 'Manque d\'information sur l\'école !');
                return redirect('admin/add-edit-year');
            }

            $academic_year->school_id = $data['school_id'];
            $academic_year->year = $data['scolar_year'];
            $academic_year->start = $data['date_debut'];
            $academic_year->end = $data['date_fin'];
            $academic_year->previous_year = $data['previous_year_id'];
            $academic_year->sequence_amount = $data['sequence_amount'];
            $academic_year->status = $data['status'];

            //            echo "<pre>"; dd($academic_year); die;
            $academic_year->save();

            Session::flash('success_message', 'Année académique "' . $academic_year->year . '" ajoutée avec succès!');
            return redirect('admin/academic_years');
        }

        //        Get all section
        $academic_years = AcademicYear::get();
        $school = School::find(1);
        $currentYear = date("Y");
        $nextYear = date("Y") + 1;
        $scolar = "$currentYear - $nextYear";
        return view('admin.academic.add_edit_year')->with(compact('title', 'academic_years', 'school', 'scolar'));
    }

    public function addEditSchedule(Request $req, $id = null)
    {
        Session::put('page', 'schedules');

        if ($id == "") {
            $title = "Ajouter un emploi de temps";
            $schedule = new Schedule();
        } else {
            $title = "Modifier unn emploi de temps";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; dd($data); die;

            $schedule->classe_id = $data['classe_id'];
            $schedule->course_id = $data['course_id'];
            $schedule->room_id = $data['room_id'];
            $schedule->teacher_id = $data['teacher_id'];
            $schedule->day = $data['day'];
            $schedule->start = $data['start'];
            $schedule->end = $data['end'];
            //            echo "<pre>"; dd($schedule); die;
            $schedule->save();

            Session::flash('success_message', 'Emploi de temps du "' . date("d-m-Y", strtotime($schedule->day)) . '" ajouté avec succès!');
            return redirect('admin/schedules');
        }

        //        Get all section
        $classes = Classe::get();
        $courses = Course::get();
        $rooms = Room::get();
        $teachers = Teacher::get();
        $currentDate = date("d/m/Y");
        return view('admin.academic.add_edit_schedule')->with(compact(
            'title',
            'classes',
            'courses',
            'rooms',
            'teachers',
            'currentDate'
        ));
    }
}
