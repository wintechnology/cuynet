<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Country;
use App\Models\CourseCategory;
use App\Models\School;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SchoolController extends Controller
{
    public function addEditSchool(Request $req, $id = null)
    {
        Session::put('page', 'schools');
        if ($id == "") {
            $title = "Ajouter une école";
            $school = new School();
        } else {
            $title = "Modifier une école";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            if(null != $req->file('logo')){
                $file = $req->file('logo');
                $picture_name = $file->getClientOriginalName();
                $picture_link = "img/school/{$picture_name}";
                $school->logo = $picture_link;

            }else{
                $school->logo = "";
            }

            if(null != $data['id']){
                School::whereId($data['id'])->update([
                    'code' => $data['code'],
            'ministry_code' => $data['ministry_code'],
            'abreviation' => $data['abreviation'],
            'name' => $data['name'],
            'country_id' => $data['country_id'],
            'city' => $data['city'],
            'neighborhood' => $data['neighborhood'],
            'phone' => $data['phone'],
            'postal_address' => $data['postal_address'],
            'geographic_situation' => $data['geographic_situation'],
            'web_site' => $data['web_site'],
            'email' => $data['email'],
            'currency' => $data['currency'],
            'founder' => $data['founder'],
            'studies_director' => $data['studies_director'],
            'logo' => $picture_link
                ]);
            } else {
                $school->save();
            }
            if(null != $req->file('logo'))
                $file->move(public_path() . '/img/school/', $picture_name);

            Session::flash('success_message', 'Ecole "'.$school->code.'" enregistrée !');
            return redirect('admin/add-edit-school');
        }

        //        Get all information
        $school = School::get()->first();
        $self_country = Country::where('id', $school['country_id'])->first();
        $countries = Country::get();
        return view('admin.settings.schools')->with(compact('title', 'school','countries', 'self_country'));
    }
}
