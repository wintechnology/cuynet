<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use App\Models\AnnexeFee;
use App\Models\Art;
use App\Models\Branch;
use App\Models\BudgetLine;
use App\Models\Classe;
use App\Models\Language;
use App\Models\Option;
use App\Models\Redoublant;
use App\Models\Regime;
use App\Models\RequiredDocument;
use App\Models\Rubric;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BranchController extends Controller
{
    public function branches()
    {
        Session::put('page', 'branches');
        $branches = Branch::get();
        $branches_name = [];
        foreach ($branches as $branch) {
            $branches_name[] = $branch->name;
        }
        return view('admin.settings.branches')->with(compact('branches', 'branches_name'));
    }

    public function options()
    {
        Session::put('page', 'options');
        $options = Option::get();
        return view('admin.settings.options')->with(compact('options'));
    }

    public function regimes()
    {
        Session::put('page', 'regimes');
        $regimes = Regime::get();
        return view('admin.settings.regimes')->with(compact('regimes'));
    }

    public function states()
    {
        Session::put('page', 'states');
        $states = State::get();
        return view('admin.settings.states')->with(compact('states'));
    }

    public function rubrics()
    {
        Session::put('page', 'rubrics');
        $rubrics = Rubric::orderBy('year_id', 'ASC')->get();
        $rubrics_name = [];
        foreach ($rubrics as $rubric) {
            $rubrics_name[] = strtoupper($rubric->name);
        }
        $classes = Classe::get();
        return view('admin.settings.rubrics')->with(compact('rubrics', 'classes', 'rubrics_name'));
    }

    public function addEditBranch(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter une branche";
            $branches = new Branch();
        } else {
            $title = "Modifier une branche";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $branches->name = $data['name'];
            $branches->save();

            $branches = Branch::get();
            $branches_name = [];
            foreach ($branches as $branch) {
                $branches_name[] = $branch->name;
            }
            Session::flash('success_message', 'Branche "' . $branches->name . '" enregistré !');
            return redirect('admin/branches')->with(compact('title', 'branches_name'));
        }

        $branches = Branch::get();
        $branches_name = [];
        foreach ($branches as $branch) {
            $branches_name[] = $branch->name;
        }
        return view('admin.settings.branches')->with(compact('title', 'branches_name'));
    }
    public function addEditOption(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter une option";
            $options = new Option();
        } else {
            $title = "Modifier une option";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $options->name = $data['name'];
            $options->save();

            Session::flash('success_message', 'Option "' . $options->name . '" enregistré !');
            return redirect('admin/options');
        }
        return view('admin.settings.options')->with(compact('title'));
    }
    public function addEditRegime(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un regime";
            $regimes = new Regime();
        } else {
            $title = "Modifier un regime";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $regimes->abr = $data['abr'];
            $regimes->name = $data['name'];
            $regimes->save();

            Session::flash('success_message', 'Regime "' . $regimes->name . '" enregistré !');
            return redirect('admin/regimes');
        }
        return view('admin.settings.regimes')->with(compact('title'));
    }
    public function addEditState(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un statut";
            $state = new State();
        } else {
            $title = "Modifier un statut";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $state->abr = $data['abr'];
            $state->name = $data['name'];
            $state->save();

            Session::flash('success_message', 'Statut "' . $state->name . '" enregistré !');
            return redirect('admin/states');
        }
        return view('admin.settings.states')->with(compact('title'));
    }
    public function addEditLv2(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter une langue";
            $lv2 = new Language();
        } else {
            $title = "Modifier une langue";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $lv2->abr = $data['abr'];
            $lv2->name = $data['name'];
            $lv2->save();

            $lv2s = Language::get();
            Session::flash('success_message', 'Lv2 "' . $lv2->name . '" enregistré !');
            return redirect('admin/add-edit-lv2')->with(compact('title', 'lv2s'));
        }
        $lv2s = Language::get();
        return view('admin.settings.lv2')->with(compact('title', 'lv2s'));
    }
    public function addEditArt(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un art";
            $art = new Art();
        } else {
            $title = "Modifier un art";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $art->abr = $data['abr'];
            $art->name = $data['name'];
            $art->save();

            $arts = Art::get();
            Session::flash('success_message', 'Art "' . $art->name . '" enregistré !');
            return redirect('admin/add-edit-art')->with(compact('title', 'arts'));
        }
        $arts = Art::get();
        return view('admin.settings.arts')->with(compact('title', 'arts'));
    }
    public function addEditRequiredDocument(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un document à fournir";
            $rd = new RequiredDocument();
        } else {
            $title = "Modifier un document à fournir";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $rd->abr = $data['abr'];
            $rd->name = $data['name'];
            $rd->save();

            Session::flash('success_message', 'Document à fournir "' . $rd->name . '" enregistré !');
            $rds = RequiredDocument::get();
            return redirect('admin/add-edit-required_document')->with(compact('title', 'rds'));
        }
        $rds = RequiredDocument::get();
        return view('admin.settings.required_documents')->with(compact('title', 'rds'));
    }

    public function addEditRubric(Request $req, $id = null)
    {
        Session::put('page', 'rubrics');

        if ($id == "") {
            $title = "Ajouter une rubrique";
            $rubric = new Rubric();
        } else {
            $title = "Modifier une rubrique";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            /* echo "<pre>";
            print_r($data);
            die; */

            $rubric->class_id = $data['class_id'];
            $rubric->year_id = $data['year_id'];
            $rubric->name = $data['name'];
            $rubric->abr = $data['abr'];
            $rubric->amount = $data['amount'];
            $rubric->actif = $data['actif'];
            $rubric->student_required = $data['student_required'];
            $rubric->budget_line_id = $data['budget_line_id'];
            $rubric->save();

            Session::flash('success_message', 'Rubrique "' . $rubric->name . '" enregistré !');
            $rubrics = RequiredDocument::get();
            $rubrics_name = [];
            foreach ($rubrics as $rubric) {
                $rubrics_name[] = $rubric->name;
            }
            $classes = Classe::get();
            $budget_lines = BudgetLine::get();
            return redirect('admin/rubrics')->with(compact('rubrics', 'classes', 'budget_lines'));
        }

        $academicYears = AcademicYear::get();
        $rubrics = Rubric::get();
        $rubrics_name = [];
        foreach ($rubrics as $rubric) {
            $rubrics_name[] = strtoupper($rubric->name);
        }
        $classes = Classe::get();
        $budget_lines = BudgetLine::get();
        return view('admin.settings.add_edit_rubric')->with(compact('title', 'rubrics', 'classes', 'rubrics_name', 'academicYears', 'budget_lines'));
    }
    public function addEditAnnexeFee(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un frais annexe";
            $annexe_fee = new AnnexeFee();
        } else {
            $title = "Modifier un frais annexe";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; print_r($data); die;

            $annexe_fee->class_id = $data['class_id'];
            $annexe_fee->name = $data['name'];
            $annexe_fee->amount = $data['amount'];
            $annexe_fee->save();

            Session::flash('success_message', 'Frais annexe "' . $annexe_fee->name . '" enregistré !');
            $annexe_fees = AnnexeFee::get();
            $classes = Classe::get();
            return redirect('admin/add-edit-annexe_fee')->with(compact('title', 'annexe_fees', 'classes'));
        }
        $annexe_fees = AnnexeFee::get();
        $classes = Classe::get();
        return view('admin.settings.annexe_fees')->with(compact('title', 'annexe_fees', 'classes'));
    }
}
