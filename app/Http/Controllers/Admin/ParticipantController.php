<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Models\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ParticipantController extends Controller
{
    public function participant(){
        Session::put('page','participants');
        $participants = Participant::with(['company'])->get();
        return view('admin.participant.participants')->with(compact('participants'));
    }

    public function addEditParticipant(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un participant";
            $participant = new Participant();
        } else {
            $title = "Modifier un participant";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; dd($data); die;

//            if (empty($data['formation_goal'])) {
//                $data['formation_goal'] = "";
//            }
//            if (empty($data['meta_title'])) {
//                $data['meta_title'] = "";
//            }
//            if (empty($data['meta_description'])) {
//                $data['meta_description'] = "";
//            }
//            if (empty($data['meta_keywords'])) {
//                $data['meta_keywords'] = "";
//            }
//            if (empty($data['category_url'])) {
//                $data['category_url'] = strtolower($data['category_name']);
//            }


            $participant->names = strtoupper($data['names']);
            $participant->surnames = ucwords($data['surnames']);
            $participant->birthday = $data['birthday'];
            $participant->birthplace = $data['birthplace'];
            $participant->company_id = $data['company_id'];
            $participant->profession = $data['profession'];
            $participant->function = $data['function'];
            $participant->phone = $data['phone'];
            $participant->email = $data['email'];
            $participant->save();

            Session::flash('success_message', 'Participant "'.strtoupper($data['names']).' '.ucwords($data['surnames']).'" ajoutée avec succès!');
            return redirect('admin/participants');
        }

        //        Get all Features
        $getMembers = Member::get();
        $date = date("d/m/Y");
        return view('admin.participant.add_edit_participant')->with(compact('title', 'getMembers', 'date'));
    }
}
