<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use App\Models\CourseParticipant;
use App\Models\CourseSpend;
use App\Models\CourseStakeholder;
use App\Models\Participant;
use App\Models\Stakeholder;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Http\Request;

class CourseReportPDF extends Controller
{
    private $fpdf;

    public function __construct()
    {

    }

    function getRandomString($n) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    public function courseReportPDF($id)
    {

//        get Infos
        setlocale(LC_CTYPE, 'fr_FR');

        $course = AcademicYear::where('id', $id)->first();
        $reportNumber = "RAP".$this->getRandomString(8).''.$course->id;
        $courses = AcademicYear::get();
        $courseSpends = CourseSpend::where('course_id', $id)->get();

        foreach ($courses as $course){
            $total_amount = 0;
            foreach ($courseSpends as $courseSpend){
                $total_amount += $courseSpend->total_amount;
            }
        }

        $stakeholders = [];
        $courseStakeholders = CourseStakeholder::where('course_id', $id)->with('course')->get();
        foreach ($courseStakeholders as $courseStakeholder){
            $stakeholders[] = Stakeholder::find($courseStakeholder->stakeholder_id);
        }

        $courseParticipants = CourseParticipant::where('course_id', $id)->with('course')->get();
        $participants = array();
        foreach ($courseParticipants as $courseParticipant){
            $participants[] = Participant::find($courseParticipant->participant_id);
        }
//        End

        $this->fpdf = new Fpdf;
        $this->fpdf->AddPage("P", 'A4');
        $this->fpdf->Image('img/admin_images/OIP.jpg',75,5,50,25);
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(55, 38, "RAPPORT DE FORMATION No : ".$reportNumber);
        $this->fpdf->SetFont("arial", '', '8');
        $this->fpdf->Text(20, 50, "Theme : ".iconv('UTF-8', 'ISO-8859-1', $course->formation->theme));
        $this->fpdf->Text(150, 50, "Date : du ". date("d/m/Y", strtotime($course->start))." au ".date("d/m/Y", strtotime($course->end)));

        $this->fpdf->Text(20, 60, "Lieu : ".iconv('UTF-8', 'ISO-8859-1', $course->place));
        $this->fpdf->Text(120, 60, "Budget : ".number_format($total_amount,0,'.',' ')." Fcfa");

        $this->fpdf->Text(20, 70, "Nombre de participants : ".count($participants));
        $this->fpdf->Text(120, 70, "Nombre d'intervenants : ".count($stakeholders));

        $this->fpdf->Text(20, 80, "Objet : ".iconv('UTF-8', 'ISO-8859-1', $course->formation->goal));

        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Text(80, 95, "LISTE DES INTERVENANTS");

//        Separator

        $this->fpdf->Cell(45,90,'',0,1);

//        Tab
        $this->fpdf->Cell(100,8,'NOMS ET PRENOMS',1,0,'C');
        $this->fpdf->Cell(45,8,'TELEPHONE',1,0,'C');
        $this->fpdf->Cell(45,8,'ADRESSE EMAIL',1,1,'C');
//      Content

        $this->fpdf->SetFont("arial", '', '8');
        foreach ($stakeholders as $stakeholder){
            $this->fpdf->Cell(100,5,iconv('UTF-8', 'ISO-8859-1', $stakeholder->names.' '.$stakeholder->surnames),1,0);
            $this->fpdf->Cell(45,5,$stakeholder->phone,1,0);
            $this->fpdf->Cell(45,5,$stakeholder->email,1,1);
        }

        //        Separator

        $this->fpdf->Cell(45,10,'',0,1);

        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Cell(180,10,'LISTE DES PARTICIPANTS',0,1,'C');

//        Tab
        $this->fpdf->Cell(100,8,'NOMS ET PRENOMS',1,0,'C');
        $this->fpdf->Cell(45,8,'TELEPHONE',1,0,'C');
        $this->fpdf->Cell(45,8,'ADRESSE EMAIL',1,1,'C');
//      Content

        $this->fpdf->SetFont("arial", '', '8');
        foreach ($participants as $participant){
            $this->fpdf->Cell(100,5,iconv('UTF-8', 'ISO-8859-1', $participant->names.' '.$participant->surnames),1,0);
            $this->fpdf->Cell(45,5,$participant->phone,1,0);
            $this->fpdf->Cell(45,5,$participant->email,1,1);
        }




        $this->fpdf->Output();
        exit;
    }
}
