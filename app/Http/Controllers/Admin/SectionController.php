<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cycle;
use App\Models\Section;
use App\Models\SectionType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class SectionController extends Controller
{
    public function section()
    {
        Session::put('page', 'sections');
        $sections = Section::orderBy('updated_at', 'DESC')->get();
        return view('admin.section.section')->with(compact('sections'));
    }

    public function cycles()
    {
        Session::put('page', 'cycles');
        $cycles = Cycle::orderBy('updated_at', 'DESC')->get();
        $cycles_name = [];
        foreach ($cycles as $cycle) {
            $cycles_name[] = strtoupper($cycle->name);
        }
        return view('admin.section.cycle')->with(compact('cycles', 'cycles_name'));
    }

    public function addEditSection(Request $req, $id = null)
    {
        Session::put('page', 'sections');
        if ($id == "") {
            $title = "Ajouter une filière\section";
            $section = new Section();
        } else {
            $title = "Modifier une filière\section";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; dd($data); die;

            $section->type_id = $data['type_id'];
            $section->code = $data['code'];
            $section->name = $data['name'];
            $section->cycle_id = $data['cycle_id'];

            //            echo "<pre>"; dd($section); die;
            $section->save();

            Session::flash('success_message', 'Section "' . $section->name . '" ajoutée avec succès!');
            return redirect('admin/sections');
        }

        //        Get all section
        $getTypes = SectionType::get();
        $cycles = Cycle::get();
        return view('admin.section.add_edit_section')->with(compact('title', 'getTypes', 'cycles'));
    }
    public function addEditCycle(Request $req, $id = null)
    {
        Session::put('page', 'cycles');
        if ($id == "") {
            $title = "Ajouter un cycle";
            $cycle = new Cycle();
        } else {
            $title = "Modifier un cycle";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; dd($data); die;

            $cycle->name = $data['name'];
            $redirected = $data['link'];

            //            echo "<pre>"; dd($section); die;
            $cycle->save();

            Session::flash('success_message', 'Cycle "' . $cycle->name . '" ajoutée avec succès!');
            if ($redirected == 1) {
                $cycles = Cycle::get();
                $cycles_name = [];
                foreach ($cycles as $cycle) {
                    $cycles_name[] = strtoupper($cycle->name);
                }
                return redirect('admin/cycles')->with(compact('title', 'cycles', 'cycles_name'));
            } else
                return Redirect::back()->with('success_message', 'Cycle "' . $cycle->name . '" ajoutée avec succès!');
        }

        //        Get all section
        $getTypes = SectionType::get();
        $cycles = Cycle::get();
        $cycles_name = [];
        foreach ($cycles as $cycle) {
            $cycles_name[] = strtoupper($cycle->name);
        }
        return view('admin.section.add_edit_section')->with(compact('title', 'cycles', 'cycles_name'));
    }
}
