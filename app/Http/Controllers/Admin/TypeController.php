<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\RoomType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TypeController extends Controller
{
    public function type(){
        Session::put('page','types');
        $roomsTypes = RoomType::get();
        return view('admin.settings.types')->with(compact('roomsTypes'));
    }

    public function addEditRoomsType(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un type de salle";
            $roomsType = new RoomType();
        } else {
            $title = "Modifier un type de salle";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $roomsType->name = $data['name'];

            $roomsType->save();

            Session::flash('success_message', 'Type de salle "'.$roomsType->name.'" enregistré !');
            return redirect('admin/types');
        }

        //        Get all section
        $roomsTypes = RoomType::get();
        return view('admin.settings.types')->with(compact('title', 'roomsTypes'));
    }
}
