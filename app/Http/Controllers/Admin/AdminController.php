<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use App\Models\CourseSpend;
use App\Models\Formation;
use App\Models\Member;
use App\Models\Participant;
use App\Models\Stakeholder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function login(Request $req){
        if($req->isMethod('post')){
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $rules = [
                'email' => 'required|email|max:255',
                'password' => 'required',
            ];

            $customMessages = [
                'email.required' => 'Email incorrect',
                'email.email' => 'Format email non valide',
                'password.required' => 'Mot de passe incorrect',
            ];

            $this->validate($req, $rules, $customMessages);

            if(Auth::guard('admin')->attempt(['email' => $data['email'], 'password' =>$data['password']])){
                Session::put('image',Auth::guard('admin')->user()->image);
                return redirect('admin/dashboard');
            }else{
                Session::flash('error_message','Email ou mot de passe incorrect.');
                return redirect()->back();
            }
        }
        return view('admin.login');
    }

    public function logout(){
        Auth::guard('admin')->logout();;
        return redirect('admin');
    }

    public function dashboard(){
        Session::put('page','dashboard');
        return view('admin.dashboard');
    }
}
