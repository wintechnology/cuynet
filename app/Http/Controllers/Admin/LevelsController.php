<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Level;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LevelsController extends Controller
{
    public function level(){
        Session::put('page','levels');
        $levels = Level::get();
        return view('admin.settings.levels')->with(compact('levels'));
    }

    public function addEditLevel(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un niveau";
            $level = new Level();
        } else {
            $title = "Modifier un level";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $level->name = $data['name'];
            $level->amount = $data['amount'];

            $level->save();

            Session::flash('success_message', 'Niveau "'.$level->name.' ('.$level->amount.' Xaf)" enregistré !');
            return redirect('admin/levels');
        }
        return view('admin.settings.levels')->with(compact('title'));
    }
}
