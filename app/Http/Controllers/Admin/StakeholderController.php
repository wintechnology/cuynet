<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Models\Stakeholder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class StakeholderController extends Controller
{
    public function stakeholder(){
        Session::put('page','stakeholders');
        $stakeholders = Stakeholder::get();
        return view('admin.stakeholder.stakeholders')->with(compact('stakeholders'));
    }

    public function addEditStakeholder(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un intervenant";
            $stakeholder = new Stakeholder();
        } else {
            $title = "Modifier un intervenant";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; dd($data); die;

//            if (empty($data['formation_goal'])) {
//                $data['formation_goal'] = "";
//            }
//            if (empty($data['meta_title'])) {
//                $data['meta_title'] = "";
//            }
//            if (empty($data['meta_description'])) {
//                $data['meta_description'] = "";
//            }
//            if (empty($data['meta_keywords'])) {
//                $data['meta_keywords'] = "";
//            }
//            if (empty($data['category_url'])) {
//                $data['category_url'] = strtolower($data['category_name']);
//            }


            $stakeholder->names = strtoupper($data['names']);
            $stakeholder->surnames = ucwords($data['surnames']);
            $stakeholder->birthday = $data['birthday'];
            $stakeholder->birthplace = $data['birthplace'];
            $stakeholder->company_id = $data['company_id'];
            $stakeholder->profession = $data['profession'];
            $stakeholder->function = $data['function'];
            $stakeholder->phone = $data['phone'];
            $stakeholder->email = $data['email'];
            $stakeholder->save();

            Session::flash('success_message', 'Intervenant "'.strtoupper($data['names']).' '.ucwords($data['surnames']).'" ajoutée avec succès!');
            return redirect('admin/stakeholders');
        }

        //        Get all Features
        $getMembers = Member::get();
        $date = date("d/m/Y");
        return view('admin.stakeholder.add_edit_stakeholder')->with(compact('title', 'getMembers', 'date'));
    }
}
