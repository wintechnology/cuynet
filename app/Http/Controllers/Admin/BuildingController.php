<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Building;
use App\Models\City;
use App\Models\Country;
use App\Models\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BuildingController extends Controller
{
    public function building(){
        Session::put('page','buildings');
        $buildings = Building::get();
        $sites = Site::get();
        return view('admin.settings.building')->with(compact('buildings', 'sites'));
    }

    public function addEditBuilding(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un bâtiment";
            $building = new Building();
        } else {
            $title = "Modifier un bâtiment";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $building->code = $data['code'];
            $building->site_id = $data['site_id'];
            $building->name = $data['name'];

            $building->save();

            Session::flash('success_message', 'Bâtiment "'.$building->name.'" enregistré !');
            return redirect('admin/buildings');
        }

        //        Get all section
        $cities = City::get();
        $sites = Site::get();
        return view('admin.settings.buildings')->with(compact('title', 'cities','sites'));
    }
}
