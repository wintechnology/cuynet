<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use App\Models\CourseSpend;
use App\Models\Formation;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Http\Request;
use NumberFormatter;

class CourseSpendInvoicePDF extends Controller
{
    private $fpdf;

    public function __construct()
    {

    }

    function getRandomString($n) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    public function courseSpendInvoicePDF($id)
    {

//        get Infos
        setlocale(LC_CTYPE, 'fr_FR');

        $course = AcademicYear::where('id', $id)->first();
        $reportNumber = "FAC".$this->getRandomString(8)."".$course->id;
        $courseSpends = CourseSpend::where('course_id', $id)->with('parentcourse')->get();
//        echo $courseSpends;die;
//        echo '<pre>';print_r($courseSpends[0]->parentcourse);die;
        $formation = Formation::find($courseSpends[0]->parentcourse->formation_id)->first();
//        End

        $fmf = new NumberFormatter('fr_FR', NumberFormatter::SPELLOUT);

        $this->fpdf = new Fpdf;
        $this->fpdf->AddPage("P", 'A4');
        $this->fpdf->Image('img/admin_images/OIP.jpg',20,5,50,25);
        $this->fpdf->SetFont("arial", 'B', '12');
        $this->fpdf->Text(90, 10, "FACTURE DES DEPENSES ".iconv('UTF-8', 'ISO-8859-1', "N°").": ".$reportNumber);
        $this->fpdf->SetFont("arial", '', '8');
        $this->fpdf->Text(90, 20, "Session de formation : ".iconv('UTF-8', 'ISO-8859-1', $course->formation->theme));
        $this->fpdf->Text(90, 30, "Date : du ". date("d/m/Y", strtotime($course->start))." au ".date("d/m/Y", strtotime($course->end)));
        $this->fpdf->line(10,35,200,35);

//        Separator

        $this->fpdf->Cell(45,30,'',0,1);

//        Tab
        $this->fpdf->SetFont("arial", 'B', '11');
        $this->fpdf->Cell(90,8,'DESIGNATION',1,0,'C');
        $this->fpdf->Cell(20,8,'QTE',1,0,'C');
        $this->fpdf->Cell(40,8,'P.U',1,0,'C');
        $this->fpdf->Cell(40,8,'P.T',1,1,'C');
//      Content
        $total_amount = 0;
        $total_total_amount = 0;
        $this->fpdf->SetFont("arial", '', '8');
        foreach ($courseSpends as $courseSpend){
            $this->fpdf->Cell(90,5,iconv('UTF-8', 'ISO-8859-1', $courseSpend->designation),1,0);
            $this->fpdf->Cell(20,5,$courseSpend->quantity,1,0);
            $this->fpdf->Cell(40,5,number_format($courseSpend->unit_amount,0,'.', ' '),1,0);
            $total_amount = $courseSpend->quantity * $courseSpend->unit_amount;
            $this->fpdf->Cell(40,5,number_format($total_amount,0,'.', ' '),1,1);
            $total_total_amount += $total_amount;
            $total_amount =0;
        }

        //        Separator

        $this->fpdf->SetFont("arial", 'B', '10');
        $this->fpdf->Cell(110,8,'',0,0,'R',true);
        $this->fpdf->Cell(40,8,'TOTAL',0,0,'C');
        $this->fpdf->Cell(40,8,number_format($total_total_amount,0,'.', ' ')." Fcfa",1,1,'R');
        $this->fpdf->Cell(190,8,ucwords($fmf->format($total_total_amount))." Fcfa",1,1,'C');


        $this->fpdf->Cell(5,10,'',0,1);

        $this->fpdf->Output();
        exit;
    }
}
