<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Borough;
use App\Models\Country;
use App\Models\Departement;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CountryController extends Controller
{
    public function countries(){
        Session::put('page','countries');
        $countries = Country::get();
        return view('admin.settings.countries')->with(compact('countries'));
    }
    public function regions(){
        Session::put('page','regions');
        $regions = Region::get();
        return view('admin.settings.regions')->with(compact('regions'));
    }
    public function departements(){
        Session::put('page','departements');
        $departements = Departement::get();
        return view('admin.settings.departements')->with(compact('departements'));
    }
    public function boroughs(){
        Session::put('page','boroughs');
        $arrondissements = Borough::get();
        return view('admin.settings.boroughs')->with(compact('arrondissements'));
    }

    public function addEditCountry(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un pays";
            $countrie = new Country();
        } else {
            $title = "Modifier un pays";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $countrie->name = $data['name'];
            $countrie->code = $data['code'];
            $countrie->nationality = $data['nationality'];

            $countrie->save();

            Session::flash('success_message', 'Pays "'.$countrie->name.'" enregistré !');
            return redirect('admin/countries');
        }
        return view('admin.settings.countries')->with(compact('title'));
    }
    public function addEditRegion(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter une region";
            $regions = new Region();
        } else {
            $title = "Modifier une region";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $regions->name = $data['name'];
            $regions->code = $data['code'];

            $regions->save();

            Session::flash('success_message', 'Region "'.$regions->name.'" enregistré !');
            return redirect('admin/regions');
        }
        return view('admin.settings.regions')->with(compact('title'));
    }
    public function addEditDepartement(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un département";
            $departements = new Departement();
        } else {
            $title = "Modifier un département";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $departements->name = $data['name'];
            $departements->code = $data['code'];
            $departements->save();

            Session::flash('success_message', 'Département "'.$departements->name.'" enregistré !');
            return redirect('admin/departments');
        }
        return view('admin.settings.departements')->with(compact('title'));
    }
    public function addEditBorough(Request $req, $id = null)
    {
        if ($id == "") {
            $title = "Ajouter un arrondissement";
            $boroughs = new Borough();
        } else {
            $title = "Modifier un arrondissement";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
//            echo "<pre>"; print_r($data); die;

            $boroughs->name = $data['name'];
            $boroughs->code = $data['code'];
            $boroughs->save();

            Session::flash('success_message', 'Arrondissement "'.$boroughs->name.'" enregistré !');
            return redirect('admin/boroughs');
        }
        return view('admin.settings.boroughs')->with(compact('title'));
    }
}
