<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use App\Models\Branch;
use App\Models\Building;
use App\Models\Classe;
use App\Models\Formation;
use App\Models\Level;
use App\Models\Room;
use App\Models\School;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ClasseController extends Controller
{
    public function classe()
    {
        Session::put('page', 'classes');
        $classes = Classe::orderBy('updated_at', 'DESC')->get();
        return view('admin.classe.classes')->with(compact('classes'));
    }

    public function addEditClasse(Request $req, $id = null)
    {
        Session::put('page', 'classes');
        if ($id == "") {
            $title = "Ajouter une classe";
            $classe = new Classe();
        } else {
            $title = "Modifier une classe";
        }

        if ($req->isMethod('post')) {
            $data = $req->all();
            //            echo "<pre>"; dd($data); die;

            $classe->school_name = $data['school_name'];
            $classe->year_id = $data['year_id'];
            $classe->abr = $data['abr'];
            $classe->description = $data['description'];
            $classe->branch_id = $data['branch_id'];
            $classe->tutel = $data['tutel'];
            $classe->department = $data['department'];
            $classe->section_id = $data['section_id'];
            $classe->level_id = $data['level_id'];
            $classe->formation = $data['formation'];
            $classe->room_id = $data['room_id'];
            $classe->max = $data['max'];
            $classe->optional = $data['optional'];
            $classe->option = $data['option'];
            $classe->actif = $data['actif'];
            $classe->exam_class = $data['exam_class'];
            $classe->limit_age = $data['limit_age'];
            $classe->simple_hourly_rate = $data['simple_hourly_rate'];
            $classe->multiple_hourly_rate = $data['multiple_hourly_rate'];

            //            echo "<pre>"; dd($classe); die;
            $classe->save();

            Session::flash('success_message', 'Classe "' . $classe->name . '" ajoutée avec succès!');
            return redirect('admin/classes');
        }

        //        Get all classes
        $classes = Classe::orderBy('updated_at', 'DESC')->get();
        $classesAbr = [];
        foreach ($classes as $class) {
            $classesAbr[] = $class->level_id . '' . $class->abr . '' . $class->section_id;
        }
        $branches = Branch::orderBy('updated_at', 'DESC')->get();
        $sections = Section::orderBy('updated_at', 'DESC')->get();
        $rooms = Room::orderBy('updated_at', 'DESC')->get();
        $buildings = Building::orderBy('updated_at', 'DESC')->get();
        $levels = Level::orderBy('updated_at', 'DESC')->get();
        $school = School::get()[0];
        $academicYears = AcademicYear::get();
        $typeFormations = Formation::get();
        return view('admin.classe.add_edit_classe')->with(compact('title', 'classes', 'sections', 'rooms', 'buildings', 'levels', 'school', 'academicYears', 'branches', 'classesAbr', 'typeFormations'));
    }
}
