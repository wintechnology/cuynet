<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    use HasFactory;
    public function company()
    {
        return $this->belongsTo('App\Models\Member', 'company_id')->select('id','company_name');
    }
}
