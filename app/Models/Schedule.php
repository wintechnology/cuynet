<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;
    public function classe()
    {
        return $this->belongsTo('App\Models\Classe', 'classe_id')->select('id','name');
    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course', 'course_id')->select('id','name');
    }

    public function room()
    {
        return $this->belongsTo('App\Models\Room', 'room_id')->select('id','name');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher', 'teacher_id')->select('id','name');
    }
}
