<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    use HasFactory;
    public function site()
    {
        return $this->belongsTo('App\Models\Site', 'site_id')->select('id', 'code','name');
    }
}
