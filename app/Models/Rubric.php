<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rubric extends Model
{
    use HasFactory;
    public function class()
    {
        return $this->belongsTo('App\Models\Classe', 'class_id')->select('id', 'abr', 'level_id');
    }
    public function aca_year()
    {
        return $this->belongsTo('App\Models\AcademicYear', 'year_id')->select('id', 'year');
    }
    public function budget_line()
    {
        return $this->belongsTo('App\Models\BudgetLine', 'budget_line_id')->select('id', 'name');
    }
}
