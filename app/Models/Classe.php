<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classe extends Model
{
    use HasFactory;
    public function section()
    {
        return $this->belongsTo('App\Models\Section', 'section_id')->select('id', 'name');
    }

    public function salle()
    {
        return $this->belongsTo('App\Models\Room', 'room_id')->select('id', 'name');
    }

    public function building()
    {
        return $this->belongsTo('App\Models\Building', 'building_id')->select('id', 'name');
    }

    public function level()
    {
        return $this->belongsTo('App\Models\Level', 'level_id')->select('id', 'name');
    }
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id')->select('id', 'name');
    }
}
