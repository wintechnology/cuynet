<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use HasFactory;
    public function category()
    {
        return $this->belongsTo('App\Models\CourseCategory', 'category_id')->select('id','name');
    }
}
