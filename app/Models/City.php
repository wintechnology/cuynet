<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;
    public function parentcountry()
    {
        return $this->belongsTo('App\Models\Country', 'country_id')->select('id', 'code','name');
    }
}
