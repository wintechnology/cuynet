<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    public function ouvrage()
    {
        return $this->belongsTo('App\Models\Ouvrage', 'ouvrage_id')->select('id','title');
    }
    public function student()
    {
        return $this->belongsTo('App\Models\Student', 'student_id')->select('id','name');
    }
    public function class()
    {
        return $this->belongsTo('App\Models\Classe', 'classe_id')->select('id','name');
    }
}
