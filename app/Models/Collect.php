<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Collect extends Model
{
    use HasFactory;
    public function academicYear()
    {
        return $this->belongsTo('App\Models\AcademicYear', 'year_id')->select('id', 'year');
    }
    public function class()
    {
        return $this->belongsTo('App\Models\Classe', 'class_id')->select('id', 'abr');
    }
    public function student()
    {
        return $this->belongsTo('App\Models\Student', 'student_id');
    }

    public function rubrique()
    {
        return $this->belongsTo('App\Models\Rubric', 'rubric');
    }
}
