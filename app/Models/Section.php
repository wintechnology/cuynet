<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;
    public function sectionType()
    {
        return $this->belongsTo('App\Models\SectionType', 'type_id')->select('id', 'name');
    }
    public function cycle()
    {
        return $this->belongsTo('App\Models\Cycle', 'cycle_id')->select('id', 'name');
    }
}
