<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;
    public function roomType()
    {
        return $this->belongsTo('App\Models\RoomType', 'type_id')->select('id','name');
    }
    public function building()
    {
        return $this->belongsTo('App\Models\Building', 'building_id')->select('id','name');
    }
}
