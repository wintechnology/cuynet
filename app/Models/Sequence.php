<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sequence extends Model
{
    use HasFactory;
    public function trimester()
    {
        return $this->belongsTo('App\Models\Trimester', 'trimester_id')->select('id','value');
    }
}
