<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseParticipant extends Model
{
    use HasFactory;
    public function course()
    {
        return $this->belongsTo('App\Models\AcademicYear', 'course_id');
    }
}
