<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AcademicYear extends Model
{
    use HasFactory;
    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id')->select('id', 'name');
    }
    public function school()
    {
        return $this->belongsTo('App\Models\School', 'school_id')->select('id', 'name');
    }
}
