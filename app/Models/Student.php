<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    public function classe()
    {
        return $this->belongsTo('App\Models\Classe', 'class_id')->select('id','abr');
    }
    public function academicYear()
    {
        return $this->belongsTo('App\Models\AcademicYear', 'year_id')->select('id','year');
    }
    public function school()
    {
        return $this->belongsTo('App\Models\School', 'school_name')->select('id','name');
    }
    public function father()
    {
        return $this->belongsTo('App\Models\Father', 'father_id');
    }
    public function mother()
    {
        return $this->belongsTo('App\Models\Mother', 'mother_id');
    }
    public function tutor()
    {
        return $this->belongsTo('App\Models\Tutor', 'tutor_id');
    }
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id')->select('id','name');
    }
    public function level()
    {
        return $this->belongsTo('App\Models\Level', 'previous_level_id')->select('id','name');
    }
    public function status()
    {
        return $this->belongsTo('App\Models\State', 'status_id')->select('id', 'name');
    }
    public function regime()
    {
        return $this->belongsTo('App\Models\Regime', 'regime_id')->select('id', 'name');
    }
    public function section()
    {
        return $this->belongsTo('App\Models\Section', 'faculty_id')->select('id', 'name');
    }

}
