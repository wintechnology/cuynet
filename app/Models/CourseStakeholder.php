<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseStakeholder extends Model
{
    use HasFactory;
    public function course()
    {
        return $this->belongsTo('App\Models\AcademicYear', 'course_id');
    }

    public function stakeholders()
    {
        return $this->HasMany('App\Models\Stakeholder', 'id')->where('status', 1);
    }
}
