<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseSpend extends Model
{
    use HasFactory;
    public function parentcourse()
    {
        return $this->belongsTo('App\Models\AcademicYear', 'course_id');
    }
}
