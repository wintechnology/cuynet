<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ouvrage extends Model
{
    use HasFactory;
    public function category()
    {
        return $this->belongsTo('App\Models\LibraryCat', 'category_id')->select('id','name');
    }
    public function location()
    {
        return $this->belongsTo('App\Models\Location', 'category_id')->select('id','name');
    }
}
