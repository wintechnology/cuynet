<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnnexeFee extends Model
{
    use HasFactory;
    public function class()
    {
        return $this->belongsTo('App\Models\Classe', 'class_id')->select('id','abr');
    }
}
