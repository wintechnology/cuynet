<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseStakeholdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_stakeholders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('course_id');
            $table->unsignedInteger('stakeholder_id');
            $table->timestamps();
        });
        Schema::table('course_stakeholders', function(Blueprint $table){
            $table->foreign('course_id')->references('id')->on('courses');
            $table->foreign('stakeholder_id')->references('id')->on('stakeholders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_stakeholders');
    }
}
