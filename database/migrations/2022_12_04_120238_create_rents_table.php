<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rents', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('ref')->unique();
            $table->unsignedTinyInteger('student_id');
            $table->unsignedTinyInteger('ouvrage_id');
            $table->string('rentDate');
            $table->string('delay')->nullable();
            $table->string('dateBack')->nullable();
            $table->integer('quantityTake')->default(1);
            $table->integer('quantityBack')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rents');
    }
}
