<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRubricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rubrics', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name');
            $table->string('abr')->nullable();
            $table->string('amount')->nullable();
            $table->unsignedTinyInteger('class_id')->nullable();
            $table->unsignedTinyInteger('year_id')->nullable();
            $table->unsignedTinyInteger('budget_line_id')->nullable();
            $table->string('actif')->nullable();
            $table->string('student_required')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rubrics');
    }
}
