<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('names');
            $table->string('surnames');
            $table->string('birthday');
            $table->string('birthplace');
            $table->string('profession');
            $table->string('function');
            $table->string('phone');
            $table->string('email');
            $table->unsignedInteger('company_id');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
        Schema::table('participants', function(Blueprint $table){
            $table->foreign('company_id')->references('id')->on('members');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
