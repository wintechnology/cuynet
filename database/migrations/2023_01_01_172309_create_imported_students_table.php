<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportedStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imported_students', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('year')->nullable();
            $table->string('subscription_date')->nullable();
            $table->string('matricule')->unique()->nullable();
            $table->string('other_matricule')->unique()->nullable();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('sex')->nullable();
            $table->string('birthday_date')->nullable();
            $table->string('birthday_place')->nullable();
            $table->string('class')->nullable();
            $table->string('status')->nullable();
            $table->string('regime')->nullable();
            $table->string('repeating')->nullable();
            $table->string('lv2')->nullable();
            $table->string('art')->nullable();
            $table->string('phone')->nullable();
            $table->string('neighborhood')->nullable();
            $table->string('email')->nullable();
            $table->string('father_name')->nullable();
            $table->string('father_phone')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('mother_phone')->nullable();
            $table->string('tutor_name')->nullable();
            $table->string('tutor_phone')->nullable();
            $table->string('school_name')->nullable();
            $table->string('nationality')->nullable();
            $table->string('city')->nullable();
            $table->string('profession')->nullable();
            $table->string('address')->nullable();
            $table->enum('marital', ['Married', 'Divorced', 'Single', 'Widow(er)']);
            $table->string('phone_partner')->nullable();
            $table->string('email_partner')->nullable();
            $table->string('name_partner')->nullable();
            $table->string('profession_partner')->nullable();
            $table->string('chronic_disease')->nullable();
            $table->string('allergy')->nullable();
            $table->string('medical_observation')->nullable();
            $table->integer('child')->default(0);
            $table->unsignedTinyInteger('faculty_id')->nullable();
            $table->unsignedTinyInteger('previous_level_id')->nullable();
            $table->string('diploma')->nullable();
            $table->string('diploma_year')->nullable();
            $table->string('diploma_mean')->nullable();
            $table->string('diploma_mention')->nullable();
            $table->unsignedTinyInteger('branch_id')->nullable();
            $table->enum('state', ['new', 'old'])->default('new');
            $table->enum('sport', ['true', 'false'])->default('false');
            $table->string('picture_link')->nullable();
            $table->string('situation')->default('false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imported_students');
    }
}
