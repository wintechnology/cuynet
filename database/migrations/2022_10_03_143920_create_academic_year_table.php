<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcademicYearTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_years', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('school_id');
            $table->string('year');
            $table->string('start')->nullable();
            $table->string('end')->nullable();
            $table->string('previous_year')->nullable();
            $table->tinyInteger('sequence_amount')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
        //        Schema::table('courses', function(Blueprint $table){
        //            $table->foreign('formation_id')->references('id')->on('formations');
        //        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic_years');
    }
}
