<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collects', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('matricule_student');
            $table->string('applicant_name')->nullable();
            $table->string('applicant_phone')->nullable();
            $table->string('applicant_email')->nullable();
            $table->string('value_date')->nullable();
            $table->unsignedTinyInteger('student_id');
            $table->string('class_id')->nullable();
            $table->unsignedTinyInteger('account_id');
            $table->string('year_id')->nullable();
            $table->string('rubric')->nullable();
            $table->string('remark')->nullable();
            $table->integer('total')->default(0);
            $table->integer('advance')->default(0);
            $table->integer('rest')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collects');
    }
}
