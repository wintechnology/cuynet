<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->unsignedTinyInteger('classe_id');
            $table->unsignedTinyInteger('course_id');
            $table->unsignedTinyInteger('room_id');
            $table->unsignedTinyInteger('teacher_id');
            $table->string('day');
            $table->string('start');
            $table->string('end');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
