<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('school_name');
            $table->unsignedTinyInteger('year_id');
            $table->string('abr')->nullable();
            $table->string('description')->nullable();
            $table->unsignedTinyInteger('branch_id')->nullable();
            $table->string('tutel')->nullable();
            $table->string('department')->nullable();
            $table->unsignedTinyInteger('section_id')->nullable();
            $table->unsignedTinyInteger('level_id')->nullable();
            $table->string('formation')->nullable();
            $table->unsignedTinyInteger('room_id')->nullable();
            $table->integer('max')->default(20);
            $table->tinyInteger('optional')->default(1);
            $table->string('option')->nullable();
            $table->tinyInteger('actif')->default(1);
            $table->tinyInteger('exam_class')->default(-1); // -1 : Classe intermédiaire, pas d'examen; 0 : Classe d'examen FACULTATIF, les évaluations de classe permettent le passage en classe supérieure; 1 : Classe d'examen OBLIGATOIRE, le succès à l'examen est OBLIGATOIRE pour passer en classe supérieure
            $table->tinyInteger('limit_age')->default(0);
            $table->integer('simple_hourly_rate')->default(0);
            $table->integer('multiple_hourly_rate')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
