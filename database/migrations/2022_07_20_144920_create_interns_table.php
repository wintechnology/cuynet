<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('names');
            $table->string('surnames');
            $table->string('id_card_num')->nullable();
            $table->string('id_card_validity')->nullable();
            $table->string('father_names')->nullable();
            $table->string('mother_names')->nullable();
            $table->string('birthday');
            $table->string('birthplace');
            $table->enum('gender',['M', 'F']);
            $table->enum('marital',['Célibataire', 'Marié(e)', 'Autre']);
            $table->integer('children');
            $table->string('phone');
            $table->string('pic')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interns');
    }
}
