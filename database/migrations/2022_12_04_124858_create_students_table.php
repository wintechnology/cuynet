<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('school_name');
            $table->unsignedTinyInteger('year_id');
            $table->string('matricule')->unique();
            $table->string('other_matricule')->unique()->nullable();
            $table->string('name');
            $table->string('surname');
            $table->string('birthday_date')->nullable();
            $table->string('birthday_place')->nullable();
            $table->string('nationality')->nullable();
            $table->string('email')->nullable();
            $table->enum('sex', ['Masculin', 'Féminin', 'Autre'])->nullable();
            $table->string('city')->nullable();
            $table->string('phone')->nullable();
            $table->string('neighborhood')->nullable();
            $table->string('profession')->nullable();
            $table->string('address')->nullable();
            $table->enum('marital', ['Married', 'Divorced', 'Single', 'Widow(er)'])->nullable();
            $table->string('phone_partner')->nullable();
            $table->string('email_partner')->nullable();
            $table->string('name_partner')->nullable();
            $table->string('profession_partner')->nullable();
            $table->string('chronic_disease')->nullable();
            $table->string('allergy')->nullable();
            $table->string('medical_observation')->nullable();
            $table->integer('child')->default(0);
            $table->unsignedTinyInteger('father_id')->nullable();
            $table->unsignedTinyInteger('mother_id')->nullable();
            $table->unsignedTinyInteger('tutor_id')->nullable();
            $table->unsignedTinyInteger('faculty_id')->nullable();
            $table->unsignedTinyInteger('previous_level_id')->nullable();
            $table->string('diploma')->nullable();
            $table->string('diploma_year')->nullable();
            $table->string('diploma_mean')->nullable();
            $table->string('diploma_mention')->nullable();
            $table->unsignedTinyInteger('branch_id')->nullable();
            $table->unsignedTinyInteger('class_id')->nullable();
            $table->unsignedTinyInteger('status_id')->nullable();
            $table->unsignedTinyInteger('regime_id')->nullable();
            $table->string('second_language')->nullable();
            $table->string('art')->nullable();
            $table->string('subscription_date')->nullable();
            $table->string('subscription_agent')->nullable();
            $table->enum('state', ['new', 'old'])->nullable();
            $table->enum('repeating', ['true', 'false','triple'])->nullable();
            $table->enum('sport', ['true', 'false'])->nullable();
            $table->string('picture_link')->nullable();
            $table->string('situation')->default('false');
            $table->string('old_school')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
