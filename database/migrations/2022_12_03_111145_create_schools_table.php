<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('code')->nullable();
            $table->string('ministry_code')->nullable();
            $table->string('abreviation')->nullable();
            $table->string('name')->nullable();
            $table->unsignedTinyInteger('country_id')->nullable();
            $table->string('city')->nullable();
            $table->string('neighborhood')->nullable();
            $table->string('phone')->nullable();
            $table->string('postal_address')->nullable();
            $table->string('geographic_situation')->nullable();
            $table->string('web_site')->nullable();
            $table->string('email')->nullable();
            $table->string('logo')->nullable();
            $table->string('founder')->nullable();
            $table->string('studies_director')->nullable();
            $table->string('currency')->nullable();
            $table->string('_token')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
