<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('section_types')->delete();
        $sectionTypeRecords = [
            [
                'id'=>1,
                'name'=>'N/A',
                'status'=>1
            ]
        ];
        DB::table('section_types')->insert($sectionTypeRecords);
    }
}
