<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GuestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('guests')->delete();
        $guestRecords = [
            [
                'id'=>1,
                'course_id'=>1,
                'names'=>'John Doe',
                'phone'=>'+237 767974659'
            ],
            [
                'id'=>2,
                'course_id'=>2,
                'names'=>'Janette Doe',
                'phone'=>'+237 767974659'
            ]
        ];
        DB::table('guests')->insert($guestRecords);
    }
}
