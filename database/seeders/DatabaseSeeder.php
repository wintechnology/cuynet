<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
//         $this->call(AdminsTableSeeder::class);
//         $this->call(AcademicYearsTableSeeder::class);
//         $this->call(CategoriesTableSeeder::class);
//         $this->call(SchoolsTableSeeder::class);
//         $this->call(SectionsTableSeeder::class);
//         $this->call(SectionTypesTableSeeder::class);
//         $this->call(RoomsTableSeeder::class);
//         $this->call(BuildingsTableSeeder::class);
         $this->call(CoursestableSeeder::class);
    }
}
