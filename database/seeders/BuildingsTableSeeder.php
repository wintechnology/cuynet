<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BuildingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buildings')->delete();
        $buildingRecords = [
            [
                'id'=>1,
                'site_id'=>1,
                'code'=>'BAT202209',
                'name'=>'BAT TEST',
                'status'=>1
            ]
        ];
        DB::table('buildings')->insert($buildingRecords);
    }
}
