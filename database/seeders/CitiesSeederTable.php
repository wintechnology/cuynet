<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->delete();
        $citiesRecords = [
            [
                'id'=>1,
                'name'=>'Yaoundé',
                'country_id'=>1
            ],
            [
                'id'=>2,
                'name'=>'Douala',
                'country_id'=>1
            ]
        ];

        DB::table('cities')->insert($citiesRecords);
    }
}
