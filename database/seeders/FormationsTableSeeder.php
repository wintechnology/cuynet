<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formations')->delete();
        $formationsRecords = [
            [
                'id'=>1,
                'theme'=>'Seminaire de présentation',
                'goal'=>'Présentation des formations',
                'type'=>'Pas catégorisé'
            ]
        ];
        DB::table('formations')->insert($formationsRecords);
    }
}
