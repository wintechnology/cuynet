<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MembersSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('members')->delete();
        $membersRecords = [
            [
                'id'=>1,
                'reference'=>'00WIN00',
                'company_name'=>'WIN Technology',
                'activity_sector'=>'NTIC',
                'country'=>'Cameroun',
                'city'=>'Yaoundé',
                'phone'=>'+237 694795166',
                'email'=>'info@winmarket.cm',
                'type'=>'Privée',
                'status'=>1
            ]
        ];

        DB::table('members')->insert($membersRecords);
    }
}
