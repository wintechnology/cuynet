<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->delete();
        $roomRecords = [
            [
                'id'=>1,
                'type_id'=>1,
                'building_id'=>1,
                'code'=>'SALLE202209',
                'name'=>'SALLE TEST',
                'status'=>1
            ]
        ];
        DB::table('rooms')->insert($roomRecords);
    }
}
