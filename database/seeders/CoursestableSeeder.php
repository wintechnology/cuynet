<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoursestableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->delete();
        $courseRecords = [
            [
                'id'=>1,
                'room_id'=>1,
                'code'=>'MAT202209',
                'name'=>'Matière TEST',
                'status'=>1
            ]
        ];
        DB::table('courses')->insert($courseRecords);
    }
}
