<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivitySectorSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activity_sectors')->delete();
        $activitiesSectorsRecords = [
            [
                'id'=>1,
                'name'=>'Inconnu',
                'status'=>1
            ]
        ];

        DB::table('activity_sectors')->insert($activitiesSectorsRecords);
    }
}
