<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        $categorieRecords = [
            [
                'id'=>1,
                'name'=>'PUBLIC',
                'status'=>1
            ]
        ];
        DB::table('categories')->insert($categorieRecords);
    }
}
