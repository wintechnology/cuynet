<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseSpendTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('course_spends')->delete();
        $courseSpendRecords = [
            [
                'id'=>1,
                'course_id'=>1,
                'reference'=>'TEST001',
                'designation'=>'Achat Test',
                'unit_amount'=>10000,
                'quantity'=>13,
                'total_amount'=>13 * 10000,
                'date'=>date("d-m-Y"),            ]
        ];
        DB::table('course_spends')->insert($courseSpendRecords);
    }
}
