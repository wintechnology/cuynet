<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sections')->delete();
        $sectionRecords = [
            [
                'id'=>1,
                'type_id'=>1,
                'code'=>'SEC202209',
                'name'=>'SECTION TEST',
                'status'=>1
            ]
        ];
        DB::table('sections')->insert($sectionRecords);
    }
}
