<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schools')->delete();
        $schoolRecords = [
            [
                'id'=>1,
                'name'=>'ECOLE PUBLIC TEST',
                'status'=>1
            ]
        ];
        DB::table('schools')->insert($schoolRecords);
    }
}
