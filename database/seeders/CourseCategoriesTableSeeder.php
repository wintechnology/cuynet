<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('course_categories')->delete();
        $courseCategoryRecords = [
            [
                'id'=>1,
                'name'=>'Pas Catégorisé',
                'status'=>1
            ]
        ];

        DB::table('course_categories')->insert($courseCategoryRecords);
    }
}
