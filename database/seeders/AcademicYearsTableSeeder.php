<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AcademicYearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('academic_years')->delete();
        $academicYearRecords = [
            [
                'id'=>1,
                'category_id'=>1,
                'school_id'=>1,
                'year'=>'2022-2023',
                'start'=>'2022-09-05',
                'end'=>'2023-06-05',
                'status'=>1
            ]
        ];
        DB::table('academic_years')->insert($academicYearRecords);
    }
}
