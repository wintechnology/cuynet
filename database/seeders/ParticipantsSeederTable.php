<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParticipantsSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('participants')->delete();
        $participantsRecords = [
            [
                'id'=>1,
                'names'=>'KONDE',
                'surnames'=>'Jean Félix',
                'birthday'=>'01/01/1970',
                'birthplace'=>'Yaoundé',
                'profession'=>'Informaticien',
                'function'=>'CEO',
                'phone'=>'+237 694795166',
                'email'=>'info@winmarket.cm',
                'company_id'=>1
            ]
        ];

        DB::table('participants')->insert($participantsRecords);
    }
}
