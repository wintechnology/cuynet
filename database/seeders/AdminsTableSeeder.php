<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();
        $adminRecords = [
            [
                'id'=>1,
                'username'=>'admin',
                'type'=>'admin',
                'mobile'=>'600000000',
                'email'=>'admin@winschool.cm',
                'password'=>'$2y$10$b/255Q/sGi66u397ScMfM.UR9bX3lvOkpwdiPPZou9cLnqNUPlZVy',
                'image'=>'',
                'status'=>1
            ]
        ];

        DB::table('admins')->insert($adminRecords);
    }
}
