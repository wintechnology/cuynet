$(document).ready(function() {
    //    Category status
    $(".updateCategoryStatus").click(function() {
        var status = $(this).text();
        var category_id = $(this).attr("category_id");
        alert(status);
        $.ajax({
            type: 'post',
            url: 'update-category-status',
            data: {status: status, category_id: category_id},
            success: function (res) {
                if (res['status'] == 0) {
                    $("#category-" + category_id).html("<a class='updateCategoryStatus' href='javascript:void(0)'>Inactive</a>");
                } else if (res['status'] == 1) {
                    $("#category-" + category_id).html("<a class='updateCategoryStatus' href='javascript:void(0)'>Active</a>");
                }
            }, error: function () {
                alert("error");
            }
        })
    });

});
