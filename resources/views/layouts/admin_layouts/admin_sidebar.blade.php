<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{ asset('img/admin_images/classroom.png') }}" alt="CUY Logo" class="brand-image img-square elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">WinSchool</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('img/admin_images/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ ucwords(Auth::guard('admin')->user()->username) }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    @if(Session::get('page')=="dashboard")
                        <?php $active = "active";?>
                    @else
                        <?php $active = "";?>
                    @endif
                    <a href="{{ url('admin/dashboard') }}" class="nav-link {{ $active }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Tableau de bord
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    @if(Session::get('page')=="academic_years")
                        <?php $active = "active";?>
                    @else
                        <?php $active = "";?>
                    @endif
                    <a href="{{ url('admin/academic_years') }}" class="nav-link {{ $active }}">
                        <i class="nav-icon fas fa-play-circle"></i>
                        <p>
                            Année académique
{{--                            <span class="right badge badge-danger">New</span>--}}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    @if(Session::get('page')=="trimesters")
                            <?php $active = "active";?>
                    @else
                            <?php $active = "";?>
                    @endif
                    <a href="{{ url('admin/trimesters') }}" class="nav-link {{ $active }}">
                        <i class="nav-icon fas fa-play-circle"></i>
                        <p>
                            Trimestre
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    @if(Session::get('page')=="schedules")
                            <?php $active = "active";?>
                    @else
                            <?php $active = "";?>
                    @endif
                    <a href="{{ url('admin/schedules') }}" class="nav-link {{ $active }}">
                        <i class="nav-icon fas fa-play-circle"></i>
                        <p>
                            Emploi de temps
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    @if(Session::get('page')=="sections")
                        <?php $active = "active";?>
                    @else
                        <?php $active = "";?>
                    @endif
                    <a href="{{ url('admin/sections') }}" class="nav-link {{ $active }}">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            Filières/Sections
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    @if(Session::get('page')=="classes")
                        <?php $active = "active";?>
                    @else
                        <?php $active = "";?>
                    @endif
                    <a href="{{ url('admin/classes') }}" class="nav-link {{ $active }}">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            Classes
{{--                            <span class="right badge badge-danger">New</span>--}}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    @if(Session::get('page')=="courses")
                        <?php $active = "active";?>
                    @else
                        <?php $active = "";?>
                    @endif
                    <a href="{{ url('admin/courses') }}" class="nav-link {{ $active }}">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Matières
{{--                            <span class="right badge badge-danger">New</span>--}}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    @if(Session::get('page')=="sequences")
                        <?php $active = "active";?>
                    @else
                        <?php $active = "";?>
                    @endif
                    <a href="{{ url('admin/sequences') }}" class="nav-link {{ $active }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Séquences
{{--                            <span class="right badge badge-danger">New</span>--}}
                        </p>
                    </a>
                </li>
{{--                Interns/Trainees--}}
                <li class="nav-item">
                    @if(Session::get('page')=="lessons")
                        <?php $active = "active";?>
                    @else
                        <?php $active = "";?>
                    @endif
                    <a href="{{ url('admin/lessons') }}" class="nav-link {{ $active }}">
                        <i class="nav-icon fas fa-people-arrows"></i>
                        <p>
                            Leçons
                        </p>
                    </a>
                </li>
{{--                Inscription--}}
                <li class="nav-item">
                    @if(Session::get('page')=="subscriptions")
                            <?php $active = "active";?>
                    @else
                            <?php $active = "";?>
                    @endif
                    <a href="{{ url('admin/subscriptions') }}" class="nav-link {{ $active }}">
                        <i class="nav-icon fas fa-sign-in-alt"></i>
                        <p>
                            Inscription
                        </p>
                    </a>
                </li>
{{--                Finances--}}
                @if(Session::get('page')=="finances")
                        <?php $active = "active"; $menu_open = "menu-open"?>
                @else
                        <?php $active = ""; $menu_open = ""?>
                @endif
                <li class="nav-item {{ $menu_open }}">
                    <a href="#" class="nav-link {{ $active }}">
                        <i class="nav-icon fas fa-money-bill-alt"></i>
                        <p>
                            Finances
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        {{--                Collect--}}
                        <li class="nav-item">
                            @if(Session::get('page')=="collects")
                                    <?php $active = "active";?>
                            @else
                                    <?php $active = "";?>
                            @endif
                            <a href="{{ url('admin/subscriptions/collect') }}" class="nav-link {{ $active }}">
                                <i class="nav-icon far fa-money-bill-alt"></i>
                                <p>
                                    Encaissements
                                </p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            @if(Session::get('page')=="fees")
                                    <?php $active = "active";?>
                            @else
                                    <?php $active = "";?>
                            @endif
                            <a href="{{ url('admin/fees') }}" class="nav-link {{ $active }}">
                                <i class="nav-icon far fa-money-bill-alt"></i>
                                <p>
                                    Parametrage de frais
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
{{--                Libraries--}}
                @if(Session::get('page')=="libraries")
                        <?php $active = "active"; $menu_open = "menu-open"?>
                @else
                        <?php $active = ""; $menu_open = ""?>
                @endif
                <li class="nav-item {{ $menu_open }}">
                    <a href="#" class="nav-link {{ $active }}">
                        <i class="nav-icon fas fa-book-open"></i>
                        <p>
                            Bibliothèque
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            @if(Session::get('page')=="ouvrages")
                                    <?php $active = "active";?>
                            @else
                                    <?php $active = "";?>
                            @endif
                            <a href="{{ url('admin/ouvrages') }}" class="nav-link {{ $active }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ouvrages</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            @if(Session::get('page')=="rents")
                                    <?php $active = "active";?>
                            @else
                                    <?php $active = "";?>
                            @endif
                            <a href="{{ url('admin/rents') }}" class="nav-link {{ $active }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Liste des pêts</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            @if(Session::get('page')=="bookings")
                                    <?php $active = "active";?>
                            @else
                                    <?php $active = "";?>
                            @endif
                            <a href="{{ url('admin/bookings') }}" class="nav-link {{ $active }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Reservations</p>
                            </a>
                        </li>
                    </ul>
                </li>
{{--                Settings--}}
                @if(Session::get('page')=="parametres")
                    <?php $active = "active"; $menu_open = "menu-open"?>
                @else
                    <?php $active = ""; $menu_open = ""?>
                @endif
                <li class="nav-item {{ $menu_open }}">
                    <a href="#" class="nav-link {{ $active }}">
                        <i class="nav-icon fas fa-tools"></i>
                        <p>
                            Paramètres
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
{{--Sub menus--}}
                    <ul class="nav nav-treeview">
                        {{--                1st Part --}}
                        @if(Session::get('page')=="inscriptions")
                                <?php $active = "active"; $menu_open = "menu-open"?>
                        @else
                                <?php $active = ""; $menu_open = ""?>
                        @endif
                        <li class="nav-item {{ $menu_open }}">
                            <a href="#" class="nav-link {{ $active }}">
                                <i class="nav-icon fas fa-link"></i>
                                <p>
                                    Inscription
                                    <i class="right fas fa-angle-double-right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="countries")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/countries') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Pays</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="regions")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/regions') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Regions</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="departments")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/departments') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Départments</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="borough")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/boroughs') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Arrondissements</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="sexes")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/sexes') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Sexes</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="maritals")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/maritals') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>État civil</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="fathers")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/fathers') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Pères</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="mothers")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/mothers') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Mères</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="tutors")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/tutors') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Tuteurs</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        {{--                2nd Part --}}
                        @if(Session::get('page')=="health")
                                <?php $active = "active"; $menu_open = "menu-open"?>
                        @else
                                <?php $active = ""; $menu_open = ""?>
                        @endif
                        <li class="nav-item {{ $menu_open }}">
                            <a href="#" class="nav-link {{ $active }}">
                                <i class="nav-icon fas fa-heartbeat"></i>
                                <p>
                                    Santé
                                    <i class="right fas fa-angle-double-right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="bloodgroups")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/bloodgroups') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Groupes Sanguin</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="rhesus")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/rhesus') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Rhésus</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        {{--                3rd Part --}}
                        @if(Session::get('page')=="schools")
                                <?php $active = "active"; $menu_open = "menu-open"?>
                        @else
                                <?php $active = ""; $menu_open = ""?>
                        @endif
                        <li class="nav-item {{ $menu_open }}">
                            <a href="#" class="nav-link {{ $active }}">
                                <i class="nav-icon fas fa-school"></i>
                                <p>
                                    Écoles
                                    <i class="right fas fa-angle-double-right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="branches")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/branches') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Branches / Facultés</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="formations")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/add-edit-formation_type') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Types de formation</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="rooms")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/rooms') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Liste des salles</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="options")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/options') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Options</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="regimes")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/regimes') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Régimes</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="states")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/states') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Status</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="lv2")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/add-edit-lv2') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>LV2</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="arts")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/add-edit-art') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Arts</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="required_documents")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/add-edit-required_document') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Pièces à fournir</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="rubrics")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/rubrics') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Rubriques</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="annexe_fee")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/add-edit-annexe_fee') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Frais Annexes</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="required_documents")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/add-edit-required_document') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Pièces à fournir</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        {{--                library Part --}}
                        @if(Session::get('page')=="library")
                                <?php $active = "active"; $menu_open = "menu-open"?>
                        @else
                                <?php $active = ""; $menu_open = ""?>
                        @endif
                        <li class="nav-item {{ $menu_open }}">
                            <a href="#" class="nav-link {{ $active }}">
                                <i class="nav-icon fas fa-book-reader"></i>
                                <p>
                                    Gestion de bibliothèque
                                    <i class="right fas fa-angle-double-right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="library_categories")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/library_categories') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Catégories bibliothèque</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="locations")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/locations') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Emplacements</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        {{--                lambda Part --}}
                        @if(Session::get('page')=="lambda")
                                <?php $active = "active"; $menu_open = "menu-open"?>
                        @else
                                <?php $active = ""; $menu_open = ""?>
                        @endif
                        <li class="nav-item {{ $menu_open }}">
                            <a href="#" class="nav-link {{ $active }}">
                                <i class="nav-icon fas fa-toolbox"></i>
                                <p>
                                    Autres
                                    <i class="right fas fa-angle-double-right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="cycles")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/cycles') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Cycles</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="categories")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/categories') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Catégories des écoles</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="year-categories")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/year-categories') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Catégories année académique</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="sites")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/sites') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Sites écoles</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="buildings")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/buildings') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Liste de bâtiments</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="types")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/types') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Types des salles</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="types_section")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/types_sections') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Types des filières/sections</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="levels")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/levels') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Niveaux</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="schools")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/add-edit-school') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Informations école</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    @if(Session::get('page')=="teachers")
                                            <?php $active = "active";?>
                                    @else
                                            <?php $active = "";?>
                                    @endif
                                    <a href="{{ url('admin/teachers') }}" class="nav-link {{ $active }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Liste des enseignants</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
