@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Inscriptions</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Inscriptions</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <form action="{{ url('/admin/subscriptions/create-step-four') }}" method="POST">
                            @csrf

                            <div class="card">
                                <div class="card-header">Etapes 4 : informations du tuteur</div>

                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Saisie Manuelle </label>
                                                &nbsp;
                                                &nbsp;
                                                &nbsp;
                                                &nbsp;
                                                &nbsp;
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="oui" />
                                                    <label class="form-check-label" for="inlineRadio1">Oui</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="non" />
                                                    <label class="form-check-label" for="inlineRadio2">Non</label>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Tuteur</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="company_id" id="company_id">
                                                    <option selected="" value="">Selectionner</option>
                                                    {{--                                                    @foreach($getMembers as $member)--}}
                                                    {{--                                                        <option value="{{ $member->id }}">{{ $member->company_name }} ({{$member->reference}})</option>--}}
                                                    {{--                                                    @endforeach--}}
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom du Tuteur</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames" placeholder="Entrer le nom">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Profession du Tuteur</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames" placeholder="Entrer la profession">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Téléphone du Tuteur</label>
                                                <input type="text" name="names" class="form-control" id="names" placeholder="Entrer le numéro téléphonique">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email du Tuteur</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames" placeholder="Entrer la date de naissance">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </div>

                                <div class="card-footer text-right">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
