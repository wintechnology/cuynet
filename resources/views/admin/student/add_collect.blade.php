@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Encaissement</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Encaissement</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <form action="{{ url('/admin/subscriptions/add-collect') }}" method="POST">@csrf
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Matricule</label>
                                                <input type="text" name="matricule" class="form-control" id="matricule" value="{{ $student->matricule }}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom & Prénom</label>
                                                <input type="text" name="name" class="form-control" id="name" value="{{ $student->name.' '.$student->surname }}" readonly>
                                                <input type="hidden" name="student_id" class="form-control" id="student_id" value="{{ $student->id }}">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Année</label>
                                                <select name="year_id" class="form-control" id="year_id" required>
                                                    <option selected="" value="">Selectionner une année</option>
                                                    @foreach($academicYears as $year)
                                                        <option value="{{ $year->id }}">{{ $year->year }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Rubrique</label>
                                                <select name="rubric" class="form-control" id="rubric" required>
                                                    <option selected="" value="">Choisir une rubrique</option>
                                                    @foreach($rubriques as $rubric)
                                                        <option value="{{ $rubric->id }}">{{ $rubric->name.' ('. number_format($rubric->amount, 0, '.', ' ').')' }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Montant versé</label>
                                                <input type="number" name="advance" class="form-control" id="advance" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Date valeur</label>
                                                <input type="date" name="value_date" class="form-control" id="value_date" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Classe</label>
                                                <select name="class_id" class="form-control" id="class_id" required>
                                                    <option selected="" value="">Choisir une classe</option>
                                                    @foreach($classes as $class)
                                                        <option value="{{ $class->id }}">{{ $class->level->name.''.$class->abr.' '.$class->section->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Compte</label>
                                                <select name="account_id" class="form-control" id="account_id" required>
                                                    <option selected="" value="">Choisir un compte</option>
                                                    <option value="1">Compte principal</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Déposant</label>
                                                <input type="text" name="applicant_name" class="form-control" id="applicant_name" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Téléphone Déposant</label>
                                                <input type="text" name="applicant_phone" class="form-control" id="applicant_phone">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email Déposant</label>
                                                <input type="email" name="applicant_email" class="form-control" id="applicant_email">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Remarque</label>
                                                <textarea name="remark" class="form-control" id="remark"></textarea>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>

                                <div class="card-footer text-right">
                                    <a href="{{ url('/admin/subscriptions/collect/'.$student->id) }}" style="max-width:200px; float: right; display: inline-block" class="btn btn-outline-primary  ml-2">Retour</a>
                                    <button type="submit" class="btn btn-primary">Valider la paiement</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
