@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Re-inscription</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Inscriptions</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <form action="{{ url('/admin/subscriptions/find') }}" method="POST">
                            @csrf

                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Recherche du dossier </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Matricule</label>
                                                <input type="text" name="matricule" class="form-control" id="matricule">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom et prénom</label>
                                                <input type="text" name="name" class="form-control" id="name">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>

                                <div class="card-footer text-right">
                                    <i>La recherche se fait par le matricule ou par le nom</i> &nbsp;&nbsp;
                                    <button type="submit" class="btn btn-primary">Rechercher</button>
                                </div>
                            </div>
                        </form>
                        @if($found)
                            @if(Session::has('modified'))
                                <div class="alert alert-success text-center" role="alert">
                                    {{ Session::get('modified') }}
                                </div>
                            @endif
                        <form action="{{ url('/admin/subscriptions/create-step-three') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            {{--                                        Divider--}}
                            <div class="col-md-12">
                                <hr/>
                                <div class="bg-warning text-bold text-center"> Le resultat de votre recherche </div>
                                <hr/>
                            </div>
                            {{--                                        End divider--}}
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <div class="bg-secondary text-bold text-center"> Etat civil </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Ecole</label>
                                                <input type="text" name="school_name" class="form-control" id="school_name" value="{{ $student->school->name }}" readonly>
                                                <input type="hidden" name="student_id" class="form-control" id="student_id" value="{{ $student->id }}">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Matricule <sup class="text-danger">*</sup></label>
                                                <input type="text" name="matricule" class="form-control" id="matricule" placeholder="Entrer le matrcicule" value="{{ $student->matricule }}" readonly>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom <sup class="text-danger">*</sup></label>
                                                <input type="text" name="name" class="form-control" id="name" placeholder="Entrer un nom" value="{{ $student->name }}" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Prénom <sup class="text-danger">*</sup></label>
                                                <input type="text" name="surname" class="form-control" id="surname" placeholder="Entrer un prénom" value="{{ $student->surname }}" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Date de naissance <sup class="text-danger">*</sup></label>
                                                <input type="text" name="birthday_date" class="form-control" id="birthday_date" value="{{ date("d/m/y", strtotime($student->birthday_date)) }}">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Sexe</label>
                                                <input type="text" name="sex" class="form-control" id="dex" value="{{ $student->sex }}">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Phone</label>
                                                <input type="text" name="phone" class="form-control" id="phone" value="{{ $student->phone }}">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Profession</label>
                                                <input type="text" name="profession" class="form-control" id="profession" placeholder="Entrer une profession" value="{{ $student->profession }}">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Année <sup class="text-danger">*</sup></label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="year_id" id="year_id">
                                                    <option value="{{ $student->academicYear->id }}">{{ $student->academicYear->year }}</option>
                                                    @foreach( $academicYears as $year)
                                                        <option value="{{ $year->id }}">{{ $year->year }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Autre Matricule</label>
                                                <input type="text" name="other_matricule" class="form-control" id="other_matricule" placeholder="Autre matricule" value="{{ $student->other_matricule }}" readonly>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Lieu de naissance</label>
                                                <input type="text" name="birthday_place" class="form-control" id="birthday_place" value="{{ $student->birthday_place }}" placeholder="Lieu de naissance" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nationalité</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="nationality" id="nationality">
                                                    <option selected="" value="{{ $student->nationality }}">{{ $student->nationality }}</option>
                                                    @foreach( $countries as $country)
                                                        <option value="{{ $country->nationality }}">{{ $country->nationality.' ('.$country->name.' - '.$country->code.')' }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Mail</label>
                                                <input type="email" name="email" class="form-control" id="email" value="{{ $student->email }}">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Ville</label>
                                                <input type="text" name="city" class="form-control" id="city" placeholder="Entrer une ville" value="{{ $student->city }}">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Quartier</label>
                                                <input type="text" name="neighborhood" class="form-control" id="neighborhood" placeholder="Entrer un quartier" value="{{ $student->neighborhood }}">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Adresse</label>
                                                <input type="text" name="address" class="form-control" id="address" placeholder="Entrer une adresse" value="{{ $student->address }}">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Autre </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Situation matrimoniale</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="marital" id="marital">
                                                    @if($student->marital == "Married")
                                                    <option selected="" value="{{ $student->marital }}">Marié(e)</option>
                                                    <option value="Single">Célibataire</option>
                                                    <option value="Widow(er)">Veuf(veuve)</option>
                                                    <option value="Divorced">Divorcé(e)</option>
                                                    @elseif($student->marital == "Single")
                                                    <option selected="" value="{{ $student->marital }}">Célibataire</option>
                                                    <option value="Married">Marié(e)</option>
                                                    <option value="Widow(er)">Veuf(veuve)</option>
                                                    <option value="Divorced">Divorcé(e)</option>
                                                    @elseif($student->marital == "Widow(er)")
                                                    <option selected="" value="{{ $student->marital }}">Veuf(veuve)</option>
                                                    <option value="Married">Marié(e)</option>
                                                    <option value="Single">Célibataire</option>
                                                    <option value="Divorced">Divorcé(e)</option>
                                                    @else
                                                    <option selected="" value="{{ $student->marital }}">Divorcé(e)</option>
                                                    <option value="Married">Marié(e)</option>
                                                    <option value="Single">Célibataire</option>
                                                    <option value="Widow(er)">Veuf(veuve)</option>
                                                    @endif
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nombre d'enfant</label>
                                                <input type="number" name="child" class="form-control" value="{{ $student->child }}" id="child">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom conjoint</label>
                                                <input type="text" name="name_partner" class="form-control" id="name_partner" value="{{ $student->name_partner }}">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Téléphone conjoint</label>
                                                <input type="text" name="phone_partner" class="form-control" id="phone_partner" value="{{ $student->phone_partner }}">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email conjoint</label>
                                                <input type="email" name="email_partner" class="form-control" id="email_partner" value="{{ $student->email_partner }}">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Profession conjoint</label>
                                                <input type="text" name="profession_partner" class="form-control" id="profession_partner" value="{{ $student->profession_partner }}">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Information médicales </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Maladie chroniques</label>
                                                <input type="text" name="chronic_disease" class="form-control" id="chronic_disease" value="{{ $student->chronic_disease }}">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Allergies</label>
                                                <input type="text" name="allergy" class="form-control" id="allergy" value="{{ $student->allergy }}">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Autres observations médicales</label>
                                                <input type="text" name="medical_observation" class="form-control" id="medical_observation" value="{{ $student->medical_observation }}">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        @if($student->mother)
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <div class="bg-secondary text-bold text-center"> Filiation : Père</div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Saisie Manuelle </label>
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="father_manual" id="inlineRadio1" value="oui" checked/>
                                                        <label class="form-check-label" for="inlineRadio1">Oui</label>
                                                    </div>

                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="father_manual" id="inlineRadio2" value="non" />
                                                        <label class="form-check-label" for="inlineRadio2">Non</label>
                                                    </div>
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Père</label>
                                                    <select class="form-control select2bs4" style="width: 100%;" name="father_id" id="father_id">
                                                        <option selected="" value="">Selectionner</option>
                                                        @foreach($fathers as $father)
                                                            <option value="{{ $father->id }}">{{ $father->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Nom du Père</label>
                                                    <input type="text" name="father_name" class="form-control" id="father_name" value="{{ $student->father->name }}">
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Profession du Père</label>
                                                    <input type="text" name="father_profession" class="form-control" id="father_profession" value="{{ $student->father->profession }}">
                                                </div>
                                                <!-- /.form-group -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Téléphone du Père</label>
                                                    <input type="text" name="father_phone" class="form-control" id="father_phone" value="{{ $student->father->phone }}">
                                                </div>
                                                <!-- /.form-group -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Email du Père</label>
                                                    <input type="email" name="father_email" class="form-control" id="father_email" value="{{ $student->father->email }}">
                                                </div>
                                                <!-- /.form-group -->
                                            </div>
                                            <!-- /.col -->
                                        @endif
                                        @if($student->mother)
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <div class="bg-secondary text-bold text-center"> Filiation : Mère</div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Saisie Manuelle </label>
                                                &nbsp;
                                                &nbsp;
                                                &nbsp;
                                                &nbsp;
                                                &nbsp;
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="mother_manual" id="inlineRadio1" value="oui" checked/>
                                                    <label class="form-check-label" for="inlineRadio1">Oui</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="mother_manual" id="inlineRadio2" value="non" />
                                                    <label class="form-check-label" for="inlineRadio2">Non</label>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Mère</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="mother_id" id="mother_id">
                                                    <option selected="" value="">Selectionner</option>
                                                    @foreach($mothers as $mother)
                                                        <option value="{{ $mother->id }}">{{ $mother->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom de la Mère</label>
                                                <input type="text" name="mother_name" class="form-control" id="mother_name" value="{{ $student->mother->name }}">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Profession de la Mère</label>
                                                <input type="text" name="mother_profession" class="form-control" id="mother_profession" value="{{ $student->mother->profession }}">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Téléphone de la Mère</label>
                                                <input type="text" name="mother_phone" class="form-control" id="mother_phone" value="{{ $student->mother->phone }}">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email de la Mère</label>
                                                <input type="email" name="mother_email" class="form-control" id="mother_email" value="{{ $student->mother->email }}">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        @endif
                                        @if($student->tutor)
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <div class="bg-secondary text-bold text-center"> Filiation : Tuteur</div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Saisie Manuelle </label>
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="tutor_manual" id="tutor_manual" value="oui" checked/>
                                                        <label class="form-check-label" for="inlineRadio1">Oui</label>
                                                    </div>

                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="tutor_manual" id="tutor_manual" value="non"/>
                                                        <label class="form-check-label" for="inlineRadio2">Non</label>                                                    <label for="exampleInputEmail1">Profession du Tuteur</label>
                                                    <input type="text" name="tutor_profession" class="form-control" id="tutor_profession" value="{{ $student->tutor->profession }}">
                                                </div>
                                                <!-- /.form-group -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Téléphone du Tuteur</label>
                                                    <input type="text" name="tutor_phone" class="form-control" id="tutor_phone" value="{{ $student->tutor->phone }}">
                                                </div>
                                                <!-- /.form-group -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Email du Tuteur</label>
                                                    <input type="email" name="tutor_email" class="form-control" id="tutor_email" value="{{ $student->tutor->email }}">
                                                </div>
                                                <!-- /.form-group -->
                                            </div>
                                            <!-- /.col -->
                                        @endif
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Parcours </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Diplome</label>
                                                <input type="text" name="diploma" class="form-control" value="{{ $student->diploma }}" id="diploma">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Année</label>
                                                <input type="text" name="diploma_year" class="form-control" value="{{ date("d/m/Y", strtotime($student->diploma_year)) }}" id="diploma_year">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Filière</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="faculty_id" id="faculty_id">
                                                    @if($student->faculty_id)
                                                    <option selected="" value="{{ $student->section->id }}">{{ $student->section->name }}</option>
                                                    @else
                                                    <option selected="" value=""></option>
                                                    @endif
                                                    @foreach($sections as $section)
                                                        <option value="{{ $section->id }}">{{ $section->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Moyenne</label>
                                                <input type="text" name="diploma_mean" class="form-control" value="{{ $section->diploma_mean }}" id="diploma_mean">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Mention</label>
                                                <input type="text" name="diploma_mention" class="form-control" value="{{ $section->diploma_mention }}" id="diploma_mention">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Niveau précédent</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="previous_level_id" id="previous_level_id">
                                                    @if($student->previous_level_id)
                                                    <option selected="" value="{{ $student->previous_level_id }}">{{ $student->level->name }}</option>
                                                    @else
                                                        <option selected="" value=""></option>
                                                    @endif
                                                    @foreach($levels as $level)
                                                        <option value="{{ $level->id }}">{{ $level->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Ecole d'origine</label>
                                                <input type="text" name="old_school" class="form-control" value="{{ $student->old_school }}" id="old_school">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Inscription </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Branche</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="branch_id" id="branch_id">
                                                    @if($student->branch_id)
                                                    <option selected="" value="{{ $student->branch_id }}">{{ $student->branch->name }}</option>
                                                    @else
                                                    <option selected="" value=""></option>
                                                    @endif
                                                    @foreach($branches as $branch)
                                                        <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Classe</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="class_id" id="class_id">
                                                    @if($student->class_id)
                                                    <option selected="" value="{{ $student->class_id }}">{{ $student->classe->abr }}</option>
                                                    @else
                                                    <option selected="" value=""></option>
                                                    @endif
                                                    @foreach($classes as $class)
                                                        <option value="{{ $class->id }}">{{ $class->abr }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Statut</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="status_id" id="status_id">
                                                    @if($student->status_id)
                                                    <option selected="" value="{{ $student->status_id }}">{{ $student->status->name }}</option>
                                                    @else
                                                    <option selected="" value=""></option>
                                                    @endif
                                                    @foreach($states as $state)
                                                        <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Régime</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="regime_id" id="regime_id">
                                                    @if($student->regime_id)
                                                    <option selected="" value="{{ $student->regime_id }}">{{ $student->regime->name }}</option>
                                                    @else
                                                    <option selected="" value=""></option>
                                                    @endif
                                                    @foreach($regimes as $regime)
                                                        <option value="{{ $regime->id }}">{{ $regime->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Date d'inscription</label>
                                                <input type="date" name="subscription_date" class="form-control" id="subscription_date" value="{{ $student->subscription_date }}">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            {{--                                        Divider--}}
                                            <div class="col-md-12">
                                                <hr/>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="state" id="state" value="new" {{ ($student->state == "new") ? "checked" : "" }}/>
                                                    <label class="form-check-label" for="inlineRadio1">Nouveau</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="state" id="state" value="old" {{ ($student->state == "old") ? "checked" : "" }}/>
                                                    <label class="form-check-label" for="inlineRadio1">Ancien</label>
                                                </div>
                                                <hr/>
                                            </div>
                                            {{--                                        End divider--}}
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">LV2</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="second_language_id" id="second_language_id">
                                                    <option selected="" value="{{ $student->second_language }}">{{ $student->second_language }}</option>
                                                    @foreach($lv2s as $lv2)
                                                        <option value="{{ $lv2->id }}">{{ $lv2->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Arts</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="art_id" id="art_id">
                                                    <option selected="" value="{{ $student->art }}">{{ $student->art }}</option>
                                                    @foreach($arts as $art)
                                                        <option value="{{ $art->id }}">{{ $art->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            {{--                                        Divider--}}
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1">REDOUBLANT(E) </label><br/>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="repeating" id="inlineRadio1" value="true" {{ ($student->repeating == "true") ? "checked" : "" }}/>
                                                    <label class="form-check-label" for="inlineRadio1">Oui</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="repeating" id="inlineRadio1" value="false" {{ ($student->repeating == "false") ? "checked" : "" }}/>
                                                    <label class="form-check-label" for="inlineRadio1">Non</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="repeating" id="inlineRadio1" value="triple" {{ ($student->repeating == "triple") ? "checked" : "" }}/>
                                                    <label class="form-check-label" for="inlineRadio1">Triple</label>
                                                </div>
                                                <hr/>
                                            </div>
                                            {{--                                        End divider--}}
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Dispense (sport) </label>
                                                &nbsp;
                                                &nbsp;
                                                &nbsp;
                                                &nbsp;
                                                &nbsp;
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="sport" id="inlineRadio1" value="true" {{ ($student->sport == "true") ? "checked" : "" }}/>
                                                    <label class="form-check-label" for="inlineRadio1">Oui</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="sport" id="inlineRadio2" value="false" {{ ($student->sport == "false") ? "checked" : "" }}/>
                                                    <label class="form-check-label" for="inlineRadio2">Non</label>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Photo 4x4 </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <input type="file" name="picture_link" class="form-control" id="picture_link" accept="image/*">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        @if(null != $student->picture_link)
                                            <div class="col-md-4">
                                                <img src="{{ asset('/'.$student->picture_link) }}" width="150" height="150" />
                                                <!-- /.form-group -->
                                            </div>
                                            <!-- /.col -->
                                        @endif
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Pièces Jointes </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Elements de dossier fournis</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="other_file" id="other_file">
                                                    <option selected="" value=""></option>
                                                    @foreach($rds as $rd)
                                                        <option value="{{ $rd->name }}">{{ $rd->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="file" name="other_file_attached" class="form-control" id="other_file_attached">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-12">
                                            <table id="example2" class="table table-bordered table-hover table-responsive-lg">
                                            <thead>
                                            <tr>
                                                <th>Nom</th>
                                                <th>Catégorie</th>
                                                <th>Taille</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($attached_files as $attached_file)
                                                <tr>
                                                    <td>
                                                        {{ $attached_file->name }}
                                                    </td>
                                                    <td>
                                                        {{ $attached_file->file_type }}
                                                    </td>
                                                    <td>
                                                        {{ $attached_file->size }}
                                                    </td>
                                                    <td>
                                                        <a href="{{ url('/'.$attached_file->link) }}" title="{{ $attached_file->name }}" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Nom</th>
                                                <th>Catégorie</th>
                                                <th>Taille</th>
                                                <th>Action</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>

                                <div class="card-footer text-right">
                                    <a href="{{ url('/admin/subscriptions') }}" class="btn btn-outline-primary">Retour</a>
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                        </form>
                        @else
                            @if(Session::has('not_found'))
                                <div class="alert alert-danger text-center" role="alert">
                                    {{ Session::get('not_found') }}
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
