@extends('layouts.admin_layouts.admin_layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Inscription Complète</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                        <li class="breadcrumb-item active">Inscriptions</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Table -->
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <form action="{{ url('/admin/subscriptions/create-step-one') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    {{-- Divider--}}
                                    <div class="col-md-12">
                                        <div class="bg-secondary text-bold text-center"> Etat civil </div>
                                        <hr />
                                    </div>
                                    {{-- End divider--}}
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Matricule <sup class="text-danger">*</sup></label>
                                            <input type="text" name="matricule" class="form-control" id="matricule" placeholder="Entrer le matrcicule" value="{{ $matricule }}" readonly>
                                            @if(null == $school)
                                            <input type="hidden" name="school_name" class="form-control" id="school_name" value="null" required>
                                            @else
                                            <input type="hidden" name="school_name" class="form-control" id="school_name" value="{{ $school['id'] }}">
                                            @endif
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nom <sup class="text-danger">*</sup></label>
                                            <input type="text" name="name" class="form-control" id="name" placeholder="Entrer un nom" value="{{ $student_name }}" required>
                                            @if(null == Auth::guard('admin')->user())
                                            <input type="hidden" name="agent" class="form-control" id="agent" value="null" required>
                                            @else
                                            <input type="hidden" name="agent" class="form-control" id="agent" value="{{ ucwords(Auth::guard('admin')->user()->username) }}">
                                            @endif
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Mail</label>
                                            <input type="email" name="email" class="form-control" id="email" value="">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Sexe <sup class="text-danger">*</sup></label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="sex" id="sex" required>
                                                <option value="Masculin">Masculin</option>
                                                <option value="Féminin">Feminin</option>
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Ville</label>
                                            <input type="text" name="city" class="form-control" id="city" placeholder="Entrer une ville">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Autre Matricule</label>
                                            <input type="text" name="other_matricule" class="form-control" id="other_matricule" placeholder="Autre matricule">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Prénom <sup class="text-danger">*</sup></label>
                                            <input type="text" name="surname" class="form-control" id="surname" placeholder="Entrer un prénom" value="{{ $student_surname }}" required>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Profession</label>
                                            <input type="text" name="profession" class="form-control" id="profession" placeholder="Entrer une profession">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Phone</label>
                                            <input type="text" name="phone" class="form-control" id="phone" value="">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Quartier</label>
                                            <input type="text" name="neighborhood" class="form-control" id="neighborhood" placeholder="Entrer un quartier">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Année <sup class="text-danger">*</sup></label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="year_id" id="year_id" required>
                                                @foreach( $academicYears as $year)
                                                <option value="{{ $year->id }}">{{ $year->year }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Date de naissance <sup class="text-danger">*</sup></label>
                                            <input type="date" name="birthday_date" class="form-control" id="birthday_date" required>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Lieu de naissance</label>
                                            <input type="text" name="birthday_place" class="form-control" id="birthday_place" value="{{ $birthay_place }}" placeholder="Lieu de naissance">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nationalité</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="nationality" id="nationality">
                                                <option selected="" value=""></option>
                                                @foreach( $countries as $country)
                                                <option value="{{ $country->nationality }}">{{ $country->nationality.' ('.$country->name.' - '.$country->code.')' }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Adresse</label>
                                            <input type="text" name="address" class="form-control" id="address" placeholder="Entrer une adresse">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    {{-- Divider--}}
                                    <div class="col-md-12">
                                        <hr />
                                        <div class="bg-secondary text-bold text-center"> Autre </div>
                                        <hr />
                                    </div>
                                    {{-- End divider--}}
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Situation matrimoniale</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="marital" id="marital">
                                                <option value="Married">Marié(e)</option>
                                                <option value="Single">Célibataire</option>
                                                <option value="Widow(er)">Veuf(veuve)</option>
                                                <option value="Divorced">Divorcé(e)</option>
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nombre d'enfant</label>
                                            <input type="number" name="child" class="form-control" value="0" id="child">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nom conjoint</label>
                                            <input type="text" name="name_partner" class="form-control" id="name_partner">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Téléphone conjoint</label>
                                            <input type="text" name="phone_partner" class="form-control" id="phone_partner" placeholder="">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email conjoint</label>
                                            <input type="email" name="email_partner" class="form-control" id="email_partner" placeholder="">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Profession conjoint</label>
                                            <input type="text" name="profession_partner" class="form-control" id="profession_partner" placeholder="">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    {{-- Divider--}}
                                    <div class="col-md-12">
                                        <hr />
                                        <div class="bg-secondary text-bold text-center"> Information médicales </div>
                                        <hr />
                                    </div>
                                    {{-- End divider--}}
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Maladie chroniques</label>
                                            <input type="text" name="chronic_disease" class="form-control" id="chronic_disease" placeholder="">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Allergies</label>
                                            <input type="text" name="allergy" class="form-control" id="allergy" placeholder="">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Autres observations médicales</label>
                                            <input type="text" name="medical_observation" class="form-control" id="medical_observation" placeholder="">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    {{-- Divider--}}
                                    <div class="col-md-12">
                                        <div class="bg-secondary text-bold text-center"> Filiation : Père</div>
                                        <hr />
                                    </div>
                                    {{-- End divider--}}
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Saisie Manuelle </label>
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="father_manual" id="inlineRadio1" value="oui" checked />
                                                <label class="form-check-label" for="inlineRadio1">Oui</label>
                                            </div>

                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="father_manual" id="inlineRadio2" value="non" />
                                                <label class="form-check-label" for="inlineRadio2">Non</label>
                                            </div>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Père</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="father_id" id="father_id">
                                                <option selected="" value="">Selectionner</option>
                                                @foreach($fathers as $father)
                                                <option value="{{ $father->id }}">{{ $father->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nom du Père</label>
                                            <input type="text" name="father_name" class="form-control" id="father_name" value="{{ $father_name }}" placeholder="Entrer le nom">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Profession du Père</label>
                                            <input type="text" name="father_profession" class="form-control" id="father_profession" value="{{ $father_profession }}" placeholder="Entrer la profession">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Téléphone du Père</label>
                                            <input type="text" name="father_phone" class="form-control" id="father_phone" value="{{ $father_phone }}" placeholder="Entrer le numéro téléphonique">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email du Père</label>
                                            <input type="email" name="father_email" class="form-control" id="father_email" placeholder="">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->

                                    {{-- Divider--}}
                                    <div class="col-md-12">
                                        <div class="bg-secondary text-bold text-center"> Filiation : Mère</div>
                                        <hr />
                                    </div>
                                    {{-- End divider--}}
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Saisie Manuelle </label>
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="mother_manual" id="inlineRadio1" value="oui" checked />
                                                <label class="form-check-label" for="inlineRadio1">Oui</label>
                                            </div>

                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="mother_manual" id="inlineRadio2" value="non" />
                                                <label class="form-check-label" for="inlineRadio2">Non</label>
                                            </div>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Mère</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="mother_id" id="mother_id">
                                                <option selected="" value="">Selectionner</option>
                                                @foreach($mothers as $mother)
                                                <option value="{{ $mother->id }}">{{ $mother->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nom de la Mère</label>
                                            <input type="text" name="mother_name" class="form-control" id="mother_name" value="{{ $father_name }}" placeholder="Entrer le nom">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Profession de la Mère</label>
                                            <input type="text" name="mother_profession" class="form-control" id="mother_profession" value="{{ $father_profession }}" placeholder="Entrer la profession">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Téléphone de la Mère</label>
                                            <input type="text" name="mother_phone" class="form-control" id="mother_phone" value="{{ $father_phone }}" placeholder="Entrer le numéro téléphonique">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email de la Mère</label>
                                            <input type="email" name="mother_email" class="form-control" id="mother_email" placeholder="">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    {{-- Divider--}}
                                    <div class="col-md-12">
                                        <div class="bg-secondary text-bold text-center"> Filiation : Tuteur</div>
                                        <hr />
                                    </div>
                                    {{-- End divider--}}
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Saisie Manuelle </label>
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="tutor_manual" id="tutor_manual" value="oui" />
                                                <label class="form-check-label" for="inlineRadio1">Oui</label>
                                            </div>

                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="tutor_manual" id="tutor_manual" value="non" checked />
                                                <label class="form-check-label" for="inlineRadio2">Non</label>
                                            </div>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Tuteur</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="tutor_id" id="tutor_id">
                                                <option selected="" value="">Selectionner</option>
                                                @foreach($tutors as $tutor)
                                                <option value="{{ $tutor->id }}">{{ $tutor->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nom du Tuteur</label>
                                            <input type="text" name="tutor_name" class="form-control" id="tutor_name" placeholder="Entrer le nom">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Profession du Tuteur</label>
                                            <input type="text" name="tutor_profession" class="form-control" id="tutor_profession" placeholder="Entrer la profession">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Téléphone du Tuteur</label>
                                            <input type="text" name="tutor_phone" class="form-control" id="tutor_phone" placeholder="Entrer le numéro téléphonique">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email du Tuteur</label>
                                            <input type="email" name="tutor_email" class="form-control" id="tutor_email" placeholder="">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    {{-- Divider--}}
                                    <div class="col-md-12">
                                        <hr />
                                        <div class="bg-secondary text-bold text-center"> Parcours </div>
                                        <hr />
                                    </div>
                                    {{-- End divider--}}
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Diplome</label>
                                            <input type="text" name="diploma" class="form-control" value="" id="diploma" placeholder="">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Année</label>
                                            <input type="text" name="diploma_year" class="form-control" value="" id="diploma_year" placeholder="">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Filière</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="faculty_id" id="faculty_id">
                                                <option selected="" value="">Selectionner une filière</option>
                                                @foreach($sections as $section)
                                                <option value="{{ $section->id }}">{{ $section->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Moyenne</label>
                                            <input type="text" name="diploma_mean" class="form-control" value="" id="diploma_mean">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Mention</label>
                                            <input type="text" name="diploma_mention" class="form-control" value="" id="diploma_mention">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Niveau précédent</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="previous_level_id" id="previous_level_id">
                                                <option selected="" value="">Selectionner un niveau</option>
                                                @foreach($levels as $level)
                                                <option value="{{ $level->id }}">{{ $level->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Ecole d'origine</label>
                                            <input type="text" name="old_school" class="form-control" value="" id="old_school">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    {{-- Divider--}}
                                    <div class="col-md-12">
                                        <hr />
                                        <div class="bg-secondary text-bold text-center"> Inscription </div>
                                        <hr />
                                    </div>
                                    {{-- End divider--}}
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Branche</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="branch_id" id="branch_id">
                                                <option selected="" value="">Selectionner une branche</option>
                                                @foreach($branches as $branch)
                                                <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Classe</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="class_id" id="class_id">
                                                <option selected="" value="">Selectionner une classe</option>
                                                @foreach($classes as $class)
                                                <option value="{{ $class->id }}">{{ $class->abr }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Statut</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="status_id" id="status_id">
                                                <option selected="" value=""></option>
                                                @foreach($states as $state)
                                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Régime</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="regime_id" id="regime_id">
                                                <option selected="" value=""></option>
                                                @foreach($regimes as $regime)
                                                <option value="{{ $regime->id }}">{{ $regime->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Date d'inscription</label>
                                            <input type="date" name="subscription_date" class="form-control" id="subscription_date">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        {{-- Divider--}}
                                        <div class="col-md-12">
                                            <hr />
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="state" id="state" value="new" checked />
                                                <label class="form-check-label" for="inlineRadio1">Nouveau</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="state" id="state" value="old" />
                                                <label class="form-check-label" for="inlineRadio1">Ancien</label>
                                            </div>
                                            <hr />
                                        </div>
                                        {{-- End divider--}}
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">LV2</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="second_language_id" id="second_language_id">
                                                <option selected="" value=""></option>
                                                @foreach($lv2s as $lv2)
                                                <option value="{{ $lv2->id }}">{{ $lv2->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Arts</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="art_id" id="art_id">
                                                <option selected="" value=""></option>
                                                @foreach($arts as $art)
                                                <option value="{{ $art->id }}">{{ $art->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        {{-- Divider--}}
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1">REDOUBLANT(E) </label><br />
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="repeating" id="inlineRadio1" value="true" />
                                                <label class="form-check-label" for="inlineRadio1">Oui</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="repeating" id="inlineRadio1" value="false" checked />
                                                <label class="form-check-label" for="inlineRadio1">Non</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="repeating" id="inlineRadio1" value="triple" />
                                                <label class="form-check-label" for="inlineRadio1">Triple</label>
                                            </div>
                                            <hr />
                                        </div>
                                        {{-- End divider--}}
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Dispense (sport) </label>
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="sport" id="inlineRadio1" value="true" />
                                                <label class="form-check-label" for="inlineRadio1">Oui</label>
                                            </div>

                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="sport" id="inlineRadio2" value="false" checked />
                                                <label class="form-check-label" for="inlineRadio2">Non</label>
                                            </div>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    {{-- Divider--}}
                                    <div class="col-md-12">
                                        <hr />
                                        <div class="bg-secondary text-bold text-center"> Photo 4x4 </div>
                                        <hr />
                                    </div>
                                    {{-- End divider--}}
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="file" name="picture_link" class="form-control" id="picture_link" accept="image/*">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    {{-- Divider--}}
                                    <div class="col-md-12">
                                        <hr />
                                        <div class="bg-secondary text-bold text-center"> Pièces Jointes </div>
                                        <hr />
                                    </div>
                                    {{-- End divider--}}
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Elements de dossier fournis</label>
                                            <select class="form-control select2bs4" style="width: 100%;" name="other_file" id="other_file">
                                                <option selected="" value=""></option>
                                                @foreach($rds as $rd)
                                                <option value="{{ $rd->name }}">{{ $rd->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="file" name="other_file_attached" class="form-control" id="other_file_attached">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>

                            <div class="card-footer text-right">
                                <a href="{{ url('/admin/subscriptions') }}" class="btn btn-outline-primary">Retour</a>
                                <button type="submit" class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.row -->

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
