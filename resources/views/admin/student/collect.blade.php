@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Encaissement</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Encaissement</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <form action="{{ url('/admin/subscriptions/find-collect') }}" method="POST">
                            @csrf

                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Recherche du dossier </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Matricule</label>
                                                <input type="text" name="matricule" class="form-control" id="matricule">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom et prénom</label>
                                                <input type="text" name="name" class="form-control" id="name">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>

                                <div class="card-footer text-right">
                                    <i>La recherche se fait par le matricule ou par le nom</i> &nbsp;&nbsp;
                                    <button type="submit" class="btn btn-primary">Rechercher</button>
                                </div>
                            </div>
                        </form>
                        @if($found)
                            <form action="{{ url('/admin/subscriptions/create-step-three') }}" method="POST">
                                {{--                                        Divider--}}
                                <div class="col-md-12">
                                    <hr/>
                                    <div class="bg-warning text-bold text-center"> Liste des encaissement pour le compte de
                                        <i>"{{ $student->name.' '.$student->surname }}"</i>
                                    </div>
                                    <hr/>
                                </div>
                                {{--                                        End divider--}}
                                <div class="card">
                                    <div class="card-footer text-right">
                                        <a href="{{ url('/admin/subscriptions/add-collect/'.$student->id) }}" style="max-width:200px; float: right; display: inline-block" class="btn btn-outline-success  mr-2">Nouvelle encaissement</a>
                                    </div>
                                    <div class="card-body">
                                        <table id="example2" class="table table-bordered table-hover table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Année</th>
                                                <th>Classe</th>
                                                <th>Rubrique</th>
                                                <th>Total</th>
                                                <th>Déjà versé</th>
                                                <th>Reste</th>
                                                <th>Action</th>
                                                <th>Nouveau versement</th>
                                                <th>En lettre</th>
                                                <th>Reçu</th>
                                                <th>Détail</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($collects as $collect)
                                                <tr>
                                                    <td>
                                                        {{ $collect->academicYear->year }}
                                                    </td>
                                                    <td>
                                                        <?php
                                                            $classe = App\Models\Classe::where('id',$collect->class->id)->first();
                                                        ?>
                                                        {{ $classe->level->name.''.$classe->abr.' '.$classe->section->name }}
                                                    </td>
                                                    <td>
                                                        {{ $collect->rubrique->name }}
                                                    </td>
                                                    <td>
                                                        {{ $collect->total }}
                                                    </td>
                                                    <td>
                                                        {{ $collect->advance }}
                                                    </td>
                                                    <td>
                                                        {{ $collect->total - $collect->advance }}
                                                    </td>
                                                    <td>
                                                        <ul>
                                                            <li>Reporter</li>
                                                            <li>Effacer</li>
                                                        </ul>
                                                    </td>
                                                    <td>
                                                        @if($collect->total - $collect->advance == 0)
                                                            Soldé
                                                        @else
                                                            <a href="{{url('/admin/subscriptions/update-collect/'.$collect->id)}}"><i class="fas fa-pen"></i></a>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php
                                                            $fmt = new NumberFormatter('fr_FR', NumberFormatter::SPELLOUT);
                                                        ?>
                                                        {{ $fmt->format($collect->total) }}
                                                    </td>
                                                    <td>
                                                       <a href="{{url('/admin/subscriptions/collect-ticket/'.$collect->student_id)}}" target="_blank">Cliquer reçu max</a>
                                                    </td>
                                                    <td>
                                                        Cliquer liste de paiements
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th class="bg-secondary"></th>
                                                <th class="bg-secondary"></th>
                                                <th class="bg-secondary"></th>
                                                <th>{{ $total_total }}</th>
                                                <th>{{ $total_advance }}</th>
                                                <th>{{ $total_rest }}</th>
                                                <th class="bg-secondary"></th>
                                                <th class="bg-secondary"></th>
                                                <th class="bg-secondary"></th>
                                                <th class="bg-secondary"></th>
                                                <th class="bg-secondary"></th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        <!-- /.row -->
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
