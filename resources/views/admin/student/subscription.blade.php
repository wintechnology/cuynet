@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Inscriptions</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Inscriptions</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success_message'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('success_message') }}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
{{--                                <a href="{{ url('/admin/subscriptions/collect') }}" style="max-width:200px; float: right; display: inline-block" class="btn btn-outline-success  mr-2">Encaissements</a>--}}
                                <a href="{{ url('/admin/subscriptions/create-step-one') }}" style="max-width:200px; float: right; display: inline-block" class="btn btn-outline-success mr-2">Inscription complète</a>
                                <a href="{{ url('/admin/subscriptions/create-step-two') }}" style="max-width:200px; float: right; display: inline-block" class="btn btn-outline-success  mr-2">Inscription simplifiée</a>
                                <a href="{{ url('/admin/subscriptions/create-step-three') }}" style="max-width:200px; float: right; display: inline-block" class="btn btn-outline-success  mr-2">Re-inscription</a>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover table-responsive">
                                    <thead>
                                    <tr>
                                        <th>Matricule</th>
                                        <th>Noms & prénoms</th>
                                        <th>Sexe</th>
                                        <th>Classe</th>
                                        <th>Date naissance</th>
                                        <th>Lieu naissance</th>
                                        <th>Date inscription</th>
                                        <th>Agent inscription</th>
                                        <th>Année Scolaire</th>
                                        <th>Situation</th>
                                        <th>Statut</th>
                                        <th>Régime</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($students as $student)
                                        <tr>
                                            <td>
                                                {{ $student->matricule }}
                                            </td>
                                            <td>
                                                {{ $student->name.' '.$student->surname }}
                                            </td>
                                            <td>
                                                {{ $student->sex }}
{{--                                                @if("M" == $student->sex)--}}
{{--                                                    Masculin--}}
{{--                                                @else--}}
{{--                                                    Féminin--}}
{{--                                                @endif--}}
                                            </td>
                                            <td>
                                                @if(null != $student->classe)
                                                    <?php
                                                        $class = App\Models\Classe::where('id',$student->classe->id)->first();
                                                    ?>
                                                    {{ $class->level->name.''.$class->abr.' '.$class->section->name }}
                                                @else

                                                @endif
                                            </td>
                                            <td>
                                                {{ date("d/m/Y", strtotime($student->birthday_date)) }}
                                            </td>
                                            <td>
                                                {{ $student->birthday_place }}
                                            </td>
                                            <td>
                                                {{ date("d/m/Y", strtotime($student->subscription_date)) }}
                                            </td>
                                            <td>
                                                {{ $student->subscription_agent }}
                                            </td>
                                            <td>
                                                @if(null != $student->academicYear)
                                                    {{ $student->academicYear->year }}
                                                @else

                                                @endif
                                            </td>
                                            <td>
                                                @if($student->situation)
                                                    inscrit
                                                @else
                                                    non inscrit
                                                @endif
                                            </td>
                                            <td>
                                                @if(null != $student->status)
                                                    {{ $student->status->name }}
                                                @else

                                                @endif
                                            </td>
                                            <td>
                                                @if(null != $student->regime)
                                                    {{ $student->regime->name }}
                                                @else

                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ url('/admin/subscriptions/student-file-pdf/'.$student->id) }}" title="Fiche individuelle" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                <a href="{{ url('/admin/subscriptions/collect/'.$student->id) }}" title="Encaissement"><i class="fas fa-money-bill"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Matricule</th>
                                        <th>Nom & prénom</th>
                                        <th>Sexe</th>
                                        <th>Classe</th>
                                        <th>Date naissance</th>
                                        <th>Lieu naissance</th>
                                        <th>Date inscription</th>
                                        <th>Agent inscription</th>
                                        <th>Année Scolaire</th>
                                        <th>Situation</th>
                                        <th>Statut</th>
                                        <th>Régime</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
