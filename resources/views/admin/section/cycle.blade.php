@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Cycles</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Cycles</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row">
                    @if(Session::has('success_message'))
                        <div class="alert alert-success col-12" role="alert">
                            {{ Session::get('success_message') }}
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Liste des Cycles</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Noms</th>
                                        <th>Statut</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cycles as $cycle)
                                        <tr>
                                            <td>{{ $cycle->name }}</td>
                                            <td>
                                                @if($cycle->status == 1)
                                                    <a class="updateCyclesStatus" id="cycle-{{ $cycle->id }}" cycle_id="{{ $cycle->id }}" href="javascript:void(0)">Active</a>
                                                @else
                                                    <a class="updateCyclesStatus" id="cycle-{{ $cycle->id }}" cycle_id="{{ $cycle->id }}" href="javascript:void(0)">Inactive</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Noms</th>
                                        <th>Statut</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->

                    <div class="col-4">
                        <form name="cycleForm" id="cycleForm" action="{{ url('admin/add-edit-cycle') }}" method="post" enctype="multipart/form-data">@csrf
                            <!-- SELECT2 EXAMPLE -->
                            <div class="card card-default">
                                <div class="card-header">
                                    <h3 class="card-title">Nouveau cycle</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom</label>
                                                <input onchange="nameEntered(this.value)" type="text" name="name" class="form-control" id="name" placeholder="Entrer le nom" required>
                                                <span class="text-red" id="name_res"></span>
                                                <input type="hidden" name="link" value="1">
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button id="btn_submit" type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                            <!-- /.card -->
                        </form>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <script type='text/javascript'>
        var js_array = <?=json_encode($cycles_name)?>;

        function nameEntered(input) {
            const button = document.getElementById('btn_submit');

            for (var i = 0; i < js_array.length; i++) {
                if(js_array.indexOf(input.toUpperCase()) !== -1)
                {
                    document.getElementById('name_res').innerHTML = "Ce cycle existe déjà";
                    button.setAttribute('disabled', '');
                } else {
                    document.getElementById('name_res').innerHTML = "";
                    button.removeAttribute('disabled');
                }
            }
        }
    </script>
@endsection
