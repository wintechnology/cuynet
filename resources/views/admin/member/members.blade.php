@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Classes</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Classes</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success_message'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('success_message') }}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Classes</h3>
{{--                                <a href="{{ url('/admin/add-edit-member') }}" style="max-width:250px; float: right; display: inline-block" class="btn btn-block btn-outline-success">Ajouter une structure</a>--}}
                                <a href="#" style="max-width:250px; float: right; display: inline-block" class="btn btn-block btn-outline-success">Ajouter une classe</a>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Filières/Sections</th>
                                        <th>Nom de la classe</th>
                                        <th>Salle</th>
                                        <th>Bâtiment</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($members as $member)
                                        <tr>
                                            <td>{{ $member->reference }}</td>
                                            <td>{{ $member->company_name }}</td>
                                            <td>{{ $member->activity_sector }}</td>
                                            <td>{{ $member->country }}</td>
                                            <td>{{ $member->city }}</td>
                                            <td>
                                                @if($member->status == 1)
                                                    <a class="updateMemberStatus" id="member-{{ $member->id }}" member_id="{{ $member->id }}" href="javascript:void(0)">Active</a>
                                                @else
                                                    <a class="updateMemberStatus" id="member-{{ $member->id }}" member_id="{{ $member->id }}" href="javascript:void(0)">Inactive</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Code</th>
                                        <th>Filières/Sections</th>
                                        <th>Nom de la classe</th>
                                        <th>Salle</th>
                                        <th>Bâtiment</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
