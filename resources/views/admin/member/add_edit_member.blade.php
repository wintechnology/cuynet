@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Structure</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Structures</a></li>
                            <li class="breadcrumb-item active">Admin Structure</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <form name="memberForm" id="MemberForm" action="{{ url('admin/add-edit-member') }}" method="post" enctype="multipart/form-data">@csrf
                    <!-- SELECT2 EXAMPLE -->
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">{{ $title }}</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Reférence</label>
                                        <input type="text" name="reference" class="form-control" id="reference" placeholder="Entrer une reférence" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Raison sociale</label>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Entrer la raison sociale" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Secteur d'activité</label>
                                        <select class="form-control select2bs4" style="width: 100%;" name="activity_sector" id="activity_sector" required>
                                            <option selected="" value="">Selectionner un secteur d'activité</option>
                                            @foreach($getSa as $sa)
                                                <option value="{{ $sa->name }}">{{ $sa->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Phone</label>
                                        <input type="text" name="phone" class="form-control" id="phone" placeholder="Entrer le numéro téléphonique">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pays</label>
                                        <select class="form-control select2bs4" style="width: 100%;" name="country" id="country" required>
                                            <option selected="" value="">Selectionner un pays</option>
                                            @foreach($getCountries as $country)
                                                <option value="{{ $country->name }}">{{ $country->name }} ({{ $country->code }})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Ville</label>
                                        <select class="form-control select2bs4" style="width: 100%;" name="city" id="city" required>
                                            <option selected="" value="">Selectionner une ville</option>
                                            @foreach($getCities as $city)
                                                <option value="{{ $city->name }}">{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Type de la structure</label>
                                        <select class="form-control select2bs4" style="width: 100%;" name="type" id="type" required>
                                            <option selected="" value="Public">Public</option>
                                            <option value="Privée">Privée</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control" id="email" placeholder="Entrer l'email">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    <!-- /.card -->
                </form>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
