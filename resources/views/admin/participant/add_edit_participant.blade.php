@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Participant</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Participants</a></li>
                            <li class="breadcrumb-item active">Admin Participant</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <form name="memberForm" id="MemberForm" action="{{ url('admin/add-edit-participant') }}" method="post" enctype="multipart/form-data">@csrf
                    <!-- SELECT2 EXAMPLE -->
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">{{ $title }}</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nom(s)</label>
                                        <input type="text" name="names" class="form-control" id="names" placeholder="Entrer le(s) nom(s)" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Prénom(s)</label>
                                        <input type="text" name="surnames" class="form-control" id="surnames" placeholder="Entrer le(s) prénom(s)" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date de naissance</label>
                                        <input type="date" name="birthday" class="form-control" id="birthday" placeholder="" value="{{ $date }}">
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Lieu de naissance</label>
                                        <input type="text" name="birthplace" class="form-control" id="birthplace" placeholder="Entrer le lieu de naissance" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Phone</label>
                                        <input type="text" name="phone" class="form-control" id="phone" placeholder="Entrer le numéro téléphonique">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Structure</label>
                                        <select class="form-control select2bs4" style="width: 100%;" name="company_id" id="company_id" required>
                                            <option selected="" value="">Selectionner une structure</option>
                                            @foreach($getMembers as $member)
                                                <option value="{{ $member->id }}">{{ $member->company_name }} ({{$member->reference}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Profession</label>
                                        <input type="text" name="profession" class="form-control" id="profession" placeholder="Entrer la profession" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Fonction</label>
                                        <input type="text" name="function" class="form-control" id="function" placeholder="Entrer la profession" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control" id="email" placeholder="Entrer l'email" required>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    <!-- /.card -->
                </form>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
