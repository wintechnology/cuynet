@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Stagiaire</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Stagiaires</a></li>
                            <li class="breadcrumb-item active">Admin Stagiaire</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <form name="internForm" id="InternForm" action="{{ url('admin/add-edit-intern') }}" method="post" enctype="multipart/form-data">@csrf
                    <!-- SELECT2 EXAMPLE -->
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">{{ $title }}</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nom(s)</label>
                                        <input type="text" name="names" class="form-control" id="names" placeholder="Entrer le(s) nom(s)" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Prénom(s)</label>
                                        <input type="text" name="surnames" class="form-control" id="surnames" placeholder="Entrer le(s) prénom(s)" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date de naissance</label>
                                        <input type="date" name="birthday" class="form-control" id="birthday" placeholder="" value="{{ $date }}">
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Lieu de naissance</label>
                                        <input type="text" name="birthplace" class="form-control" id="birthplace" placeholder="Entrer le lieu de naissance" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Phone</label>
                                        <input type="text" name="phone" class="form-control" id="phone" placeholder="Entrer le numéro téléphonique" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control" id="email" placeholder="Entrer l'email">
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Genre</label>
                                        <select class="form-control select2bs4" style="width: 100%;" name="gender" id="gender" required>
                                            <option selected="" value="">Selectionner un genre</option>
                                            <option value="M">M</option>
                                            <option value="F">F</option>
                                            <option value="N/A">N/A</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Marital</label>
                                        <select class="form-control select2bs4" style="width: 100%;" name="marital" id="marital" required>
                                            <option selected="" value="">Selectionner une situation maritale</option>
                                            <option value="Célibataire">Célibataire</option>
                                            <option value="Marié(e)">Marié(e)</option>
                                            <option value="Autre">Autre</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nombre d'enfant(s)</label>
                                        <input type="number" name="children" class="form-control" id="children" value="0" required>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Numéro CNI/Passport</label>
                                        <input type="text" name="id_card" class="form-control" id="id_card" value="Entrer le numéro pièce identité">
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Date expiration pièce d'indentité</label>
                                        <input type="date" name="id_card_validity" class="form-control" id="id_card_validity">
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nom(s) et prénom(s) du père</label>
                                        <input type="text" name="father_name" class="form-control" id="father_name" value="Entrer le nom du père">
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nom(s) et prénom(s) de la mère</label>
                                        <input type="text" name="mother_name" class="form-control" id="mother_name" value="Entrer le nom de la mère">
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Photo</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" name="pic" class="custom-file-input" id="pic" accept=".jpg,.jpeg,.png">
                                                <label class="custom-file-label" for="exampleInputFile">Choisir une image</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text">JPEG/JPG/PNG</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary" disabled>Submit</button>
                        </div>
                    </div>
                    <!-- /.card -->
                </form>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
