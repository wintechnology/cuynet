@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Prêts</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Prêts</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success_message'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('success_message') }}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Prêts</h3>
                                <a href="{{ url('/admin/add-edit-rent') }}" style="max-width:250px; float: right; display: inline-block" class="btn btn-block btn-outline-success">Ajouter un prêt</a>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Référence</th>
                                        <th>Emprunteur</th>
                                        <th>Date d'emprunt</th>
                                        <th>Echéance</th>
                                        <th>Date retour</th>
                                        <th>Quantité empruntée</th>
                                        <th>Quantité retournée</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($rents as $rent)
                                        <tr>
                                            <td>{{ $rent->ref }}</td>
                                            <td>{{ $rent->student->name }}</td>
                                            <td>{{ $rent->rentDate }}</td>
                                            <td>{{ $rent->delay }}</td>
                                            <td>{{ $rent->dateBack }}</td>
                                            <td>{{ $rent->quantityTake }}</td>
                                            <td>{{ $rent->quantityBack }}</td>
                                            <td>
                                                {{--                                                <a href="{{ url('/admin/academic_years/infos/'.$academic_year->id) }}"><i class="fas fa-eye"></i></a>--}}
                                                {{--                                                @if($course->status == 1)--}}
                                                {{--                                                    <a class="updateCourseStatus" id="course-{{ $course->id }}" course_id="{{ $course->id }}" href="javascript:void(0)"><i class="fas fa-toggle-on"></i></a>--}}
                                                {{--                                                @else--}}
                                                {{--                                                    <a class="updateCourseStatus" id="course-{{ $course->id }}" course_id="{{ $course->id }}" href="javascript:void(0)"><i class="fas fa-toggle-off"></i></a>--}}
                                                {{--                                                @endif--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Référence</th>
                                        <th>Emprunteur</th>
                                        <th>Date d'emprunt</th>
                                        <th>Echéance</th>
                                        <th>Date retour</th>
                                        <th>Quantité empruntée</th>
                                        <th>Quantité retournée</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
