@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Regimes</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Regimes</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row">
                    @if(Session::has('success_message'))
                        <div class="alert alert-success col-12" role="alert">
                            {{ Session::get('success_message') }}
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Liste des régimes</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Abréviation</th>
                                        <th>Nom</th>
                                        <th>Statut</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($regimes as $regime)
                                        <tr>
                                            <td>{{ $regime->abr }}</td>
                                            <td>{{ $regime->name }}</td>
                                            <td>
{{--                                                @if($regime->status == 1)--}}
{{--                                                    <a class="updateRegimesStatus" id="regime-{{ $regime->id }}" regime_id="{{ $regime->id }}" href="javascript:void(0)">Active</a>--}}
{{--                                                @else--}}
{{--                                                    <a class="updateRegimesStatus" id="regime-{{ $regime->id }}" regime_id="{{ $regime->id }}" href="javascript:void(0)">Inactive</a>--}}
{{--                                                @endif--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Abréviation</th>
                                        <th>Nom</th>
                                        <th>Statut</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->

                    <div class="col-4">
                        <form name="regimeForm" id="regimeForm" action="{{ url('admin/add-edit-regime') }}" method="post" enctype="multipart/form-data">@csrf
                            <!-- SELECT2 EXAMPLE -->
                            <div class="card card-default">
                                <div class="card-header">
                                    <h3 class="card-title">Nouveau régime</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Abréviation</label>
                                                <input type="text" name="abr" class="form-control" id="abr">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom <sup class="text-danger">*</sup> </label>
                                                <input type="text" name="name" class="form-control" id="name" required>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                            <!-- /.card -->
                        </form>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
