@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Enseignants</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Enseignants</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row">
                    @if(Session::has('success_message'))
                        <div class="alert alert-success col-12" role="alert">
                            {{ Session::get('success_message') }}
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Liste des enseignants</h3>
                                <a href="{{ url('/admin/add-edit-teacher') }}" style="max-width:250px; float: right; display: inline-block" class="btn btn-block btn-outline-success">Ajouter un enseignant</a>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Matricule</th>
                                        <th>Nom</th>
                                        <th>Téléphone</th>
                                        <th>Email</th>
                                        <th>Statut</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($teachers as $teacher)
                                        <tr>
                                            <td>{{ $teacher->matricule }}</td>
                                            <td>{{ $teacher->name }}</td>
                                            <td>{{ $teacher->phone }}</td>
                                            <td>{{ $teacher->mail }}</td>
                                            <td>
                                                @if($teacher->status == 1)
                                                    <a class="updateTeacherStatus" id="teacher-{{ $teacher->id }}" teacher_id="{{ $teacher->id }}" href="javascript:void(0)">Active</a>
                                                @else
                                                    <a class="updateTeacherStatus" id="teacher-{{ $teacher->id }}" teacher_id="{{ $teacher->id }}" href="javascript:void(0)">Inactive</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Matricule</th>
                                        <th>Nom</th>
                                        <th>Téléphone</th>
                                        <th>Email</th>
                                        <th>Statut</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->

                    <div class="col-4">
                        <form name="teacherForm" id="teacherForm" action="{{ url('admin/add-edit-teacher') }}" method="post" enctype="multipart/form-data">@csrf
                            <!-- SELECT2 EXAMPLE -->
                            <div class="card card-default">
                                <div class="card-header">
                                    <h3 class="card-title">Nouvel enseignant</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Matricule</label>
                                                <input type="text" name="matricule" class="form-control" id="matricule" placeholder="Entrer un matricule" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom</label>
                                                <input type="text" name="name" class="form-control" id="name" placeholder="Entrer un nom" required>
                                            </div>
{{--                                            <div class="form-group">--}}
{{--                                                <label for="exampleInputEmail1">Catégorie</label>--}}
{{--                                                <select class="form-control select2bs4" style="width: 100%;" name="category_id" id="category_id" required>--}}
{{--                                                    <option selected="" value="">Selectionner une catégorie</option>--}}
{{--                                                    @foreach($categories as $category)--}}
{{--                                                        <option value="{{ $category->id }}">{{ $category->name }}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Phone</label>
                                                <input type="text" name="phone" class="form-control" id="phone" value="000000000" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Mail</label>
                                                <input type="text" name="mail" class="form-control" id="mail" value="mail+1@xyz.cm" required>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                            <!-- /.card -->
                        </form>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
