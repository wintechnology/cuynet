@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Information sur l'école</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Ecole</a></li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <form name="classeForm" id="ClasseForm" action="{{ url('admin/add-edit-school') }}" method="post" enctype="multipart/form-data">@csrf
                    <!-- SELECT2 EXAMPLE -->
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">{{ $title }}</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Code</label>
                                        <input type="text" name="code" class="form-control" value="{{ $school->code }}">
                                        <input type="hidden" name="id" class="form-control" value="{{ $school->id }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Code Ministère</label>
                                        <input type="text" name="ministry_code" class="form-control" value="{{ $school->ministry_code }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Abréviation</label>
                                        <input type="text" name="abreviation" class="form-control" value="{{ $school->abreviation }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nom</label>
                                        <input type="text" name="name" class="form-control" value="{{ $school->name }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Pays</label>
                                        <select class="form-control select2bs4" style="width: 100%;" name="country_id" id="country_id">
                                            @if(null != $self_country)
                                            <option value="{{ $self_country->id }}">{{ $self_country->name }}</option>
                                            @else
                                            <option selected="" value=""></option>
                                            @endif
                                            @foreach( $countries as $country)
                                                @if( $country != $self_country)
                                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Ville</label>
                                        <input type="text" name="city" class="form-control" value="{{ $school->city }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Quartier</label>
                                        <input type="text" name="neighborhood" class="form-control" value="{{ $school->neighborhood }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Téléphone</label>
                                        <input type="text" name="phone" class="form-control" value="{{ $school->phone }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Adresse postale</label>
                                        <input type="text" name="postal_address" class="form-control" value="{{ $school->postal_address }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Situation géographique</label>
                                        <input type="text" name="geographic_situation" class="form-control" value="{{ $school->geographic_situation }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Site web</label>
                                        <input type="text" name="web_site" class="form-control" value="{{ $school->web_site }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">E-mail</label>
                                        <input type="text" name="email" class="form-control" value="{{ $school->email }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Logo</label>
                                        <input type="file" name="logo" class="form-control" value="" accept="image/*">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                @if(null != $school->logo)
                                <div class="col-md-4">
                                    <img src="{{ asset('/'.$school->logo) }}" width="350" height="300" />
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                @endif
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Devise</label>
                                        <input type="text" name="currency" class="form-control" value="{{ $school->currency }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                {{--                                        Divider--}}
                                <div class="col-md-12">
                                    <hr/>
                                    <div class="bg-secondary text-bold text-center"> Les responsables </div>
                                    <hr/>
                                </div>
                                {{--                                        End divider--}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Fondateur</label>
                                        <input type="text" name="founder" class="form-control" value="{{ $school->founder }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Directeur des études</label>
                                        <input type="text" name="studies_director" class="form-control" value="{{ $school->studies_director }}">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Enregistrer</button>
                        </div>
                    </div>
                    <!-- /.card -->
                </form>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
