@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Salles</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Salles</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row">
                    @if(Session::has('success_message'))
                        <div class="alert alert-success col-12" role="alert">
                            {{ Session::get('success_message') }}
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Liste des Salles</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Type</th>
                                        <th>Nom</th>
                                        <th>Bâtiment</th>
                                        <th>Statut</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($rooms as $room)
                                        <tr>
                                            <td>{{ $room->code }}</td>
                                            <td>
                                                @if(null != $room->roomType)
                                                {{ $room->roomType->name }}
                                                @endif
                                            </td>
                                            <td>{{ $room->name }}</td>
                                            <td>
                                                @if(null != $room->building)
                                                {{ $room->building->name }}
                                                @endif
                                            </td>
                                            <td>
                                                @if($room->status == 1)
                                                    <a class="updateRoomStatus" id="room-{{ $room->id }}" room_id="{{ $room->id }}" href="javascript:void(0)">Active</a>
                                                @else
                                                    <a class="updateRoomStatus" id="room-{{ $room->id }}" room_id="{{ $room->id }}" href="javascript:void(0)">Inactive</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Code</th>
                                        <th>Type</th>
                                        <th>Nom</th>
                                        <th>Bâtiment</th>
                                        <th>Statut</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->

                    <div class="col-4">
                        <form name="roomForm" id="roomForm" action="{{ url('admin/add-edit-room') }}" method="post" enctype="multipart/form-data">@csrf
                            <!-- SELECT2 EXAMPLE -->
                            <div class="card card-default">
                                <div class="card-header">
                                    <h3 class="card-title">Nouveau bâtiment</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Code</label>
                                                <input type="text" name="code" class="form-control" id="code" placeholder="Entrer un code">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom</label>
                                                <input type="text" name="name" class="form-control" id="name" placeholder="Entrer un nom" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Type de salle</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="roomType_id" id="roomType_id">
                                                    <option selected="" value="">Selectionner un type</option>
                                                    @foreach($roomTypes as $roomType)
                                                        <option value="{{ $roomType->id }}">{{ $roomType->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Bâtiment</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="building_id" id="building_id">
                                                    <option selected="" value="">Selectionner un bâtiment</option>
                                                    @foreach($buildings as $building)
                                                        <option value="{{ $building->id }}">{{ $building->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                            <!-- /.card -->
                        </form>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
