@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Rubriques</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Rubriques</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row">
                    @if(Session::has('success_message'))
                        <div class="alert alert-success col-12" role="alert">
                            {{ Session::get('success_message') }}
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Liste des rubriques</h3>
                                <a href="{{ url('/admin/add-edit-rubric') }}" style="max-width:250px; float: right; display: inline-block" class="btn btn-block btn-outline-success">Nouvelle Rubrique</a>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Année</th>
                                        <th>Libellé</th>
                                        <th>Abr</th>
                                        <th>Montant</th>
                                        <th>Classe</th>
                                        <th>Ligne de budget</th>
                                        <th>Eleve requis</th>
                                        <th>Statut</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($rubrics as $rubric)
                                        <tr>
                                            <td>{{ $rubric->aca_year->year }}</td>
                                            <td>{{ $rubric->name }}</td>
                                            <td>{{ $rubric->abr }}</td>
                                            <td>{{ $rubric->amount }}</td>
                                            <td>{{ $rubric->class->abr }}</td>
                                            <td>{{ $rubric->budget_line->name }}</td>
                                            <td>
                                                @if($rubric->student_required)
                                                OUI
                                                @else
                                                NON
                                                @endif
                                            </td>
                                            <td>
                                                @if($rubric->actif)
                                                Actif
                                                @else
                                                Inactif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Année</th>
                                        <th>Libellé</th>
                                        <th>Abr</th>
                                        <th>Montant</th>
                                        <th>Classe</th>
                                        <th>Ligne de budget</th>
                                        <th>Eleve requis</th>
                                        <th>Statut</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
