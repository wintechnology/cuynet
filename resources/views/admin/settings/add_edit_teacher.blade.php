@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Enregistrement d'un enseignant</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Enregistrement d'un enseignant</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <form action="{{ url('/admin/add-edit-teacher') }}" method="POST">
                            @csrf
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <div class="bg-secondary text-bold text-center"> Etat civil </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Ecole</label>
                                                @if(null == $school)
                                                    <input type="text" name="" class="form-control" id="date" value="N/A" readonly>
                                                    <input type="hidden" name="school_id" class="form-control" id="date" value="" required>
                                                @else
                                                    <input type="text" name="" class="form-control" id="date" value="{{ $school['name'] }}" readonly>
                                                    <input type="hidden" name="school_id" class="form-control" id="school_id" value="{{ $school['id'] }}">
                                                @endif
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Matricule</label>
                                                <input type="text" name="matricule" class="form-control" id="matricule" placeholder="Entrer le matrcicule">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom</label>
                                                <input type="text" name="name" class="form-control" id="name" placeholder="Entrer un nom" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Prénom</label>
                                                <input type="text" name="surname" class="form-control" id="surname" placeholder="Entrer un prénom" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Date de naissance</label>
                                                <input type="date" name="birthday" class="form-control" id="birthday" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Sexe</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="year_id" id="year_id">
                                                    @foreach( $sexes as $sex)
                                                        <option value="{{ $sex->id }}">{{ $sex->code }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Phone</label>
                                                <input type="text" name="phone" class="form-control" id="phone" value="000000000" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Profession</label>
                                                <input type="text" name="profession" class="form-control" id="profession" placeholder="Entrer une profession" required>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Année</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="year_id" id="year_id">
                                                    @foreach( $academicYears as $year)
                                                        <option value="{{ $year->id }}">{{ $year->year }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Autre Matricule</label>
                                                <input type="text" name="matricule_other" class="form-control" id="matricule_other" placeholder="Autre matricule">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Lieu de naissance</label>
                                                <input type="text" name="birthday_place" class="form-control" id="birthday_place" placeholder="Lieu de naissance">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nationalité</label>
                                                <input type="text" name="nationality" class="form-control" id="nationality" placeholder="Nationalité">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Mail</label>
                                                <input type="email" name="mail" class="form-control" id="mail" value="mail+1@xyz.cm" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Ville</label>
                                                <input type="text" name="ville" class="form-control" id="ville" placeholder="Entrer une ville" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Quartier</label>
                                                <input type="text" name="quartier" class="form-control" id="quartier" placeholder="Entrer un quartier" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Adresse</label>
                                                <input type="text" name="adresse" class="form-control" id="adresse" placeholder="Entrer une adresse" required>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
{{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Autre </div>
                                            <hr/>
                                        </div>
{{--                                        End divider--}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Situation matrimoniale</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="year_id" id="year_id">
                                                    <option value="Marié(e)">Marié(e)</option>
                                                    <option value="Célibataire">Célibataire</option>
                                                    <option value="Veuf(veuve)">Veuf(veuve)</option>
                                                    <option value="Divorcé(e)">Divorcé(e)</option>
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nombre d'enfant</label>
                                                <input type="number" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom conjoint</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames" placeholder="Entrer le nom">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Téléphone conjoint</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames" placeholder="">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email conjoint</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames" placeholder="">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Profession conjoint</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames" placeholder="">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>

                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Information médicales </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Maladie chroniques</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames" placeholder="">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Allergies</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames" placeholder="">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Autres observations médicales</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames" placeholder="">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Parcours </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Diplome</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Année</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Spécialité</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Statut</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Salaire de base</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Taux horaire</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Discipline </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Matière principale</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Matière enseignée</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Autres références </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Identifiant social</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Code Fiscal</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nature pièce d'identité</label>
                                                <select class="form-control select2bs4" style="width: 100%;" name="year_id" id="year_id">
                                                    <option value="CNI">CNI</option>
                                                    <option value="Passport">Passeport</option>
                                                    <option value="Autre">Autre</option>
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Numéro pièce</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Livrée le</label>
                                                <input type="date" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Lieu de délivrance</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Expire le</label>
                                                <input type="date" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center">Informations bancaires </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nom Banque</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Code Banque</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Code Guichet</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">N° Compte</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">RIB</label>
                                                <input type="text" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Photo 4x4 </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="file" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        {{--                                        Divider--}}
                                        <div class="col-md-12">
                                            <hr/>
                                            <div class="bg-secondary text-bold text-center"> Pièces Jointes </div>
                                            <hr/>
                                        </div>
                                        {{--                                        End divider--}}
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="file" name="surnames" class="form-control" id="surnames">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>

                                <div class="card-footer text-right">
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
