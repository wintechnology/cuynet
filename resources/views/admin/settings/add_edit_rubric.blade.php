@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Rubrique de paiement</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Rubrique de paiement</a></li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <form name="rubricForm" id="rubricForm" action="{{ url('admin/add-edit-rubric') }}" method="post" enctype="multipart/form-data">@csrf
                    <!-- SELECT2 EXAMPLE -->
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">{{ $title }}</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Année académique <sup class="text-danger">*</sup></label>
                                        <select class="form-control select2bs4" style="width: 100%;" name="year_id" id="year_id" required>
                                            <option selected="" value="">Selectionner un l'année scolaire</option>
                                            @foreach($academicYears as $academicYear)
                                                <option value="{{ $academicYear->id }}">{{ $academicYear->year }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Classe </label>
                                        <select class="form-control select2bs4" style="width: 100%;" name="class_id" id="class_id">
                                            <option selected="" value="">Selectionner une classe</option>
                                            @foreach($classes as $class)
                                                <option value="{{ $class->id }}">{{ $class->level->name.' '.$class->abr }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Abréviation <sup class="text-danger">*</sup> </label>
                                        <input type="text" name="abr" class="form-control" id="abr" required>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Libellé <sup class="text-danger">*</sup></label>
                                        <input onchange="nameEntered(this.value)" type="text" name="name" class="form-control" id="name" required>
                                        <span class="text-red" id="name_res"></span>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Actif </label>
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="actif" id="inlineRadio1" value="true" checked/>
                                            <label class="form-check-label" for="inlineRadio1">Oui</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="actif" id="inlineRadio2" value="false"/>
                                            <label class="form-check-label" for="inlineRadio2">Non</label>
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Montant <sup class="text-danger">*</sup></label>
                                        <input type="number" name="amount" class="form-control" id="amount" value="0" required>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Eleve requis </label>
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="student_required" id="inlineRadio1" value="true" />
                                            <label class="form-check-label" for="inlineRadio1">Oui</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="student_required" id="inlineRadio2" value="false" checked />
                                            <label class="form-check-label" for="inlineRadio2">Non</label>
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Ligne budget </label>
                                        <select class="form-control select2bs4" style="width: 100%;" name="budget_line_id" id="budget_line_id" required>
                                            <option selected="" value="">Selectionner un ligne de budget</option>
                                            @foreach($budget_lines as $budget_line)
                                                <option value="{{ $budget_line->id }}">{{ $budget_line->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{ url('/admin/rubrics') }}" class="btn btn-outline-primary">Retour</a>
                            <button type="submit" class="btn btn-primary" id="btn_submit">Enregistrer</button>
                        </div>
                    </div>
                    <!-- /.card -->
                </form>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script type='text/javascript'>
        var js_array = <?=json_encode($rubrics_name)?>;

        function nameEntered(input) {
            const button = document.getElementById('btn_submit');

            for (var i = 0; i < js_array.length; i++) {
                if(js_array.indexOf(input.toUpperCase()) !== -1)
                {
                    document.getElementById('name_res').innerHTML = "Cette rubrique existe déjà";
                    button.setAttribute('disabled', '');
                } else {
                    document.getElementById('name_res').innerHTML = "";
                    button.removeAttribute('disabled');
                }
            }
        }
    </script>
@endsection
