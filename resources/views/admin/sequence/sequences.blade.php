@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Sequences</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Sequences</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success_message'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('success_message') }}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Sequences</h3>
                                <a href="{{ url('/admin/add-edit-sequence') }}" style="max-width:250px; float: right; display: inline-block" class="btn btn-block btn-outline-success">Ajouter une sequence</a>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Année</th>
                                        <th>Trimestre</th>
                                        <th>Désignation</th>
                                        <th>Date début</th>
                                        <th>Date fin</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sequences as $sequence)
                                        <tr>
                                            <td>{{ $sequence->id }}</td>
                                            <td>{{ $sequence->year }}</td>
                                            <td>{{ $sequence->trimester->value }}</td>
                                            <td>{{ $sequence->value }}</td>
                                            <td>{{ date("d/m/Y", strtotime($sequence->start)) }}</td>
                                            <td>{{ date("d/m/Y", strtotime($sequence->end)) }}</td>
                                            <td>
                                                @if($sequence->status == 1)
                                                    <a class="updateSequenceStatus" id="sequence-{{ $sequence->id }}" sequence_id="{{ $sequence->id }}" href="javascript:void(0)">Actif</a>
                                                @else
                                                    <a class="updateSequenceStatus" id="sequence-{{ $sequence->id }}" sequence_id="{{ $sequence->id }}" href="javascript:void(0)">Inactif</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>N°</th>
                                        <th>Année</th>
                                        <th>Trimestre</th>
                                        <th>Désignation</th>
                                        <th>Date début</th>
                                        <th>Date fin</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
