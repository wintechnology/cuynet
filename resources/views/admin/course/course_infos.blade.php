@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Infos session de formation</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Session de formations</a></li>
                            <li class="breadcrumb-item active">Infos session de formation</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <form name="courseForm" id="CourseForm">
                    <!-- SELECT2 EXAMPLE -->
                    <div class="card card-default">
                        <div class="card-header">
                            <a href="{{ url('/admin/courses') }}" style="max-width:250px; float: left; display: inline-block" class="btn btn-block btn-info"> <i class="fas fa-arrow-alt-circle-left"></i> Retour</a>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Formation</label>
                                        <input type="text" name="formation" class="form-control" id="formation" value="{{ $course->formation->theme }}" readonly>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Lieu</label>
                                        <input type="text" name="place" class="form-control" id="place" value="{{$course->place}}" readonly>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date début</label>
                                        <input type="text" name="date_debut" class="form-control" id="date" value="{{ date("d/m/Y à G:i", strtotime($course->start)) }}" readonly>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date fin</label>
                                        <input type="text" name="date_fin" class="form-control" id="date" value="{{ date("d/m/Y à G:i", strtotime($course->end)) }}" readonly>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nombre de places</label>
                                        <input type="number" name="places" class="form-control" id="places" value="{{ $course->places }}" readonly>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <a href="{{ url('/admin/courses/stakeholders/'.$course->id) }}" style="max-width:300px;" class="btn btn-block btn-outline-primary">Liste des intervenants</a>
                                    <a href="{{ url('/admin/courses/participants/'.$course->id) }}" style="max-width:300px;" class="btn btn-block btn-outline-primary">Liste des participants</a>
                                    <a href="{{ url('/admin/courses/spends/'.$course->id) }}" style="max-width:300px;" class="btn btn-block btn-outline-primary">Détails des dépenses</a>
                                    <a href="{{ url('/admin/courses/guests/'.$course->id) }}" style="max-width:300px;" class="btn btn-block btn-outline-primary">Liste des invités</a>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">

                        </div>
                    </div>
                    <!-- /.card -->
                </form>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
