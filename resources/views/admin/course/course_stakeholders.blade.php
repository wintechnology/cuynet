@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Intervenants session de formations</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('/admin/courses/infos/'.$id) }}">Session de formation #{{$id}}</a></li>
                            <li class="breadcrumb-item active">Intervenants de session de formation</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success_message'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('success_message') }}
                            </div>
                        @endif
                        @if(Session::has('error_message'))
                            <div class="alert alert-danger" role="alert">
                                {{ Session::get('error_message') }}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <a href="{{ url('/admin/courses/infos/'.$id) }}" style="max-width:250px; float: left; display: inline-block" class="btn btn-block btn-info"> <i class="fas fa-arrow-alt-circle-left"></i> Retour</a>
                                <div class="text-center">
                                    <a href="" style="max-width:350px; float: right; display: inline-block" class="btn btn-block btn-outline-success" data-toggle="modal" data-target="#modalLoginForm">Ajouter un intervenant</a>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="tab-content p-0">
                                    <!-- Morris chart - Sales -->
                                    <div class="chart tab-pane active" id="revenue-chart"
                                         style="position: relative;">
                                        <table class="table table-bordered table-hover table-responsive-lg">
                                            <thead>
                                            <tr>
                                                <th>Noms</th>
                                                <th>Prenoms</th>
                                                <th>Profession</th>
                                                <th>Fonction</th>
                                                <th>Téléphone</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($stakeholders as $stakeholder)
                                            <tr>
                                                <td>{{ $stakeholder->names }}</td>
                                                <td>{{ $stakeholder->surnames }}</td>
                                                <td>{{ $stakeholder->profession }}</td>
                                                <td>{{ $stakeholder->function}}</td>
                                                <td>{{ $stakeholder->phone }}</td>
                                                <td>
                                                    {{--                                                            @if($stakeholder['status'] == 1)--}}
                                                    {{--                                                                <a class="updateStakeholderStatus"--}}
                                                    {{--                                                                   id="stakeholder-{{ $stakeholder->id }}"--}}
                                                    {{--                                                                   stakeholder_id="{{ $stakeholder->id }}"--}}
                                                    {{--                                                                   href="javascript:void(0)">Active</a>--}}
                                                    {{--                                                            @else--}}
                                                    {{--                                                                <a class="updateStakeholderStatus"--}}
                                                    {{--                                                                   id="stakeholder-{{ $stakeholder->id }}"--}}
                                                    {{--                                                                   stakeholder_id="{{ $stakeholder->id }}"--}}
                                                    {{--                                                                   href="javascript:void(0)">Inactive</a>--}}
                                                    {{--                                                            @endif--}}
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Noms</th>
                                                <th>Prenoms</th>
                                                <th>Profession</th>
                                                <th>Fonction</th>
                                                <th>Téléphone</th>
                                                <th>Action</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    {{--    Modal Form --}}
    <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">Ajouter un intervenant</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form name="courseForm" id="CourseForm" action="{{ url('admin/add-course-stakeholder/'.$stakeholders[0]->course_id) }}" method="post" enctype="multipart/form-data">@csrf
                    <div class="modal-body mx-3">
                        <div class="md-form mb-5">
                            <select class="form-control select2bs4" style="width: 100%;" name="stakeholder_id" id="stakeholder_id" required>
                                <option selected="" value="">Selectionner un intervenant</option>
                                @foreach($stakeholders_list as $stakeholder_list)
                                    <option value="{{ $stakeholder_list->id }}">{{ $stakeholder_list->names.' '.$stakeholder_list->surnames.' ('.$stakeholder_list->company->company_name.')' }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="course_id" value="{{$id}}">
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-primary" style="max-width:350px; width:350px; display: inline-block">Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
