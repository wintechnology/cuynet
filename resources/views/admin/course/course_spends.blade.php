@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Dépenses de sessions de formation</h1>
                        <b>Formation : </b> {{ $formation->theme }}
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('/admin/courses/infos/'.$id) }}">Session de formation #{{$id}}</a></li>
                            <li class="breadcrumb-item active">Dépenses de sessions de formation</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success_message'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('success_message') }}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <a href="{{ url('/admin/courses/infos/'.$id) }}" style="max-width:250px; float: left; display: inline-block" class="btn btn-block btn-info"> <i class="fas fa-arrow-alt-circle-left"></i> Retour</a>
                                <a href="{{ url('/admin/add-edit-spend/'.$id) }}" style="max-width:250px; float: right; display: inline-block" class="btn btn-block btn-outline-success">Ajouter un détail de dépense</a>
                                <a href="{{ url('/admin/courses/invoicePDF/'.$id) }}" style="max-width:250px; float: right; display: inline-block" class="btn btn-block btn-outline-info mr-2" target="_blank"><i class="fas fa-print"></i> Imprimer facture</a>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Reférence</th>
                                        <th>Désignation</th>
                                        <th>Qté</th>
                                        <th>P.U</th>
                                        <th>Prix Total (en Fcfa)</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($courseSpends as $spend)
                                        <tr>
                                            <td>{{ $spend->reference }}</td>
                                            <td>{{ $spend->designation }}</td>
                                            <td>{{ $spend->quantity }}</td>
                                            <td>{{ $spend->unit_amount }}</td>
                                            <td>{{ $spend->total_amount }}</td>
                                            <td>{{ date("d/m/Y", strtotime($spend->date)) }}</td>
                                            <td>
{{--                                                <a href="{{ url('/admin/courses/infos/'.$course->id) }}"><i class="fas fa-eye"></i></a>--}}
{{--                                                @if($course->status == 1)--}}
{{--                                                    <a class="updateCourseStatus" id="course-{{ $course->id }}" course_id="{{ $course->id }}" href="javascript:void(0)"><i class="fas fa-toggle-on"></i></a>--}}
{{--                                                @else--}}
{{--                                                    <a class="updateCourseStatus" id="course-{{ $course->id }}" course_id="{{ $course->id }}" href="javascript:void(0)"><i class="fas fa-toggle-off"></i></a>--}}
{{--                                                @endif--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Reférence</th>
                                        <th>Désignation</th>
                                        <th>Qté</th>
                                        <th>P.U</th>
                                        <th>Prix Total (en Fcfa)</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
