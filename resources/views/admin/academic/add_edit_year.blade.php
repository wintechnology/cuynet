@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Année Académique</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Année Académiques</a></li>
                            <li class="breadcrumb-item active">Admin Année Académique</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <form name="courseForm" id="CourseForm" action="{{ url('admin/add-edit-year') }}" method="post" enctype="multipart/form-data">@csrf
                    <!-- SELECT2 EXAMPLE -->
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">{{ $title }}</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Etablissement</label>
                                        @if(Session::has('error_message'))
                                            <div class="alert alert-danger" role="alert">
                                                {{ Session::get('error_message') }}
                                            </div>
                                        @endif
                                        @if(null == $school)
                                            <input type="text" name="" class="form-control" id="date" value="N/A" readonly>
                                            <input type="hidden" name="school_id" class="form-control" id="date" value="" required>
                                        @else
                                            <input type="text" name="" class="form-control" id="date" value="{{ $school['name'] }}" readonly>
                                            <input type="hidden" name="school_id" class="form-control" id="school_id" value="{{ $school['id'] }}">
                                        @endif
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Année Scolaire</label>
                                        <input type="text" name="scolar_year" class="form-control" id="scolar_year" value="{{$scolar}}" required>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date début</label>
                                        <input type="date" name="date_debut" class="form-control" id="date" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Année Précédente</label>
                                        <select class="form-control select2bs4" style="width: 100%;" name="previous_year_id" id="previous_year_id">
                                            <option selected="" value=""></option>
                                            @foreach( $academic_years as $year)
                                            <option value="{{ $year->year }}">{{ $year->year }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date fin</label>
                                        <input type="date" name="date_fin" class="form-control" id="date" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nombre de séquence(s)</label>
                                        <input type="number" name="sequence_amount" class="form-control" id="sequence_amount" value="6" required>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-12">
                                    <label for="exampleInputEmail1">Etat </label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="1" checked />
                                        <label class="form-check-label" for="inlineRadio1">Activée</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="0" />
                                        <label class="form-check-label" for="inlineRadio1">Désactivée</label>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    <!-- /.card -->
                </form>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
