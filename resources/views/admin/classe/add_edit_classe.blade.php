@extends('layouts.admin_layouts.admin_layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Classe</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Classes</a></li>
                        <li class="breadcrumb-item active">Admin Classe</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <form name="classeForm" id="ClasseForm" action="{{ url('admin/add-edit-classe') }}" method="post" enctype="multipart/form-data">@csrf
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">{{ $title }}</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Ecole</label>
                                    @if(null == $school)
                                    <input type="text" class="form-control" value="n/a" readonly>
                                    <input type="hidden" name="school_name" class="form-control" id="school_name" value="null">
                                    @else
                                    <input type="text" class="form-control" value="{{ $school['name'] }}" readonly>
                                    <input type="hidden" name="school_name" class="form-control" id="school_name" value="{{ $school['id'] }}">
                                    @endif
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Année</label>
                                    <select class="form-control select2bs4" style="width: 100%;" name="year_id" id="year_id" required>
                                        @foreach( $academicYears as $year)
                                        <option value="{{ $year->id }}">{{ $year->year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Abréviation</label>
                                    <input onchange="abrEnteredByClass(this.value)" type="text" name="abr" class="form-control" id="abr" placeholder="Entrer l'abréviation" required>
                                    <span class="text-red" id="abr_res"></span>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Description</label>
                                    <input type="text" name="description" class="form-control" id="description">
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Branche / Faculté</label>
                                    <select class="form-control select2bs4" style="width: 100%;" name="branch_id" id="branch_id">
                                        <option selected="" value=""></option>
                                        @foreach( $branches as $branch)
                                        <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tutelle</label>
                                    <input type="text" name="tutel" class="form-control" id="tutel">
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Département</label>
                                    <input type="text" name="department" class="form-control" id="department">
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Filière</label>
                                    <select onchange="abrEnteredBySection(this.value)" class="form-control select2bs4" style="width: 100%;" name="section_id" id="section_id" required>
                                        <option selected="" value=""></option>
                                        @foreach( $sections as $section)
                                        <option value="{{ $section->id }}">{{ $section->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-red" id="section_res"></span>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Niveau</label>
                                    <select onchange="abrEnteredByLevel(this.value)" class="form-control select2bs4" style="width: 100%;" name="level_id" id="level_id" required>
                                        <option selected="" value=""></option>
                                        @foreach( $levels as $level)
                                        <option value="{{ $level->id }}">{{ $level->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-red" id="level_res"></span>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Formation</label>
                                    <select class="form-control select2bs4" style="width: 100%;" name="formation" id="formation" required>
                                        <option selected="" value=""></option>
                                        @foreach( $typeFormations as $formation)
                                        <option value="{{ $formation->nam }}">{{ $formation->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Salle principale</label>
                                    <select class="form-control select2bs4" style="width: 100%;" name="room_id" id="room_id">
                                        <option selected="" value=""></option>
                                        @foreach( $rooms as $room)
                                        <option value="{{ $room->id }}">{{ $room->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Effectif maximale</label>
                                    <input type="number" name="max" class="form-control" id="max" placeholder="Entrer l'effectif maximal" value="10" required>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                {{-- Divider--}}
                                <div class="col-md-12">
                                    <label for="exampleInputEmail1">Classe optionnelle </label><br />
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="optional" id="inlineRadio1" value="1" checked />
                                        <label class="form-check-label" for="inlineRadio1">Oui</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="optional" id="inlineRadio1" value="0" />
                                        <label class="form-check-label" for="inlineRadio1">Non</label>
                                    </div>
                                    <hr />
                                </div>
                                {{-- End divider--}}
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Option des inscrits</label>
                                    <input type="text" name="option" class="form-control" id="option">
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Âge limite</label>
                                    <input type="number" name="limit_age" class="form-control" id="limit_age">
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                {{-- Divider--}}
                                <div class="col-md-12">
                                    <label for="exampleInputEmail1">Actif</label><br />
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="actif" id="inlineRadio1" value="1" checked />
                                        <label class="form-check-label" for="inlineRadio1">Oui</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="actif" id="inlineRadio1" value="0" />
                                        <label class="form-check-label" for="inlineRadio1">Non</label>
                                    </div>
                                    <hr />
                                </div>
                                {{-- End divider--}}
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-12">
                                {{-- Divider--}}
                                <div class="col-md-12">
                                    <label for="exampleInputEmail1">Classe examen</label><br />
                                    <hr />
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="exam_class" id="inlineRadio1" value="-1" checked />
                                        <label class="form-check-label" for="inlineRadio1">Classe intermédiaire, pas d'examen</label>
                                    </div>
                                    <br />
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="exam_class" id="inlineRadio1" value="0" />
                                        <label class="form-check-label" for="inlineRadio1">Classe d'examen FACULTATIF, les évaluations de classe permettent le passage en classe supérieure</label>
                                    </div>
                                    <br />
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="exam_class" id="inlineRadio1" value="1" />
                                        <label class="form-check-label" for="inlineRadio1">Classe d'examen OBLIGATOIRE, le succès à l'examen est OBLIGATOIRE pour passer en classe supérieure</label>
                                    </div>
                                    <hr />
                                </div>
                                {{-- End divider--}}
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Taux Hor Simple</label>
                                    <input type="number" name="simple_hourly_rate" class="form-control" id="simple_hourly_rate">
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Taux Hor Multiple</label>
                                    <input type="number" name="multiple_hourly_rate" class="form-control" id="multiple_hourly_rate">
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="{{ url('/admin/classes') }}" class="btn btn-outline-primary">Retour</a>
                        <button type="submit" class="btn btn-primary" id="btn_submit">Enregistrer</button>
                    </div>
                </div>
                <!-- /.card -->
            </form>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type='text/javascript'>
    var js_array = <?=json_encode($classesAbr)?>;


    function abrEnteredByClass(input) {

        var level_id = document.getElementById('level_id').value;
        var section_id = document.getElementById('section_id').value;
        var input = level_id + '' + input + '' + section_id;
        const button = document.getElementById('btn_submit');

        for (var i = 0; i < js_array.length; i++) {
            if(js_array.indexOf(input) !== -1)
            {
                document.getElementById('abr_res').innerHTML = "Cette classe existe déjà";
                document.getElementById('section_res').innerHTML = "dans cette filère";
                document.getElementById('level_res').innerHTML = "et de ce niveau";
                button.setAttribute('disabled', '');
            } else {
                document.getElementById('abr_res').innerHTML = "";
                document.getElementById('section_res').innerHTML = "";
                document.getElementById('level_res').innerHTML = "";
                button.removeAttribute('disabled');
            }
        }
    }

    function abrEnteredByLevel(input) {

        var class_abr = document.getElementById('abr').value;
        var section_id = document.getElementById('section_id').value;
        var input = input + '' + class_abr + '' + section_id;
        const button = document.getElementById('btn_submit');

        for (var i = 0; i < js_array.length; i++) {
            if(js_array.indexOf(input) !== -1)
            {
                document.getElementById('abr_res').innerHTML = "Cette classe existe déjà";
                document.getElementById('section_res').innerHTML = "dans cette filère";
                document.getElementById('level_res').innerHTML = "et de ce niveau";
                button.setAttribute('disabled', '');
            } else {
                document.getElementById('abr_res').innerHTML = "";
                document.getElementById('section_res').innerHTML = "";
                document.getElementById('level_res').innerHTML = "";
                button.removeAttribute('disabled');
            }
        }
    }

    function abrEnteredBySection(input) {

        var level_id = document.getElementById('level_id').value;
        var section_id = document.getElementById('section_id').value;
        var input = level_id + '' + class_abr + '' + input;
        const button = document.getElementById('btn_submit');

        for (var i = 0; i < js_array.length; i++) {
            if(js_array.indexOf(input) !== -1)
            {
                document.getElementById('abr_res').innerHTML = "Cette classe existe déjà";
                document.getElementById('section_res').innerHTML = "dans cette filère";
                document.getElementById('level_res').innerHTML = "et de ce niveau";
                button.setAttribute('disabled', '');
            } else {
                document.getElementById('abr_res').innerHTML = "";
                document.getElementById('section_res').innerHTML = "";
                document.getElementById('level_res').innerHTML = "";
                button.removeAttribute('disabled');
            }
        }
    }
</script>
@endsection
