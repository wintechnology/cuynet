@extends('layouts.admin_layouts.admin_layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Classes</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Acceuil</a></li>
                            <li class="breadcrumb-item active">Classes</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Table -->
                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success_message'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('success_message') }}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Classes</h3>
                                <a href="{{ url('/admin/add-edit-classe') }}" style="max-width:250px; float: right; display: inline-block" class="btn btn-block btn-outline-success">Ajouter une classe</a>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover table-responsive-lg">
                                    <thead>
                                    <tr>
                                        <th>Abréviation</th>
                                        <th>Branche/Faculté</th>
                                        <th>Filière/Section</th>
                                        <th>Niveau</th>
                                        <th>Salle</th>
                                        <th>Formation</th>
                                        <th>Effectif Max</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($classes as $classe)
                                        <tr>
                                            <td>{{ $classe->abr }}</td>
                                            <td>{{ $classe->branch->name }}</td>
                                            <td>{{ $classe->section->name }}</td>
                                            <td>{{ $classe->level->name }}</td>
                                            <td>{{ $classe->salle->name }}</td>
                                            <td>{{ $classe->formation }}</td>
                                            <td>{{ $classe->max }}</td>
                                            <td>
                                                @if($classe->actif == 1)
                                                    <a class="updateClasseStatus" id="classe-{{ $classe->id }}" classe_id="{{ $classe->id }}" href="javascript:void(0)">Actif</a>
                                                @else
                                                    <a class="updateClasseStatus" id="classe-{{ $classe->id }}" classe_id="{{ $classe->id }}" href="javascript:void(0)">Inactif</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Abréviation</th>
                                        <th>Branche/Faculté</th>
                                        <th>Filière/Section</th>
                                        <th>Niveau</th>
                                        <th>Salle</th>
                                        <th>Formation</th>
                                        <th>Effectif Max</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
