<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::prefix('/admin')->namespace('Admin')->group(function () {
    Route::match(['get', 'post'], '/', [App\Http\Controllers\Admin\AdminController::class, 'login']);
    Route::group(['middleware' => ['admin']], function () {
        Route::get('dashboard', [App\Http\Controllers\Admin\AdminController::class, 'dashboard']);
        Route::get('logout', [App\Http\Controllers\Admin\AdminController::class, 'logout']);


        //        Courses
        Route::get('courses', [App\Http\Controllers\Admin\CourseController::class, 'course']);
        Route::match(['get', 'post'], 'add-edit-course/{id?}', [App\Http\Controllers\Admin\CourseController::class, 'addEditCourse']);

        //        Academic Year
        Route::get('academic_years', [App\Http\Controllers\Admin\AcademicYearController::class, 'academic_year']);
        //        Route::get('courses/infos/{id}', [App\Http\Controllers\Admin\AcademicYearController::class, 'courseInfos']);
        Route::match(['get', 'post'], 'add-edit-year/{id?}', [App\Http\Controllers\Admin\AcademicYearController::class, 'addEditYear']);
        Route::get('courses/reportPDF/{id}', [App\Http\Controllers\Admin\CourseReportPDF::class, 'courseReportPDF']);

        //        Schedule
        Route::get('schedules', [App\Http\Controllers\Admin\AcademicYearController::class, 'schedules']);
        Route::match(['get', 'post'], 'add-edit-schedule/{id?}', [App\Http\Controllers\Admin\AcademicYearController::class, 'addEditSchedule']);

        //        Branch
        Route::get('branches', [App\Http\Controllers\Admin\BranchController::class, 'branches']);
        Route::match(['get', 'post'], 'add-edit-branch/{id?}', [App\Http\Controllers\Admin\BranchController::class, 'addEditBranch']);


        //        Options
        Route::get('options', [App\Http\Controllers\Admin\BranchController::class, 'options']);
        Route::match(['get', 'post'], 'add-edit-option/{id?}', [App\Http\Controllers\Admin\BranchController::class, 'addEditOption']);


        //        Regimes
        Route::get('regimes', [App\Http\Controllers\Admin\BranchController::class, 'regimes']);
        Route::match(['get', 'post'], 'add-edit-regime/{id?}', [App\Http\Controllers\Admin\BranchController::class, 'addEditRegime']);


        //        States
        Route::get('states', [App\Http\Controllers\Admin\BranchController::class, 'states']);
        Route::match(['get', 'post'], 'add-edit-state/{id?}', [App\Http\Controllers\Admin\BranchController::class, 'addEditState']);

        Route::match(['get', 'post'], 'add-edit-lv2/{id?}', [App\Http\Controllers\Admin\BranchController::class, 'addEditLv2']);
        Route::match(['get', 'post'], 'add-edit-art/{id?}', [App\Http\Controllers\Admin\BranchController::class, 'addEditArt']);
        Route::match(['get', 'post'], 'add-edit-required_document/{id?}', [App\Http\Controllers\Admin\BranchController::class, 'addEditRequiredDocument']);

        Route::match(['get', 'post'], 'rubrics', [App\Http\Controllers\Admin\BranchController::class, 'rubrics']);
        Route::match(['get', 'post'], 'add-edit-rubric/{id?}', [App\Http\Controllers\Admin\BranchController::class, 'addEditRubric']);

        Route::match(['get', 'post'], 'add-edit-annexe_fee/{id?}', [App\Http\Controllers\Admin\BranchController::class, 'addEditAnnexeFee']);
        // Formation Types
        Route::match(['get', 'post'], 'add-edit-formation_type/{id?}', [App\Http\Controllers\Admin\FormationController::class, 'addEditFormation']);

        //        Route::get('courses/invoicePDF/{id}',[App\Http\Controllers\Admin\CourseSpendInvoicePDF::class, 'courseSpendInvoicePDF']);

        //        AcademicYear Guest
        //        Route::match(['get', 'post'], 'add-edit-guest/{id?}', [App\Http\Controllers\Admin\AcademicYear::class, 'addEditCourseGuest']);
        //        Participant
        //        Route::match(['get', 'post'], 'add-course-participant/{id?}', [App\Http\Controllers\Admin\AcademicYear::class, 'addEditCourseParticipant']);
        //        Stakeholder
        //        Route::match(['get', 'post'], 'add-course-stakeholder/{id?}', [App\Http\Controllers\Admin\AcademicYear::class, 'addEditCourseStakeholder']);

        //        Route::get('courses/stakeholders/{id}', [App\Http\Controllers\Admin\AcademicYear::class, 'courseInfosStakeholders']);
        //        Route::get('courses/participants/{id}', [App\Http\Controllers\Admin\AcademicYear::class, 'courseInfosParticipants']);
        //        Route::get('courses/spends/{id}', [App\Http\Controllers\Admin\AcademicYear::class, 'courseInfosSpends']);
        //        Route::get('courses/guests/{id}', [App\Http\Controllers\Admin\AcademicYear::class, 'courseInfosGuests']);

        //        Classes
        Route::get('classes', [App\Http\Controllers\Admin\ClasseController::class, 'classe']);
        Route::match(['get', 'post'], 'add-edit-classe/{id?}', [App\Http\Controllers\Admin\ClasseController::class, 'addEditClasse']);

        //        Participants
        Route::get('participants', [App\Http\Controllers\Admin\ParticipantController::class, 'participant']);
        Route::match(['get', 'post'], 'add-edit-participant/{id?}', [App\Http\Controllers\Admin\ParticipantController::class, 'addEditParticipant']);

        //        Sequence
        Route::get('sequences', [App\Http\Controllers\Admin\SequenceController::class, 'sequence']);
        Route::match(['get', 'post'], 'add-edit-sequence/{id?}', [App\Http\Controllers\Admin\SequenceController::class, 'addEditSequence']);

        //        Sequence
        Route::get('trimesters', [App\Http\Controllers\Admin\TrimesterController::class, 'trimester']);
        Route::match(['get', 'post'], 'add-edit-trimester/{id?}', [App\Http\Controllers\Admin\TrimesterController::class, 'addEditTrimester']);

        //        Sections
        Route::get('sections', [App\Http\Controllers\Admin\SectionController::class, 'section']);
        Route::match(['get', 'post'], 'add-edit-section/{id?}', [App\Http\Controllers\Admin\SectionController::class, 'addEditSection']);

        //        Cycles
        Route::get('cycles', [App\Http\Controllers\Admin\SectionController::class, 'cycles']);
        Route::match(['get', 'post'], 'add-edit-cycle/{id?}', [App\Http\Controllers\Admin\SectionController::class, 'addEditCycle']);

        //        Lessons
        Route::get('lessons', [App\Http\Controllers\Admin\LessonController::class, 'lesson']);
        Route::match(['get', 'post'], 'add-edit-lesson/{id?}', [App\Http\Controllers\Admin\LessonController::class, 'addEditLesson']);

        ////        Settings

        /// Courses Categories
        Route::get('categories', [App\Http\Controllers\Admin\CategoryController::class, 'category']);
        Route::match(['get', 'post'], 'add-edit-category/{id?}', [App\Http\Controllers\Admin\CategoryController::class, 'addEditCategory']);

        /// Year Categories
        Route::get('year-categories', [App\Http\Controllers\Admin\CategoryController::class, 'yearCategory']);
        Route::match(['get', 'post'], 'add-edit-year-category/{id?}', [App\Http\Controllers\Admin\CategoryController::class, 'addEditYearCategory']);

        /// Rooms Types
        Route::get('types', [App\Http\Controllers\Admin\TypeController::class, 'type']);
        Route::match(['get', 'post'], 'add-edit-roomsType/{id?}', [App\Http\Controllers\Admin\TypeController::class, 'addEditRoomsType']);

        /// Sections Types
        Route::get('types_sections', [App\Http\Controllers\Admin\SectionTypeController::class, 'type']);
        Route::match(['get', 'post'], 'add-edit-types_sections/{id?}', [App\Http\Controllers\Admin\SectionTypeController::class, 'addEditSectionsType']);

        /// Levels
        Route::get('levels', [App\Http\Controllers\Admin\LevelsController::class, 'level']);
        Route::match(['get', 'post'], 'add-edit-levels/{id?}', [App\Http\Controllers\Admin\LevelsController::class, 'addEditLevel']);

        /// Countries
        Route::get('countries', [App\Http\Controllers\Admin\CountryController::class, 'countries']);
        Route::match(['get', 'post'], 'add-edit-countries/{id?}', [App\Http\Controllers\Admin\CountryController::class, 'addEditCountry']);

        /// Building
        Route::get('buildings', [App\Http\Controllers\Admin\BuildingController::class, 'building']);
        Route::match(['get', 'post'], 'add-edit-building/{id?}', [App\Http\Controllers\Admin\BuildingController::class, 'addEditBuilding']);

        /// Region
        Route::get('regions', [App\Http\Controllers\Admin\CountryController::class, 'regions']);
        Route::match(['get', 'post'], 'add-edit-region/{id?}', [App\Http\Controllers\Admin\CountryController::class, 'addEditRegion']);

        //Department
        Route::get('departments', [App\Http\Controllers\Admin\CountryController::class, 'departements']);
        Route::match(['get', 'post'], 'add-edit-department/{id?}', [App\Http\Controllers\Admin\CountryController::class, 'addEditDepartement']);

        /// boroughs
        Route::get('boroughs', [App\Http\Controllers\Admin\CountryController::class, 'boroughs']);
        Route::match(['get', 'post'], 'add-edit-borough/{id?}', [App\Http\Controllers\Admin\CountryController::class, 'addEditBorough']);

        /// Sexes
        Route::get('sexes', [App\Http\Controllers\Admin\SubscriptionController::class, 'sexes']);
        Route::match(['get', 'post'], 'add-edit-sex/{id?}', [App\Http\Controllers\Admin\SubscriptionController::class, 'addEditSex']);

        /// Civils States
        Route::get('maritals', [App\Http\Controllers\Admin\SubscriptionController::class, 'maritals']);
        Route::match(['get', 'post'], 'add-edit-marital/{id?}', [App\Http\Controllers\Admin\SubscriptionController::class, 'addEditMarital']);

        /// fathers
        Route::get('fathers', [App\Http\Controllers\Admin\SubscriptionController::class, 'fathers']);
        Route::match(['get', 'post'], 'add-edit-father/{id?}', [App\Http\Controllers\Admin\SubscriptionController::class, 'addEditFather']);

        /// mohers
        Route::get('mothers', [App\Http\Controllers\Admin\SubscriptionController::class, 'mothers']);
        Route::match(['get', 'post'], 'add-edit-mother/{id?}', [App\Http\Controllers\Admin\SubscriptionController::class, 'addEditMother']);

        /// tutors
        Route::get('tutors', [App\Http\Controllers\Admin\SubscriptionController::class, 'tutors']);
        Route::match(['get', 'post'], 'add-edit-tutor/{id?}', [App\Http\Controllers\Admin\SubscriptionController::class, 'addEditTutor']);

        /// Sites
        Route::get('sites', [App\Http\Controllers\Admin\SiteController::class, 'sites']);
        Route::match(['get', 'post'], 'add-edit-site/{id?}', [App\Http\Controllers\Admin\SiteController::class, 'addEditSite']);

        /// Rooms
        Route::get('rooms', [App\Http\Controllers\Admin\RoomController::class, 'room']);
        Route::match(['get', 'post'], 'add-edit-room/{id?}', [App\Http\Controllers\Admin\RoomController::class, 'addEditRoom']);

        /// Schools
        //        Route::get('schools', [App\Http\Controllers\Admin\SchoolController::class, 'schools']);
        Route::match(['get', 'post'], 'add-edit-school/{id?}', [App\Http\Controllers\Admin\SchoolController::class, 'addEditSchool']);

        /// Teacher
        Route::get('teachers', [App\Http\Controllers\Admin\TeacherController::class, 'teachers']);
        Route::match(['get', 'post'], 'add-edit-teacher/{id?}', [App\Http\Controllers\Admin\TeacherController::class, 'addEditTeacher']);

        /// library_categories
        Route::get('library_categories', [App\Http\Controllers\Admin\LibraryController::class, 'libraryCategories']);
        Route::match(['get', 'post'], 'add-edit-library_category/{id?}', [App\Http\Controllers\Admin\LibraryController::class, 'addEditLibraryCategory']);

        /// locations
        Route::get('locations', [App\Http\Controllers\Admin\LibraryController::class, 'locations']);
        Route::match(['get', 'post'], 'add-edit-location/{id?}', [App\Http\Controllers\Admin\LibraryController::class, 'addEditLocation']);

        /// ouvrage
        Route::get('ouvrages', [App\Http\Controllers\Admin\LibraryController::class, 'ouvrages']);
        Route::match(['get', 'post'], 'add-edit-ouvrage/{id?}', [App\Http\Controllers\Admin\LibraryController::class, 'addEditOuvrage']);

        /// rent
        Route::get('rents', [App\Http\Controllers\Admin\LibraryController::class, 'rents']);
        Route::match(['get', 'post'], 'add-edit-rent/{id?}', [App\Http\Controllers\Admin\LibraryController::class, 'addEditRent']);

        /// bookings
        Route::get('bookings', [App\Http\Controllers\Admin\LibraryController::class, 'bookings']);
        Route::match(['get', 'post'], 'add-edit-booking/{id?}', [App\Http\Controllers\Admin\LibraryController::class, 'addEditBooking']);

        /// Subscriptions
        Route::get('subscriptions', [App\Http\Controllers\Admin\SubscriptionController::class, 'subscriptions']);
        //        Route::match(['get','post'],'add-edit-subscription/{id?}', [App\Http\Controllers\Admin\SubscriptionController::class, 'addEditSubscription']);
        Route::prefix('/subscriptions')->namespace('Subscriptions')->group(function () {
            Route::get('create-step-one', [App\Http\Controllers\Admin\SubscriptionController::class, 'createStepOne']);
            Route::post('create-step-one', [App\Http\Controllers\Admin\SubscriptionController::class, 'postCreateStepOne']);

            Route::get('create-step-two', [App\Http\Controllers\Admin\SubscriptionController::class, 'createStepTwo']);
            Route::post('create-step-two', [App\Http\Controllers\Admin\SubscriptionController::class, 'postCreateStepTwo']);

            Route::get('create-step-three', [App\Http\Controllers\Admin\SubscriptionController::class, 'createStepThree']);
            Route::post('create-step-three', [App\Http\Controllers\Admin\SubscriptionController::class, 'postCreateStepThree']);

            Route::get('create-step-four', [App\Http\Controllers\Admin\SubscriptionController::class, 'createStepFour']);
            Route::post('create-step-four', [App\Http\Controllers\Admin\SubscriptionController::class, 'postCreateStepFour']);

            Route::post('find', [App\Http\Controllers\Admin\SubscriptionController::class, 'postFind']);
            Route::post('find-collect', [App\Http\Controllers\Admin\SubscriptionController::class, 'postFindCollect']);
            Route::get('collect/{id?}', [App\Http\Controllers\Admin\SubscriptionController::class, 'collect']);
            Route::match(['get', 'post'], 'add-collect/{id?}', [App\Http\Controllers\Admin\SubscriptionController::class, 'addCollect']);
            Route::get('collect-ticket/{id?}', [App\Http\Controllers\Admin\SubscriptionController::class, 'collectTicketPDF']);

            Route::get('student-file-pdf/{id?}', [App\Http\Controllers\Admin\SubscriptionController::class, 'studentFilePDF']);
        });
    });
});
